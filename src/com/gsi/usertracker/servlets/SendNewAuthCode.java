package com.gsi.usertracker.servlets;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.Authorization;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.utility.GSIUtility;
import com.gsi.utility.SendMessageUtility;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;

public class SendNewAuthCode extends HttpServlet {
	private static final long serialVersionUID = 3754085364052460468L;
	private static final Logger logger = Logger.getLogger("2faLogger");	

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
		int hostPort = request.getServerPort();
    	String twoFaUserCredentials = "";
    	String emailAddresses = "";
    	String phoneNumbers = "";
        try {
        	twoFaUserCredentials = GSIUtility.getParam(request, "twoFaUserCredentials", "");
        	String deliverySelect = GSIUtility.getParam(request, "deliverySelect", "");
        	emailAddresses = GSIUtility.getParam(request, "emailAddresses", "");
        	phoneNumbers = GSIUtility.getParam(request, "phoneNumbers", "");
        	String userName = null;
        	logger.info("1 - twoFaUserCredentials: " + twoFaUserCredentials);
        	logger.info("1.1 - deliverySelect: " + deliverySelect);
        	logger.info("1.2 - emailAddresses: " + emailAddresses);
        	logger.info("1.3 - phoneNumbers: " + phoneNumbers);

	        String sendOptions = "";
	        if (emailAddresses != null && emailAddresses.trim().length() > 0) {
	        	sendOptions = sendOptions + "&emailAddresses=" + emailAddresses;
	        }
	        if (phoneNumbers != null && phoneNumbers.trim().length() > 0) {
	        	sendOptions = sendOptions + "&phoneNumbers=" + phoneNumbers;
	        }
        	if (deliverySelect != null && deliverySelect.trim().length() > 0) {
	        	if (twoFaUserCredentials != null) {
	        		twoFaUserCredentials = GSIUtility.easyDecrypt(twoFaUserCredentials);
	            	logger.info("2 - twoFaUserCredentials decrypted : " + twoFaUserCredentials);
		        	StringTokenizer st = new StringTokenizer(twoFaUserCredentials, ";");
		        	while(st.hasMoreTokens()) {
		        		String hold = st.nextToken();
		        		if (hold.indexOf(GSIUtility.USERNAME) > -1) {
		            		StringTokenizer st2 = new StringTokenizer(hold, ":");
		            		st2.nextToken();
		            		userName = st2.nextToken();
		        		}
		        	}
	        	}
        	    UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        	    Browser browser = userAgent.getBrowser();
        	    String browserName = browser.getName();

        	    List triggerCriteriaList = TwoFactorAuthorizerDAO.selectTriggerProperties();
		        String userIPAddress = request.getRemoteAddr();
		        TwoFactorDomain authorizationTypeTfd = new TwoFactorDomain();
		        authorizationTypeTfd.setIpAddress(userIPAddress);
		        authorizationTypeTfd.setUserName(userName);
		        authorizationTypeTfd.setBrowserType(browserName);
		        String adminAlways2FA = TwoFactorAuthorizerDAO.selectPropertyByKey("adminAlways2FA", hostPort);
		        Authorization authorization = GSIUtility.determineAuthorization(authorizationTypeTfd, triggerCriteriaList, adminAlways2FA);

        	    TwoFactorDomain tfd = new TwoFactorDomain();
	        	tfd.setValid(0);
	        	if (userName != null) {
	        		tfd.setUserName(userName);
	        	}
	        	if (authorization.getAuthorizationType().equals(GSIUtility.IPBLACKLIST) || authorization.getAuthorizationType().equals(GSIUtility.IPWHITELIST) || authorization.getAuthorizationType().equals(GSIUtility.IPUNIQUE)) {
	        		tfd.setIpAddress(request.getRemoteAddr());
	        	}
	        	else if (authorization.getAuthorizationType().equals(GSIUtility.BROWSERUNIQUE)) {
	        		tfd.setBrowserType(browserName);
	        	}
	        	else if (authorization.getAuthorizationType().equals(GSIUtility.ADMIN2FA)) {
	        		// no need to set anything else...
	        	}
	        	else {
	        		throw new Exception("Authorization Trigger type is not valid!");
	        	}
	        	List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthCode(tfd);
	        	boolean hasCodeAlready = false;
		        if (userDomain != null & userDomain.size() > 0) {
	            	logger.info("3 - userDomain exists, deleting old one first:" + ((TwoFactorDomain)userDomain.get(0)).toString());
		        	TwoFactorAuthorizerDAO.deleteUserDomain(tfd);
		        	hasCodeAlready = true;
		        }
	        	String newAuthCode = RandomStringUtils.random(8, true, true);
	        	tfd.setAuthCode(newAuthCode);
	        	logger.info("4 - now adding new userDomain:" + tfd.toString());
	        	TwoFactorAuthorizerDAO.insertUserDomain(tfd);
		        // now send that auth code
		        StringTokenizer st = new StringTokenizer(deliverySelect, "|");
		        String encryptedMethod = st.nextToken();
		        String maskedMethod = st.nextToken();
		        String messageBody = "Your JDE authentication code is:" + newAuthCode + ". Please enter it on the JDE authentication page to continue the login process.";
	        	String deliveryMethod = GSIUtility.easyDecrypt(encryptedMethod);
		        if (maskedMethod.indexOf("@") > -1) { // email address
		        	List<String> emailRecipients = new ArrayList<String>();
		        	emailRecipients.add(deliveryMethod);
		        	String emailSubject = "JDE Authentication Code";
		        	logger.info("16 - Before email send");
		        	SendMessageUtility.sendEmailViaRest(messageBody, emailSubject, emailRecipients, hostPort);
			        logger.info("17 - After email send"); 
		        }
		        else { // phone number
		        	List<String> phoneRecipients = new ArrayList<String>();
		        	phoneRecipients.add(deliveryMethod);
		        	logger.info("16 - Before sms send");
		        	SendMessageUtility.sendSMSViaRest(messageBody, phoneRecipients, hostPort);
			        logger.info("17 - After sms send"); 
		        }
	        	//logger.info("13 - Before redirect  - " + httpServletRequest.getSession().getId());
	        	//logger.info("13.1 - twoFaUserCredentials=" + user);
		        // now redirect to auth capture page
	        	String hiddenMessage = "";
	        	if (hasCodeAlready) {
	        		hiddenMessage = "&hiddenMessage=" + GSIUtility.easyEncrypt("A new authorization code has been sent to you. Enter it on the page after it's been received.");
	        	}
	        	String redirectURL =  GSIUtility.getURL(request, "/AuthCapture", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(twoFaUserCredentials) + sendOptions + hiddenMessage);
	        	logger.info("13.2 - redirectURL: " + redirectURL);
	        	response.sendRedirect(redirectURL);
	        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
				return;
        	}
        	else {
        		logger.info("***** THEY DIDN'T SELECT A DELIVERY METHOD *****");
	        	String hiddenMessage = "&hiddenMessage=You must select a delivery method to receive the authorization code.";
		        String redirectURL = GSIUtility.getURL(request, "/AuthSendOptions", "?twoFaUserCredentials=" + twoFaUserCredentials + sendOptions + hiddenMessage);
	        	logger.info("13.2 - redirectURL: " + redirectURL);
	        	response.sendRedirect(redirectURL);
	        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
				return;
        	}
        } 
        catch (Exception e) {
        	try {
	    		logger.info("***** ERROR *****");
				logger.error("",e);
		        String sendOptions = "";
		        if (emailAddresses != null && emailAddresses.trim().length() > 0) {
		        	sendOptions = sendOptions + "&emailAddresses=" + emailAddresses;
		        }
		        if (phoneNumbers != null && phoneNumbers.trim().length() > 0) {
		        	sendOptions = sendOptions + "&phoneNumbers=" + phoneNumbers;
		        }
	        	String hiddenMessage = "&hiddenMessage=An error has occurred:" + e.getMessage();
		        String redirectURL = GSIUtility.getURL(request, "/AuthSendOptions", "?twoFaUserCredentials=" + twoFaUserCredentials + sendOptions + hiddenMessage);
	        	logger.info("13.2 - redirectURL: " + redirectURL);
	        	response.sendRedirect(redirectURL);
	        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
				return;
        	}
        	catch (Exception e1) {
        		logger.error("", e);
        	}
        }  
    }  	
}
