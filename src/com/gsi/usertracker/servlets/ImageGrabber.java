package com.gsi.usertracker.servlets;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImageGrabber extends HttpServlet {
    /**
     * Servlet resizes an image located in a directory in a web project ex.
     * /image?root=/albums&file=/thumbs/imagename.jpg&width=270&height=100 ex.
     * /image?file=/thumbs/imagename.jpg&width=270 (default root, calculated
     * height)
     */
    private static final long serialVersionUID = -8285774993751841288L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        // Optional: Only supports output of jpg and png, defaults to png if not
        // specified
        String imageOutput = getParam(request, "output", "png");
        // Optional: Folder in web app where images are located, defaults to
        // albums if not specified
        //String imageRoot = getParam(request, "root", "/albums");
        // Required: Path from root to image, including filename
        String imageFile = getParam(request, "file", "/Album1/image1.jpg");
        // Required: Width image should be resized to
        int width = Integer.parseInt(getParam(request, "width", "80"));
        // Optional: If specified used, otherwise proportions are calculated
        int height = Integer.parseInt(getParam(request, "height", "0"));

        // Set the mime type of the image
        if ("png".equals(imageOutput))
            response.setContentType("image/png");
        else
            response.setContentType("image/jpeg");

        // Server Location of the image
        String imageLoc = imageFile;

        try {
            // Read the original image from the Server Location
            BufferedImage bufferedImage = ImageIO.read(Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/resources/" + imageLoc));
            // Calculate the new Height if not specified
            int calcHeight = height > 0 ? height : (width * bufferedImage.getHeight() / bufferedImage.getWidth());
            // Write the image
            if (width != 0 && height != 0) {
            	ImageIO.write(createResizedCopy(bufferedImage, width, calcHeight), imageOutput, response.getOutputStream());
            }
            else {
            	ImageIO.write(bufferedImage, imageOutput, response.getOutputStream());
            }
        } catch (Exception e) {
            log("Problem with image: " + imageLoc + e);
        }
    }

    protected BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight) {
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = scaledBI.createGraphics();
        //g.setColor(new Color(30, 74, 109));
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, scaledWidth, scaledHeight);
        g.setComposite(AlphaComposite.Src);
        //g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, 0, 0, originalImage.getWidth(null), originalImage.getHeight(null), new Color(30, 74, 109), null);
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, 0, 0, originalImage.getWidth(null), originalImage.getHeight(null), Color.DARK_GRAY, null);
        g.dispose();
        Image intermediateWithTransparentPixels = makeColorTransparent(scaledBI, Color.DARK_GRAY);
        //finalize the transparent image
        BufferedImage finalImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gf = finalImage.createGraphics();
        gf.setComposite(AlphaComposite.SrcOver);
        gf.setColor(new Color(0, 0, 0, 0));
        gf.fillRect(0, 0, scaledWidth, scaledHeight);
        gf.drawImage(intermediateWithTransparentPixels, 0, 0, scaledWidth, scaledHeight, new Color(0, 0, 0, 0), null);
        gf.dispose();

        return finalImage;
       
        //return scaledBI;
    }

    public static Image makeColorTransparent(Image im, final Color color) {
        ImageFilter filter = new RGBImageFilter() {
            // the color we are looking for... Alpha bits are set to opaque
            public int markerRGB = color.getRGB() | 0xFF000000;

            public final int filterRGB(int x, int y, int rgb) {
                if ((rgb | 0xFF000000) == markerRGB) {
                    // Mark the alpha bits as zero - transparent
                    return 0x00FFFFFF & rgb;
                } else {
                    // nothing to do
                    return rgb;
                }
            }
        };

        ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }    
    // Check the param if it's not present return the default
    private String getParam(HttpServletRequest request, String param, String def) {
        String parameter = request.getParameter(param);
        if (parameter == null || "".equals(parameter)) {
            return def;
        } else {
            return parameter;
        }
    }
}