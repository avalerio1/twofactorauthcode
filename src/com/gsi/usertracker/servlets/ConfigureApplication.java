package com.gsi.usertracker.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.utility.GSIUtility;

public class ConfigureApplication extends HttpServlet {
	private static final long serialVersionUID = 5402908389861111413L;
	private static final Logger logger = Logger.getLogger("prLogger");

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	logger.info("1 - In 2FAConfigurationApplication");
		int hostPort = request.getServerPort();
    	//checkClassPath(hostPort);
    	String outputString = "";
    	StringBuffer xsb = new StringBuffer();
		PrintWriter out = response.getWriter();
		String hiddenMessage = "";
        try {
        	hiddenMessage = request.getParameter("hiddenMessage");
        	if (hiddenMessage == null) {
        		hiddenMessage = "";
        	}
        	else {
        		hiddenMessage = GSIUtility.easyDecrypt(hiddenMessage);
        	}

        	InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("skeletons/2FAConfiguration.html");
        	BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String output;
			StringBuffer osb = new StringBuffer();
			while ((output = br.readLine()) != null) {
				osb.append(output).append("\n");
			}
			outputString = osb.toString();
			outputString = outputString.replaceAll("!!hiddenMessage!!", hiddenMessage);
			outputString = outputString.replaceAll("!!pageContents!!", "");
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			outputString = outputString.replaceAll("!!productLogo!!", "gShield_Logo_small_final.png");
			boolean foundIt = TwoFactorAuthorizerDAO.checkPropertyByKey("twilioFromNumber", hostPort);
			if (foundIt) {
				outputString = outputString.replaceAll("!!jdeSystemDatabaseName!!", TwoFactorAuthorizerDAO.selectPropertyByKey("jdeSystemDatabaseName", hostPort));
				outputString = outputString.replaceAll("!!jdeSystemConnectionURL!!", TwoFactorAuthorizerDAO.selectPropertyByKey("jdeSystemConnectionURL", hostPort));
				outputString = outputString.replaceAll("!!jdeUserDatabaseName!!", TwoFactorAuthorizerDAO.selectPropertyByKey("jdeUserDatabaseName", hostPort));
				outputString = outputString.replaceAll("!!jdeEnvironmentConnectionURL!!", TwoFactorAuthorizerDAO.selectPropertyByKey("jdeEnvironmentConnectionURL", hostPort));
				outputString = outputString.replaceAll("!!dbmsUserId!!", TwoFactorAuthorizerDAO.selectPropertyByKey("dbmsUserId", hostPort));
				outputString = outputString.replaceAll("!!dbmsUserPassword!!", TwoFactorAuthorizerDAO.selectPropertyByKey("dbmsUserPassword", hostPort));
				outputString = outputString.replaceAll("!!jdeJDBCDriver!!", TwoFactorAuthorizerDAO.selectPropertyByKey("jdeJDBCDriver", hostPort));
				outputString = outputString.replaceAll("!!jdeUserName!!", TwoFactorAuthorizerDAO.selectPropertyByKey("jdeUserName", hostPort));
				outputString = outputString.replaceAll("!!emailFromAddress!!", TwoFactorAuthorizerDAO.selectPropertyByKey("emailFromAddress", hostPort));
				String noSendOptionsPassthru = TwoFactorAuthorizerDAO.selectPropertyByKey("noSendOptionsPassthru", hostPort);
				if (noSendOptionsPassthru.trim().equalsIgnoreCase("True")) {
					outputString = outputString.replaceAll("!!ptTrueSelected!!", "selected");
					outputString = outputString.replaceAll("!!ptFalseSelected!!", "");
				}
				else {
					outputString = outputString.replaceAll("!!ptTrueSelected!!", "");
					outputString = outputString.replaceAll("!!ptFalseSelected!!", "selected");
				}
				String authorizationType = TwoFactorAuthorizerDAO.selectPropertyByKey("authorizationType", hostPort);
				if (authorizationType.trim().equalsIgnoreCase("GA")) {
					outputString = outputString.replaceAll("!!atGASelected!!", "selected");
					outputString = outputString.replaceAll("!!at2FASelected!!", "");
				}
				else {
					outputString = outputString.replaceAll("!!atGASelected!!", "");
					outputString = outputString.replaceAll("!!at2FASelected!!", "selected");
				}
				outputString = outputString.replaceAll("!!trigger1!!", TwoFactorAuthorizerDAO.selectPropertyByKey("trigger1", hostPort));
				outputString = outputString.replaceAll("!!totpOrganization!!", TwoFactorAuthorizerDAO.selectPropertyByKey("totpOrganization", hostPort));
				outputString = outputString.replaceAll("!!twilioFromNumber!!", TwoFactorAuthorizerDAO.selectPropertyByKey("twilioFromNumber", hostPort));
				String loggingLevel = TwoFactorAuthorizerDAO.selectPropertyByKey("loggingLevel", hostPort);
				if (loggingLevel.trim().equalsIgnoreCase("ALL")) {
					outputString = outputString.replaceAll("!!allSelected!!", "selected");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("TRACE")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "selected");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("DEBUG")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "selected");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("INFO")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "selected");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("WARN")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "selected");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("ERROR")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "selected");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("FATAL")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "selected");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
				else if (loggingLevel.trim().equalsIgnoreCase("OFF")) {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "selected");
				}
				else {
					outputString = outputString.replaceAll("!!allSelected!!", "");
					outputString = outputString.replaceAll("!!traceSelected!!", "");
					outputString = outputString.replaceAll("!!debugSelected!!", "");
					outputString = outputString.replaceAll("!!infoSelected!!", "");
					outputString = outputString.replaceAll("!!warnSelected!!", "");
					outputString = outputString.replaceAll("!!errorSelected!!", "");
					outputString = outputString.replaceAll("!!fatalSelected!!", "");
					outputString = outputString.replaceAll("!!offSelected!!", "");
				}
			}
			else {
				outputString = outputString.replaceAll("!!jdeSystemDatabaseName!!", "");
				outputString = outputString.replaceAll("!!jdeSystemConnectionURL!!", "");
				outputString = outputString.replaceAll("!!jdeUserDatabaseName!!", "");
				outputString = outputString.replaceAll("!!jdeEnvironmentConnectionURL!!", "");
				outputString = outputString.replaceAll("!!dbmsUserId!!", "");
				outputString = outputString.replaceAll("!!dbmsUserPassword!!", "");
				outputString = outputString.replaceAll("!!jdeJDBCDriver!!", "");
				outputString = outputString.replaceAll("!!jdeUserName!!", "");
				outputString = outputString.replaceAll("!!emailFromAddress!!", "");
				outputString = outputString.replaceAll("!!ptTrueSelected!!", "");
				outputString = outputString.replaceAll("!!ptFalseSelected!!", "");
				outputString = outputString.replaceAll("!!atGASelected!!", "");
				outputString = outputString.replaceAll("!!at2FASelected!!", "");
				outputString = outputString.replaceAll("!!trigger1!!", "");
				outputString = outputString.replaceAll("!!totpOrganization!!", "");
				outputString = outputString.replaceAll("!!twilioFromNumber!!", "");
				outputString = outputString.replaceAll("!!allSelected!!", "");
				outputString = outputString.replaceAll("!!traceSelected!!", "");
				outputString = outputString.replaceAll("!!debugSelected!!", "");
				outputString = outputString.replaceAll("!!infoSelected!!", "");
				outputString = outputString.replaceAll("!!warnSelected!!", "");
				outputString = outputString.replaceAll("!!errorSelected!!", "");
				outputString = outputString.replaceAll("!!fatalSelected!!", "");
				outputString = outputString.replaceAll("!!offSelected!!", "");
			}
    	}
        catch (Exception e) {
    		logger.info("***** ERROR *****");
			logger.error("",e);
			//outputString = "An error occured: " + e.getMessage();
        	hiddenMessage = request.getParameter("hiddenMessage");
        	if (hiddenMessage == null) {
        		hiddenMessage = "";
        	}
        	else {
        		hiddenMessage = GSIUtility.easyDecrypt(hiddenMessage);
        	}
        	InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("skeletons/GeneralMessagesWide.html");
        	BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String output;
			StringBuffer osb = new StringBuffer();
			while ((output = br.readLine()) != null) {
				osb.append(output).append("\n");
			}
			outputString = osb.toString();
			outputString = outputString.replaceAll("!!hiddenMessage!!", hiddenMessage);
			outputString = outputString.replaceAll("!!pageContents!!", "");
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			outputString = outputString.replaceAll("!!productLogo!!", "gShield_Logo_small_final.pngg");
			outputString = outputString.replaceAll("!!generalMessage!!", "An error occurred: " + e.getMessage() + ". Please contact your system administrator." + "<br>" + "<br>");
        }  
        finally {
    		response.setContentType("text/html");
    		response.setHeader("Cache-Control", "no-cache");
			out.print(outputString);
        	out.close();
        }
    }  	
}
