package com.gsi.usertracker.servlets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.utility.GSIUtility;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.KeyRepresentation;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder;

public class GoogleQRGenerator extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger("2faLogger");	
       
    public GoogleQRGenerator() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int hostPort = request.getServerPort();
	   	String twoFaUserCredentials = "";
    	String userName = null;
        try {
        	String needQRCode = GSIUtility.getParam(request, "xyz", "");
        	if (needQRCode != null && needQRCode.trim().equalsIgnoreCase("true")) {
	        	twoFaUserCredentials = GSIUtility.getParam(request, "twoFaUserCredentials", "");
	        	logger.info("1 - twoFaUserCredentials: " + twoFaUserCredentials);
	        	if (twoFaUserCredentials != null) {
	        		twoFaUserCredentials = GSIUtility.easyDecrypt(twoFaUserCredentials);
	            	logger.info("2 - twoFaUserCredentials decrypted : " + twoFaUserCredentials);
		        	StringTokenizer st = new StringTokenizer(twoFaUserCredentials, ";");
		        	while(st.hasMoreTokens()) {
		        		String hold = st.nextToken();
		        		if (hold.indexOf(GSIUtility.USERNAME) > -1) {
		            		StringTokenizer st2 = new StringTokenizer(hold, ":");
		            		st2.nextToken();
		            		userName = st2.nextToken();
		        		}
		        	}
	        	}
	    	    TwoFactorDomain tfd = new TwoFactorDomain();
	        	tfd.setValid(0);
        		tfd.setUserName(userName);
	        	List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthCode(tfd);
	        	boolean hasCodeAlready = false;
		        if (userDomain != null & userDomain.size() > 0) {
	            	logger.info("3 - userDomain exists, deleting old one first:" + ((TwoFactorDomain)userDomain.get(0)).toString());
		        	TwoFactorAuthorizerDAO.deleteUserDomain(tfd);
		        	hasCodeAlready = true;
		        }
		        
				GoogleAuthenticatorConfigBuilder gacb = new GoogleAuthenticatorConfigBuilder().setKeyRepresentation(KeyRepresentation.BASE32);
				GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator(gacb.build());
				//GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
				GoogleAuthenticatorKey key = googleAuthenticator.createCredentials();
				String secret = key.getKey();
				StringBuffer sb = new StringBuffer().append(secret);
				List<Integer> scratchCodes = key.getScratchCodes();
		        for (Iterator i = scratchCodes.iterator(); i.hasNext();) {
		        	Integer scratchCode = (Integer)i.next();
		        	sb.append("|").append(scratchCode);
		        }
	        	tfd.setAuthCode(sb.toString());
	        	logger.info("4 - now adding new userDomain:" + tfd.toString());
	        	TwoFactorAuthorizerDAO.insertUserDomain(tfd);
				String totpOrganization = TwoFactorAuthorizerDAO.selectPropertyByKey("totpOrganization", hostPort);
				logger.info("4.01 - totpOrganization:" + totpOrganization);
				if (totpOrganization != null && totpOrganization.trim().length() > 0) {
					String holdUserName = userName;
					if (holdUserName.trim().length() < 3) {
						holdUserName = rPad(holdUserName, 3);
					}
					logger.info("4.02 - holdUserName:" + holdUserName + "*");
					logger.info("4.03 - secret:" + secret);
					logger.info("4.04 - date/time:" + new Date());
					URIBuilder uri = new URIBuilder().setScheme("otpauth").setHost("totp").setPath("/" +totpOrganization + ":" + holdUserName).setParameter("secret", secret).setParameter("issuer", totpOrganization);
					logger.info("4.1 - GA URI: " + uri.toString());
					logger.info("4.2 - GA URI URL decoded: " + java.net.URLDecoder.decode(uri.toString()));
					String myCodeText = uri.toString(); // "otpauth://totp/GSI%20Inc.:alvalerio@mail.com?secret=P5DGHDBYKYQI4FAP&issuer=GSI+Inc.";
					int size = 200;
					try {
						Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
						hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
						//hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
						hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
			
						QRCodeWriter qrCodeWriter = new QRCodeWriter();
						BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
						int width = byteMatrix.getWidth();
						BufferedImage image = new BufferedImage(width, width, BufferedImage.TYPE_INT_RGB);
						image.createGraphics();
			
						Graphics2D graphics = (Graphics2D) image.getGraphics();
						graphics.setColor(Color.WHITE);
						graphics.fillRect(0, 0, width, width);
						graphics.setColor(Color.BLACK);
			
						for (int i = 0; i < width; i++) {
							for (int j = 0; j < width; j++) {
								if (byteMatrix.get(i, j)) {
									graphics.fillRect(i, j, 1, 1);
								}
							}
						}
						
			            response.setContentType("image/png");
			        	OutputStream out = response.getOutputStream();
			        	ImageIO.write(image, "png", out);
			        	out.flush();
			        	out.close();
					} 
					catch (WriterException e) {
			    		logger.info("***** ERROR *****");
						logger.error("",e);
						throw new Exception("Error writing QR code: " + e.getMessage());
					}
				}
				else {
					throw new Exception("TOTP organization is missing from the properties file.");
				}
        	}
        } 
        catch (Exception e) {
        	try {
	    		logger.info("***** ERROR *****");
				logger.error("",e);
		        String sendOptions = "";
	        	String hiddenMessage = "&hiddenMessage=An error has occurred:" + e.getMessage();
		        String redirectURL = GSIUtility.getURL(request, "/AuthSendOptions", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(twoFaUserCredentials) + sendOptions + hiddenMessage);
	        	logger.info("13.2 - redirectURL: " + redirectURL);
	        	response.sendRedirect(redirectURL);
	        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
				return;
        	}
        	catch (Exception e1) {
        		logger.error("", e);
        	}
        }  
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public static String rPad(String inString, int outLength) {
		int inStringLen = inString.trim().length();
		StringBuffer outString = new StringBuffer(outLength + 1);
		outString.append(inString.trim());
		for (int i = inStringLen; i < outLength; i++) {
			outString.append(" ");
		}
		return outString.toString();
	}

	@Override
	public void init() throws ServletException {
		System.setProperty("java.awt.headless", "true"); 
		super.init();
	}
}
