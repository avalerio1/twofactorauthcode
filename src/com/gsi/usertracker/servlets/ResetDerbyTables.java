package com.gsi.usertracker.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.Property;
import com.gsi.utility.GSIIbatisConnectionUtility;
import com.gsi.utility.GSIUtility;
import com.ibatis.sqlmap.client.SqlMapClient;

public class ResetDerbyTables extends HttpServlet {
	private static final long serialVersionUID = 3754085364052460468L;
	private static final Logger logger = Logger.getLogger(ResetDerbyTables.class);
	private static final String ENCRYPTEDTRIGGERCRITERIATABLENAME = "QVBQLlRSSUdHRVJDUklURVJJQQ==";
	private static final String ENCRYPTEDUSERLOGINTRACKERTABLENAME = "QVBQLlVTRVJMT0dJTlRSQUNLRVI=";
	private static final String ENCRYPTEDUSERDOMAINTABLENAME = "QVBQLlVTRVJET01BSU4=";
	private static final String ENCRYPTEDPROPERTIESTABLENAME = "QVBQLlBST1BFUlRJRVM=";
	

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
    	String resultMessage = "";
        try {
        	if (request.getSession().getAttribute("gsiSuperAdminUser") == null) {
            	String paramString = "?hiddenMessage=" + GSIUtility.easyEncrypt("You login first before accessing the admin console."); 
            	logger.info("18.1 - *** redirecting to AdminConsoleLogin");
            	String redirectURL = GSIUtility.getURL(request, "/AdminConsoleLogin", paramString);
            	if (redirectURL != null) {
            		logger.info("9 - REDIRECT to MainMenu URL: " + redirectURL.toString());
    				HttpServletResponse httpResponse = (HttpServletResponse) response;
            		logger.info("10 - Before redirect");
    				httpResponse.sendRedirect(redirectURL.toString());
            		logger.info("11 - After redirect");
    				return;
            	}
        	}
        	else {
    			SqlMapClient smc = TwoFactorAuthorizerDAO.getSqlMapClient();
	        	String encryptedTableName = GSIUtility.getParam(request, "tableName", "");
	        	/*if (encryptedTableName.equals(ENCRYPTEDTRIGGERCRITERIATABLENAME)) { // trigger criteria
	        		TwoFactorAuthorizerDAO.dropTriggerCriteriaTable(smc);
	        		TwoFactorAuthorizerDAO.createTriggerCriteriaTable(smc);
	        		// now insert triggers into the table
	    			HashMap<String, String> triggers = new HashMap<String, String>(); 
	    			ResourceBundle properties = PropertyResourceBundle.getBundle("resources/gsitracking");
	    			for (Object key : properties.keySet()) {
	    				String keyString = (String) key;
	    				//String value = getPropertyValue(properties.getProperty(keyString));
	    				String value = properties.getString(keyString);
	    				if (keyString.indexOf("trigger") > -1) {
	    					triggers.put(keyString, properties.getString(keyString));
	    				}
	    			}
	    			TwoFactorAuthorizerDAO.addTriggerData(smc, triggers);
	        		resultMessage = "Trigger Criteria table has been dropped and refreshed successfully.";
	        	} else */
	        	if (encryptedTableName.equals(ENCRYPTEDUSERLOGINTRACKERTABLENAME)) { // user login tracker
	        		TwoFactorAuthorizerDAO.dropUserLoginTrackerTable(smc);
	        		TwoFactorAuthorizerDAO.createUserLoginTrackerTable(smc);
	        		resultMessage = "User Login Tracker table has been dropped and refreshed successfully.";
	        	}
	        	else if (encryptedTableName.equals(ENCRYPTEDUSERDOMAINTABLENAME)) { // user domain
	        		TwoFactorAuthorizerDAO.dropUserDomainTable(smc);
	        		TwoFactorAuthorizerDAO.createUserDomainTable(smc);
	        		resultMessage = "User Domain table has been dropped and refreshed successfully.";
	        	}
	        	else if (encryptedTableName.equals(ENCRYPTEDPROPERTIESTABLENAME)) { //properties 
	        		TwoFactorAuthorizerDAO.dropPropertiesTable(smc);
	        		TwoFactorAuthorizerDAO.createPropertiesTable(smc);
	    			ResourceBundle properties = PropertyResourceBundle.getBundle("resources/gsitracking");
	    			for (Object key : properties.keySet()) {
	    				String keyString = (String) key;
	    				//String value = getPropertyValue(properties.getProperty(keyString));
	    				String value = properties.getString(keyString);
	    				Property prop = new Property();
	    				prop.setKeyValue(keyString);
	    				prop.setValueValue(value);
	    				smc.insert("insertProperty", prop);
	    			}
	        		resultMessage = "Properties table has been dropped and refreshed successfully.";
	        	}
	        	else {
	        		resultMessage = "Table parameter does not match any encrypted tables.";
	        	}
        	}
        } 
        catch (Exception e) {
        	logger.error("", e);
    		resultMessage = "An error occurred resetting table: " + e.getMessage();
        }  
        finally {
        	String paramString = "?hiddenMessage=" + GSIUtility.easyEncrypt(resultMessage); 
        	logger.info("18.1 - *** redirecting to MainMenu");
        	String redirectURL = GSIUtility.getURL(request, "/MainMenu", paramString);
        	if (redirectURL != null) {
        		logger.info("9 - REDIRECT to ViewUserDomain URL: " + redirectURL.toString());
				HttpServletResponse httpResponse = (HttpServletResponse) response;
        		logger.info("10 - Before redirect");
				httpResponse.sendRedirect(redirectURL.toString());
        		logger.info("11 - After redirect");
				return;
        	}
        }
    }  	
}
