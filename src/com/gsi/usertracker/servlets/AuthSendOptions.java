package com.gsi.usertracker.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.utility.GSIUtility;

public class AuthSendOptions extends HttpServlet {
	private static final long serialVersionUID = -1;
	private static final Logger logger = Logger.getLogger("2faLogger");
	

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
    	StringBuffer osb = new StringBuffer();
    	String outputString = "";
        try {
        	StringBuffer sb = new StringBuffer();
        	sb.append("<tr>").append("\n");
        	sb.append("<td colspan=\"2\" align=\"center\">").append("\n");
        	sb.append("<table cellpadding=\"15\" cellspacing=\"0\" class=\"overlay\" width=\"528\">").append("\n");
        	sb.append("<tbody>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\"><h2>Sign In</h2>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td style=\"padding: 0px; margin: 0px;\" class=\"listHeader\">").append("<img src=\"").append(request.getContextPath()).append("/imageGrabber?file=gShield_Logo_small_final.png&width=0&height=0\" height=\"63\" width=\"80\">").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\">").append("\n");
        	sb.append("We have detected that you are logging in from either a new location, browser or time frame. Please select the delivery method below to receive an authorization code to continue logging into the system.").append("\n");
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\">").append("\n");
        	sb.append("<form name=\"authCaptureForm\" id=\"authCaptureForm\" method=\"POST\" action=\"").append(request.getContextPath()).append("/SendNewAuthCode\">").append("\n");
        	sb.append("<div><label for=\"deliveryMethod\">Delivery Method</label></div>").append("\n");
        	sb.append("<select id=\"deliverySelect\" name=\"deliverySelect\">").append("\n");
        	sb.append("<option value=\"\">-- Select Delivery Method --</option>").append("\n");
        	sb.append("</select>").append("\n");
        	sb.append("<input type=\"hidden\" name=\"twoFaUserCredentials\" id=\"twoFaUserCredentials\" value=\"").append(request.getParameter("twoFaUserCredentials")).append("\">").append("\n");
			String emailAddress = request.getParameter("emailAddresses");
			if (emailAddress == null) {
				emailAddress = "";
			}
        	sb.append("<input type=\"hidden\" name=\"emailAddresses\" id=\"emailAddresses\" value=\"").append(emailAddress).append("\">").append("\n");
			String phoneNumbers = request.getParameter("phoneNumbers");
			if (phoneNumbers == null) {
				phoneNumbers = "";
			}
        	sb.append("<input type=\"hidden\" name=\"phoneNumbers\" id=\"phoneNumbers\" value=\"").append(phoneNumbers).append("\">").append("\n");
        	sb.append("<div class=\"margin-top5\" nowrap=\"\"></div>").append("\n");
        	sb.append("<div>&nbsp;</div>").append("\n");
        	sb.append("<div align=\"left\"></div>").append("\n");
        	sb.append("<div>&nbsp;</div>").append("\n");
        	sb.append("<div class=\"loginAlignLeft\" id=\"ADVANCED_CHECKBOX\" style=\"display: block;\"></div>").append("\n");
        	sb.append("<div>&nbsp;</div>").append("\n");
        	sb.append("<div>").append("\n");
        	sb.append("<input class=\"buttonstylenormal margin-top5\" value=\"Continue...\" type=\"submit\">").append("\n");
        	sb.append("</div>").append("\n");
        	sb.append("</form>").append("\n");
        	sb.append("</td>").append("\n");
           	sb.append("</tr>").append("\n");

        	
        	sb.append("").append("\n");
        	sb.append("").append("\n");
        	
        	
        	sb.append("</tbody>").append("\n");
        	sb.append("</table>").append("\n");

        	
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr align=\"center\" valign=\"middle\">").append("\n");
        	sb.append("<td align=\"center\" class=\"copyright\">&copy;2017 GSI. All Rights Reserved.&emsp;</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("</table>").append("\n");

        	sb.append("<script>").append("\n");
        	sb.append("document.addEventListener('DOMContentLoaded', function() {");
        	sb.append("// add email addresses and phone numbers to delivery select").append("\n");
        	sb.append("try {").append("\n");
        	sb.append("var deliveryMethodSelect = document.getElementById(\"deliverySelect\");").append("\n");
        	sb.append("var emailAddresses = document.getElementById(\"emailAddresses\").value;").append("\n");
        	sb.append("if (emailAddresses != \"null\" && emailAddresses != null && emailAddresses !=\"\") {").append("\n");
        	sb.append("var emailArray = emailAddresses.split(',');").append("\n");
        	sb.append("for (var i = 0; i < emailArray.length - 1; i++) {").append("\n");
        	sb.append("var emailAddress = emailArray[i];").append("\n");
        	sb.append("var pieces = emailAddress.split('|');").append("\n");
        	sb.append("var option = document.createElement(\"option\");").append("\n");
        	sb.append("option.value = emailAddress;").append("\n");
        	sb.append("option.text = 'Email Address: ' + pieces[1];").append("\n");
        	sb.append("deliveryMethodSelect.add(option);").append("\n");
        	sb.append("}").append("\n");
        	sb.append("}").append("\n");
        	sb.append("}").append("\n");
        	sb.append("catch (err) {").append("\n");
        	sb.append("alert('Error is:' + err);").append("\n");
        	sb.append("}").append("\n");
        	sb.append("try {").append("\n");
        	sb.append("var phoneNumbers = document.getElementById(\"phoneNumbers\").value;").append("\n");
        	sb.append("if (phoneNumbers != \"null\" && phoneNumbers != null && phoneNumbers !=\"\") {").append("\n");
        	sb.append("var phoneArray = phoneNumbers.split(',');").append("\n");
        	sb.append("for (var i = 0; i < phoneArray.length - 1; i++) {").append("\n");
        	sb.append("var phoneNumber = phoneArray[i];").append("\n");
        	sb.append("var pieces = phoneNumber.split('|');").append("\n");
        	sb.append("var option = document.createElement(\"option\");").append("\n");
        	sb.append("option.value = phoneNumber;").append("\n");
        	sb.append("option.text = 'Mobile Phone: ' + pieces[1];").append("\n");
        	sb.append("deliveryMethodSelect.add(option);").append("\n");
        	sb.append("}").append("\n");
        	sb.append("}").append("\n");
        	sb.append("}").append("\n");
        	sb.append("catch (err) {").append("\n");
        	sb.append("alert('Error is:' + err);").append("\n");
        	sb.append("}").append("\n");
        	sb.append("});").append("\n");
        	sb.append("</script>").append("\n");

        	String hiddenMessage = request.getParameter("hiddenMessage");
        	if (hiddenMessage == null) {
        		hiddenMessage = "";
        	}
        	else {
        		hiddenMessage = GSIUtility.easyDecrypt(hiddenMessage);
        	}

        	InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("skeletons/2FASkeleton.html");
        	BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String output;
			while ((output = br.readLine()) != null) {
				osb.append(output).append("\n");
			}
			outputString = osb.toString();
			outputString = outputString.replaceAll("!!hiddenMessage!!", hiddenMessage);
			outputString = outputString.replaceAll("!!pageContents!!", sb.toString());
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			outputString = outputString.replaceAll("!!productLogo!!", "gShield_Logo_small_final.png");
        	
			//outputString = outputString.replaceAll("!!twoFaUserCredentials!!", request.getParameter("twoFaUserCredentials"));
			/*String emailAddress = request.getParameter("emailAddresses");
			if (emailAddress == null) {
				emailAddress = "";
			}
			outputString = outputString.replaceAll("!!emailAddresses!!", emailAddress);
			String phoneNumbers = request.getParameter("phoneNumbers");
			if (phoneNumbers == null) {
				phoneNumbers = "";
			}
			outputString = outputString.replaceAll("!!phoneNumbers!!", phoneNumbers);
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			if (request.getParameter("hiddenMessage") != null) {
				outputString = outputString.replaceAll("!!hiddenMessage!!", request.getParameter("hiddenMessage"));
			}
			else {
				outputString = outputString.replaceAll("!!hiddenMessage!!", "");
			}*/
        } 
        catch (Exception e) {
        	logger.error("", e);
        	outputString = "An error occurred showing main menu : " + e.getMessage();
        }
        finally {
    		response.setContentType("text/html");
    		response.setHeader("Cache-Control", "no-cache");
			out.print(outputString);
        	out.close();
        }
    }  	
}
