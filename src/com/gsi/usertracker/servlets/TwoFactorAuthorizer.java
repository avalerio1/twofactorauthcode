package com.gsi.usertracker.servlets;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.Authorization;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.utility.GSIUtility;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;

public class TwoFactorAuthorizer extends HttpServlet {
	private static final Logger logger = Logger.getLogger("2faLogger");	

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) {  
		int hostPort = request.getServerPort();
    	String twoFaUserCredentials = GSIUtility.getParam(request, "twoFaUserCredentials", "");
        try {
        	String authorizerCode = GSIUtility.getParam(request, "authorizerCode", "");
        	String userName = null;
        	logger.info("1 - authorizer code and twoFaUserCredentials: " + authorizerCode + " - " + twoFaUserCredentials);

        	if (twoFaUserCredentials != null) {
        		twoFaUserCredentials = GSIUtility.easyDecrypt(twoFaUserCredentials);
            	logger.info("2 - twoFaUserCredentials decrypted : " + twoFaUserCredentials);
	        	StringTokenizer st = new StringTokenizer(twoFaUserCredentials, ";");
	        	while(st.hasMoreTokens()) {
	        		String hold = st.nextToken();
	        		if (hold.indexOf(GSIUtility.USERNAME) > -1) {
	            		StringTokenizer st2 = new StringTokenizer(hold, ":");
	            		st2.nextToken();
	            		userName = st2.nextToken();
	        		}
	        	}
        	}
    	    UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
    	    Browser browser = userAgent.getBrowser();
    	    String browserName = browser.getName();
        	TwoFactorDomain tfd = new TwoFactorDomain();
        	tfd.setValid(0);
        	if (authorizerCode != null) {
        		tfd.setAuthCode(authorizerCode);
        	}
        	if (userName != null) {
        		tfd.setUserName(userName);
        	}
        	
    	    List triggerCriteriaList = TwoFactorAuthorizerDAO.selectTriggerProperties();
	        String userIPAddress = request.getRemoteAddr();
	        TwoFactorDomain authorizationTypeTfd = new TwoFactorDomain();
	        authorizationTypeTfd.setIpAddress(userIPAddress);
	        authorizationTypeTfd.setUserName(userName);
	        authorizationTypeTfd.setBrowserType(browserName);
	        String adminAlways2FA = TwoFactorAuthorizerDAO.selectPropertyByKey("adminAlways2FA", hostPort);
	        Authorization authorization = GSIUtility.determineAuthorization(authorizationTypeTfd, triggerCriteriaList, adminAlways2FA);
	        logger.info("2.1 -Authorization: " + authorization.toString());
        	if (authorization.getAuthorizationType().equals(GSIUtility.IPBLACKLIST) || authorization.getAuthorizationType().equals(GSIUtility.IPWHITELIST) || authorization.getAuthorizationType().equals(GSIUtility.IPUNIQUE)) {
        		tfd.setIpAddress(request.getRemoteAddr());
        	}
        	else if (authorization.getAuthorizationType().equals(GSIUtility.BROWSERUNIQUE)) {
        		tfd.setBrowserType(browserName);
        	}
        	else if (authorization.getAuthorizationType().equals(GSIUtility.ADMIN2FA)) {
        		// no need to set anything else...
        	}
        	else {
        		throw new Exception("Authorization Trigger type is not valid!");
        	}
        	logger.info("2.2 -TwoFactorDomain: " + tfd.toString());
        	List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthCode(tfd);
	        if (userDomain != null & userDomain.size() > 0) {
            	logger.info("3 - userDomain != null & userDomain.size() > 0");
	        	tfd.setValid(1);
	        	logger.info("3.1: " + tfd.toString());
	        	TwoFactorAuthorizerDAO.updateValidCode(tfd);
	        	//request.getSession().setAttribute("2FA", "false");
	        	
	        	StringBuffer userAndPasswordParams = new StringBuffer("?");
	        	StringTokenizer st = new StringTokenizer(twoFaUserCredentials, ";");
	        	while(st.hasMoreTokens()) {
	        		String hold = st.nextToken();
	        		StringTokenizer st2 = new StringTokenizer(hold, ":");
	        		userAndPasswordParams.append(st2.nextToken()).append("=").append(st2.nextToken()).append("&");
	        	}
	        	String sendOptions = "";
	        	if (authorization.getAuthorizationType().equals(GSIUtility.ADMIN2FA)) {
    	        	sendOptions="8Ij9CFd1=" + userName; //RandomStringUtils.random(8, true, true);
	        	}
	        	
	        	String redirectURL = GSIUtility.getURL(request, "/E1Menu.maf", userAndPasswordParams.toString() + sendOptions);
	        	if (redirectURL != null) {
	        		logger.info("5 - REDIRECT to LOGIN URL: " + redirectURL.toString());
					HttpServletResponse httpResponse = (HttpServletResponse) response;
	        		logger.info("6 - Before redirect");
					httpResponse.sendRedirect(redirectURL.toString());
	        		logger.info("7 - After redirect");
					return;
	        	}
	        	else {
	            	logger.info("4 - redirectURL == null");
	        		// we should probably do something here....
	        	}
	        }
	        else {
	        	logger.info("8 - userDomain == null || userDomain.size() == 0");
	        	logger.info("8.1 - TwoFactorDomain: " + tfd.toString());
	        	String paramString = "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(twoFaUserCredentials) + "&hiddenMessage='Authorization Code was not found or has expired. If it has expired please request a new one from the link on the page.'"; 
		        String redirectURL = GSIUtility.getURL(request, "/AuthCapture", paramString);
	        	if (redirectURL != null) {
	        		logger.info("9 - REDIRECT to AUTH CAPTURE URL: " + redirectURL.toString());
					HttpServletResponse httpResponse = (HttpServletResponse) response;
	        		logger.info("10 - Before redirect");
					httpResponse.sendRedirect(redirectURL.toString());
	        		logger.info("11 - After redirect");
					return;
	        	}
	        }
        } 
        catch (Exception e) {
        	try {
				logger.error("",e);
	            e.printStackTrace();
	        	String paramString = "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(twoFaUserCredentials) + "&hiddenMessage='Authorization Code was not able to be verified at this time. Error: " + e.getMessage() + "'"; 
		        String redirectURL = GSIUtility.getURL(request, "/AuthCapture", paramString);
	        	if (redirectURL != null) {
	        		logger.info("9 - REDIRECT to AUTH CAPTURE URL: " + redirectURL.toString());
					HttpServletResponse httpResponse = (HttpServletResponse) response;
	        		logger.info("10 - Before redirect");
					httpResponse.sendRedirect(redirectURL.toString());
	        		logger.info("11 - After redirect");
					return;
	        	}
        	}
        	catch (Exception e2) {
        		logger.error("", e2);
        		e2.printStackTrace();
        	}
        }  
    }  	
}
