package com.gsi.usertracker.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.TriggerCriteria;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.usertracker.model.UserLoginTracker;
import com.gsi.utility.GSIUtility;

public class ViewTriggerCriteria extends HttpServlet {
	private static final long serialVersionUID = -1;
	private static final Logger logger = Logger.getLogger(ViewTriggerCriteria.class);
	

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
    	StringBuffer osb = new StringBuffer();
    	String outputString = "";
        try {
        	if (request.getSession().getAttribute("gsiSuperAdminUser") == null) {
            	String paramString = "?hiddenMessage=" + GSIUtility.easyEncrypt("You login first before accessing the admin console."); 
            	logger.info("18.1 - *** redirecting to AdminConsoleLogin");
            	String redirectURL = GSIUtility.getURL(request, "/AdminConsoleLogin", paramString);
            	if (redirectURL != null) {
            		logger.info("9 - REDIRECT to MainMenu URL: " + redirectURL.toString());
    				HttpServletResponse httpResponse = (HttpServletResponse) response;
            		logger.info("10 - Before redirect");
    				httpResponse.sendRedirect(redirectURL.toString());
            		logger.info("11 - After redirect");
    				return;
            	}
        	}
        	else {
	        	StringBuffer sb = new StringBuffer();
	        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S a");
	        	sb.append("<table cellpadding=\"25\" cellspacing=\"0\" class=\"overlay\">").append("\n");
	        	sb.append("<tbody>").append("\n");
	        	sb.append("<tr>").append("\n");
	        	sb.append("<td width=\"100%\" style=\"text-align: center; color:white;\">").append("\n");
	        	sb.append("<div>").append("\n");
	        	sb.append("<h3 style=\"font-size: 16px\">Configuration Parameters</h3>").append("\n");
	        	sb.append("</div>").append("\n");
	        	sb.append("<div align=\"center\">").append("\n");
	        	sb.append("<table class=\"radius\">").append("\n");
	        	sb.append("<thead style=\"thead\">").append("\n");
	        	sb.append("<tr>").append("\n");
	        	sb.append("<th>Trigger Type</th>").append("\n");
	        	sb.append("<th>Trigger Criteria</th>").append("\n");
	        	sb.append("</tr>").append("\n");
	        	sb.append("</thead>").append("\n");
	        	sb.append("<tbody style=\"border-collapse: collapse;\">").append("\n");
	        	List allUsers = TwoFactorAuthorizerDAO.selectTriggerProperties();
	        	for (Iterator i = allUsers.iterator(); i.hasNext();) {
	        		TriggerCriteria tfd = (TriggerCriteria)i.next();
	            	sb.append("<tr>");
	            	sb.append("<td class=\"cpmp\">").append(tfd.getTriggerType()).append("</td>");
	            	sb.append("<td class=\"cpmp2\">").append(tfd.getTriggerCriteria()).append("</td>");
	            	sb.append("</tr>");
	        	}
            	sb.append("</tbody>");
	        	sb.append("</table>");
	        	sb.append("</div>");
	        	sb.append("</td>");
	        	sb.append("</tr>").append("\n");
	        	sb.append("</tbody>").append("\n");
	        	sb.append("</table>").append("\n");
	        	String hiddenMessage = request.getParameter("hiddenMessage");
	        	if (hiddenMessage == null) {
	        		hiddenMessage = "";
	        	}
	        	else {
	        		hiddenMessage = GSIUtility.easyDecrypt(hiddenMessage);
	        	}

	        	InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("skeletons/TablePageSkeleton.html");
	        	BufferedReader br = new BufferedReader(new InputStreamReader(input));
				String output;
				while ((output = br.readLine()) != null) {
					osb.append(output).append("\n");
				}
				outputString = osb.toString();
				outputString = outputString.replaceAll("!!hiddenMessage!!", hiddenMessage);
				outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
				outputString = outputString.replaceAll("!!tableContents!!", sb.toString());
        	}
        } 
        catch (Exception e) {
        	logger.error("", e);
        	outputString = "An error occurred showing main menu : " + e.getMessage();
        }
        finally {
    		response.setContentType("text/html");
    		response.setHeader("Cache-Control", "no-cache");
			out.print(outputString);
        	out.close();
        }
    }  	
}
