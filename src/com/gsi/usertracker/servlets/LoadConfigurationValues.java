package com.gsi.usertracker.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.utility.GSIUtility;
import com.ibatis.sqlmap.client.SqlMapClient;

public class LoadConfigurationValues extends HttpServlet {
	private static final long serialVersionUID = 401104958842731627L;
	private static final Logger logger = Logger.getLogger("prLogger");

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
		int hostPort = request.getServerPort();
    	//checkClassPath(hostPort);
    	String userName = "";
        try {
            Properties props = new Properties();
            InputStream is = null;
            try {
				File f = new File(System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE) + "ConfigurationExport.properties");
				is = new FileInputStream( f );
            }
            catch (Exception e) {
            	is = null;
            	logger.error("", e);
            }
	        if ( is == null ) {
	        	// send message that props file is missing
	        	throw new Exception("Trying to load properties from file but ConfigurationExport.properties is missing.");
	        }
	        else {
		        props.load( is );
		        is.close();

	        	String jdeSystemDatabaseName = props.getProperty("jdeSystemDatabaseName", "");
	        	String jdeSystemConnectionURL = props.getProperty("jdeSystemConnectionURL", "");
	        	String jdeUserDatabaseName = props.getProperty("jdeUserDatabaseName", "");
	        	String jdeEnvironmentConnectionURL = props.getProperty("jdeEnvironmentConnectionURL", "");
	        	String jdeEnvironment = props.getProperty("jdeEnvironment", "");
	        	String dbmsUserId = props.getProperty("dbmsUserId", "");
	        	String dbmsUserPassword = props.getProperty("dbmsUserPassword", "");
	        	String jdeJDBCDriver = props.getProperty("jdeJDBCDriver", "");
	        	String jdeUserName = props.getProperty("jdeUserName", "");
	        	String emailFromAddress = props.getProperty("emailFromAddress", "");
	        	String twilioFromNumber = props.getProperty("twilioFromNumber", "");
	        	String noSendOptionsPassthru = props.getProperty("noSendOptionsPassthru", "");
	        	String authorizationType = props.getProperty("authorizationType", "");
	        	String trigger1 = props.getProperty("trigger1", "");
	        	String totpOrganization = props.getProperty("totpOrganization", "");
	        	String loggingLevel = props.getProperty("loggingLevel", "");
		        
	        	logger.info("1 - jdeSystemDatabaseName: " + jdeSystemDatabaseName);
	        	logger.info("1 - jdeSystemConnectionURL: " + jdeSystemConnectionURL);
	        	logger.info("1 - jdeUserDatabaseName: " + jdeUserDatabaseName);
	        	logger.info("1 - jdeEnvironmentConnectionURL: " + jdeEnvironmentConnectionURL);
	        	logger.info("1 - jdeEnvironment: " + jdeEnvironment);
	        	logger.info("1 - dbmsUserId: " + dbmsUserId);
	        	logger.info("1 - dbmsUserPassword: " + dbmsUserPassword);
	        	logger.info("1 - jdeJDBCDriver: " + jdeJDBCDriver);
	        	logger.info("1 - jdeUserName: " + jdeUserName);
	        	logger.info("1 - emailFromAddress: " + emailFromAddress);
	        	logger.info("1 - noSendOptionsPassthru: " + noSendOptionsPassthru);
	        	logger.info("1 - authorizationType: " + authorizationType);
	        	logger.info("1 - trigger1: " + trigger1);
	        	logger.info("1 - totpOrganization: " + totpOrganization);
	        	logger.info("1 - twilioFromNumber: " + twilioFromNumber);
	        	logger.info("1 - loggingLevel: " + loggingLevel);

				SqlMapClient smc = TwoFactorAuthorizerDAO.getSqlMapClient();
				boolean foundIt = TwoFactorAuthorizerDAO.checkPropertyByKey("twilioFromNumber", hostPort);
				if (foundIt) {
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeSystemDatabaseName", jdeSystemDatabaseName, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeSystemConnectionURL", jdeSystemConnectionURL, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeUserDatabaseName", jdeUserDatabaseName, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeEnvironmentConnectionURL", jdeEnvironmentConnectionURL, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "dbmsUserId", dbmsUserId, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "dbmsUserPassword", dbmsUserPassword, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeJDBCDriver", jdeJDBCDriver, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeUserName", jdeUserName, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "emailFromAddress", emailFromAddress, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "noSendOptionsPassthru", noSendOptionsPassthru, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "authorizationType", authorizationType, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "trigger1", trigger1, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "totpOrganization", totpOrganization, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "twilioFromNumber", twilioFromNumber, hostPort);
		        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "loggingLevel", loggingLevel, hostPort);
				}
				else {
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeSystemDatabaseName", jdeSystemDatabaseName, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeSystemConnectionURL", jdeSystemConnectionURL, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeUserDatabaseName", jdeUserDatabaseName, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeEnvironmentConnectionURL", jdeEnvironmentConnectionURL, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeEnvironment", jdeEnvironment, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "dbmsUserId", dbmsUserId, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "dbmsUserPassword", dbmsUserPassword, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeJDBCDriver", jdeJDBCDriver, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeUserName", jdeUserName, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "emailFromAddress", emailFromAddress, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "noSendOptionsPassthru", noSendOptionsPassthru, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "authorizationType", authorizationType, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "trigger1", trigger1, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "totpOrganization", totpOrganization, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "twilioFromNumber", twilioFromNumber, hostPort);
		        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "loggingLevel", loggingLevel, hostPort);
				}
	        }
        	logger.info("***** Configuration Loaded *****");
		    String hiddenMessage = "&hiddenMessage=" + GSIUtility.easyEncrypt("Configuration has been loaded from properties fils. Please check system status..");
			String redirectURL = GSIUtility.getURL(request, "/ConfigureApplication", "?userName=" + GSIUtility.easyEncrypt(userName) + hiddenMessage);
		    logger.info("13.2 - redirectURL: " + redirectURL);
		    response.sendRedirect(redirectURL);
			return;
        } 
        catch (Exception e) {
        	try {
	    		logger.info("***** ERROR *****");
				logger.error("",e);
		        String sendOptions = "";
	        	String hiddenMessage = "&hiddenMessage=" + GSIUtility.easyEncrypt("An error has occurred loading configuration:" + e.getMessage());
		        String redirectURL = GSIUtility.getURL(request, "/ConfigureApplication", "?userName=" + userName + sendOptions + hiddenMessage);
	        	logger.info("13.2 - redirectURL: " + redirectURL);
	        	response.sendRedirect(redirectURL);
				return;
        	}
        	catch (Exception e1) {
        		logger.error("", e);
        	}
        }  
    }  	
}
