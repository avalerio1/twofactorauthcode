package com.gsi.usertracker.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.utility.GSIUtility;

public class AuthCapture extends HttpServlet {
	private static final long serialVersionUID = -1;
	private static final Logger logger = Logger.getLogger("2faLogger");
	

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
    	StringBuffer osb = new StringBuffer();
    	String outputString = "";
        try {
        	String twoFACredentials = request.getParameter("twoFaUserCredentials");
			String emailAddress = request.getParameter("emailAddresses");
			if (emailAddress == null) {
				emailAddress = "";
			}
			String phoneNumbers = request.getParameter("phoneNumbers");
			if (phoneNumbers == null) {
				phoneNumbers = "";
			}
        	
        	StringBuffer sb = new StringBuffer();
        	sb.append("<script type=\"text/JavaScript\">").append("\n");
        	sb.append("function submitNewAuthCodeForm() {").append("\n");
        	sb.append("document.sendNewAuthCodeForm.submit();").append("\n");
        	sb.append("}").append("\n");
        	sb.append("</script>").append("\n");
        	sb.append("<form id=\"authCaptureForm\" name=\"sendNewAuthCodeForm\" method=\"POST\" action=\"").append(request.getContextPath()).append("/AuthSendOptions\">").append("\n");
        	sb.append("<input type=\"hidden\" name=\"twoFaUserCredentials\" id=\"twoFaUserCredentials\" value=\"").append(twoFACredentials).append("\">").append("\n");
        	sb.append("<input type=\"hidden\" name=\"deliverySelect\" id=\"deliverySelect\" value=\"!!deliverySelect!!\">").append("\n");
        	sb.append("<input type=\"hidden\" name=\"emailAddresses\" id=\"emailAddresses\" value=\"").append(emailAddress).append("\">").append("\n");
        	sb.append("<input type=\"hidden\" name=\"phoneNumbers\" id=\"phoneNumbers\" value=\"").append(phoneNumbers).append("\">").append("\n");
        	sb.append("</form>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td colspan=\"2\" align=\"center\">").append("\n");
        	sb.append("<table cellpadding=\"15\" cellspacing=\"0\" class=\"overlay\" width=\"528\">").append("\n");
        	sb.append("<tbody>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\"><h2>Sign In</h2>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td style=\"padding: 0px; margin: 0px;\" class=\"listHeader\">").append("<img src=\"").append(request.getContextPath()).append("/imageGrabber?file=gShield_Logo_small_final.png&width=0&height=0\" height=\"63\" width=\"80\">").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\">").append("\n");
        	sb.append("An Authorization Code has been sent to you. Enter it here to continue logging in...").append("\n");
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\">").append("\n");
        	sb.append("<form name=\"authCaptureForm\" id=\"authCaptureForm\" method=\"POST\" action=\"").append(request.getContextPath()).append("/TwoFactorAuthorizer\">").append("\n");
        	sb.append("<div><label for=\"authCode\">Authorization Code</label></div>").append("\n");
        	sb.append("<input size=\"20\" class=\"textfield margin-top5\" name=\"authorizerCode\" id=\"authorizerCode\" value=\"\" type=\"text\">").append("\n");
        	sb.append("<input type=\"hidden\" name=\"twoFaUserCredentials\" id=\"twoFaUserCredentials\" value=\"").append(twoFACredentials).append("\">");
        	sb.append("<input type=\"hidden\" name=\"deliverySelect\" id=\"deliverySelect\" value=\"!!deliverySelect!!\">");
        	sb.append("<input type=\"hidden\" name=\"emailAddresses\" id=\"emailAddresses\" value=\"").append(emailAddress).append("\">");
        	sb.append("<input type=\"hidden\" name=\"phoneNumbers\" id=\"phoneNumbers\" value=\"").append(phoneNumbers).append("\">");
        	sb.append("<div class=\"margin-top5\" nowrap=\"\"></div>").append("\n");
        	sb.append("<div>&nbsp;</div>").append("\n");
        	sb.append("<div align=\"left\"></div>").append("\n");
        	sb.append("<div>&nbsp;</div>").append("\n");
        	sb.append("<div class=\"loginAlignLeft\" id=\"ADVANCED_CHECKBOX\" style=\"display: block;\"></div>").append("\n");
        	sb.append("<div>&nbsp;</div>").append("\n");
        	sb.append("<div>").append("\n");
        	sb.append("<input class=\"buttonstylenormal margin-top5\" value=\"Sign In\" type=\"submit\">").append("\n");
        	sb.append("</div>").append("\n");
        	sb.append("</form>").append("\n");
        	sb.append("</td>").append("\n");
           	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td>").append("\n");
        	sb.append("<div>").append("\n");
        	sb.append("<label for=\"authCode\">To have another authorization code sent to you click <a href=\"#\" onclick=\"submitNewAuthCodeForm();return false;\">here.</a></label>").append("\n");
        	sb.append("</div>").append("\n");
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("</tbody>").append("\n");
        	sb.append("</table>").append("\n");
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr align=\"center\" valign=\"middle\">").append("\n");
        	sb.append("<td align=\"center\" class=\"copyright\">&copy;2017 GSI. All Rights Reserved.&emsp;</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("</table>").append("\n");
        	sb.append("<input type=\"hidden\" name=\"requestURI\" id=\"requestURI\" value=\"").append(request.getRequestURI()).append("\">").append("\n");

        	String hiddenMessage = request.getParameter("hiddenMessage");
        	if (hiddenMessage == null) {
        		hiddenMessage = "";
        	}
        	else {
        		hiddenMessage = GSIUtility.easyDecrypt(hiddenMessage);
        	}

        	InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("skeletons/2FASkeleton.html");
        	BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String output;
			while ((output = br.readLine()) != null) {
				osb.append(output).append("\n");
			}
			outputString = osb.toString();
			outputString = outputString.replaceAll("!!hiddenMessage!!", hiddenMessage);
			outputString = outputString.replaceAll("!!pageContents!!", sb.toString());
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			outputString = outputString.replaceAll("!!productLogo!!", "gShield_Logo_small_final.png");
        	
			//outputString = outputString.replaceAll("!!twoFaUserCredentials!!", request.getParameter("twoFaUserCredentials"));
			/*String emailAddress = request.getParameter("emailAddresses");
			if (emailAddress == null) {
				emailAddress = "";
			}
			outputString = outputString.replaceAll("!!emailAddresses!!", emailAddress);
			String phoneNumbers = request.getParameter("phoneNumbers");
			if (phoneNumbers == null) {
				phoneNumbers = "";
			}
			outputString = outputString.replaceAll("!!phoneNumbers!!", phoneNumbers);
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			if (request.getParameter("hiddenMessage") != null) {
				outputString = outputString.replaceAll("!!hiddenMessage!!", request.getParameter("hiddenMessage"));
			}
			else {
				outputString = outputString.replaceAll("!!hiddenMessage!!", "");
			}*/
        } 
        catch (Exception e) {
        	logger.error("", e);
        	outputString = "An error occurred showing main menu : " + e.getMessage();
        }
        finally {
    		response.setContentType("text/html");
    		response.setHeader("Cache-Control", "no-cache");
			out.print(outputString);
        	out.close();
        }
    }  	
}
