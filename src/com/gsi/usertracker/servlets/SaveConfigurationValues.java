package com.gsi.usertracker.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.utility.GSIUtility;
import com.ibatis.sqlmap.client.SqlMapClient;

public class SaveConfigurationValues extends HttpServlet {
	private static final long serialVersionUID = 401104958842731627L;
	private static final Logger logger = Logger.getLogger("prLogger");

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
		int hostPort = request.getServerPort();
    	String userName = "";
        try {
        	String jdeSystemDatabaseName = GSIUtility.getParam(request, "jdeSystemDatabaseName", "");
        	String jdeSystemConnectionURL = GSIUtility.getParam(request, "jdeSystemConnectionURL", "");
        	String jdeUserDatabaseName = GSIUtility.getParam(request, "jdeUserDatabaseName", "");
        	String jdeEnvironmentConnectionURL = GSIUtility.getParam(request, "jdeEnvironmentConnectionURL", "");
        	String jdeEnvironment = GSIUtility.getParam(request, "jdeEnvironment", "");
        	String dbmsUserId = GSIUtility.getParam(request, "dbmsUserId", "");
        	String dbmsUserPassword = GSIUtility.getParam(request, "dbmsUserPassword", "");
        	String jdeJDBCDriver = GSIUtility.getParam(request, "jdeJDBCDriver", "");
        	String jdeUserName = GSIUtility.getParam(request, "jdeUserName", "");
        	String emailFromAddress = GSIUtility.getParam(request, "emailFromAddress", "");
        	String twilioFromNumber = GSIUtility.getParam(request, "twilioFromNumber", "");
        	String noSendOptionsPassthru = GSIUtility.getParam(request, "noSendOptionsPassthru", "");
        	String authorizationType = GSIUtility.getParam(request, "authorizationType", "");
        	String trigger1 = GSIUtility.getParam(request, "trigger1", "");
        	String totpOrganization = GSIUtility.getParam(request, "totpOrganization", "");
        	String loggingLevel = GSIUtility.getParam(request, "loggingLevel", "");
        	
        	logger.info("1 - jdeSystemDatabaseName: " + jdeSystemDatabaseName);
        	logger.info("1 - jdeSystemConnectionURL: " + jdeSystemConnectionURL);
        	logger.info("1 - jdeUserDatabaseName: " + jdeUserDatabaseName);
        	logger.info("1 - jdeEnvironmentConnectionURL: " + jdeEnvironmentConnectionURL);
        	logger.info("1 - jdeEnvironment: " + jdeEnvironment);
        	logger.info("1 - dbmsUserId: " + dbmsUserId);
        	logger.info("1 - dbmsUserPassword: " + dbmsUserPassword);
        	logger.info("1 - jdeJDBCDriver: " + jdeJDBCDriver);
        	logger.info("1 - jdeUserName: " + jdeUserName);
        	logger.info("1 - emailFromAddress: " + emailFromAddress);
        	logger.info("1 - noSendOptionsPassthru: " + noSendOptionsPassthru);
        	logger.info("1 - authorizationType: " + authorizationType);
        	logger.info("1 - trigger1: " + trigger1);
        	logger.info("1 - totpOrganization: " + totpOrganization);
        	logger.info("1 - twilioFromNumber: " + twilioFromNumber);
        	logger.info("1 - loggingLevel: " + loggingLevel);

			SqlMapClient smc = TwoFactorAuthorizerDAO.getSqlMapClient();
			boolean foundIt = TwoFactorAuthorizerDAO.checkPropertyByKey("twilioFromNumber", hostPort);
			if (foundIt) {
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeSystemDatabaseName", jdeSystemDatabaseName, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeSystemConnectionURL", jdeSystemConnectionURL, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeUserDatabaseName", jdeUserDatabaseName, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeEnvironmentConnectionURL", jdeEnvironmentConnectionURL, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "dbmsUserId", dbmsUserId, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "dbmsUserPassword", dbmsUserPassword, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeJDBCDriver", jdeJDBCDriver, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "jdeUserName", jdeUserName, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "emailFromAddress", emailFromAddress, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "noSendOptionsPassthru", noSendOptionsPassthru, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "authorizationType", authorizationType, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "trigger1", trigger1, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "totpOrganization", totpOrganization, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "twilioFromNumber", twilioFromNumber, hostPort);
	        	TwoFactorAuthorizerDAO.updatePropertyNoOldValue(smc, "loggingLevel", loggingLevel, hostPort);
			}
			else {
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeSystemDatabaseName", jdeSystemDatabaseName, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeSystemConnectionURL", jdeSystemConnectionURL, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeUserDatabaseName", jdeUserDatabaseName, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeEnvironmentConnectionURL", jdeEnvironmentConnectionURL, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeEnvironment", jdeEnvironment, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "dbmsUserId", dbmsUserId, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "dbmsUserPassword", dbmsUserPassword, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeJDBCDriver", jdeJDBCDriver, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "jdeUserName", jdeUserName, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "emailFromAddress", emailFromAddress, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "noSendOptionsPassthru", noSendOptionsPassthru, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "authorizationType", authorizationType, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "trigger1", trigger1, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "totpOrganization", totpOrganization, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "twilioFromNumber", twilioFromNumber, hostPort);
	        	TwoFactorAuthorizerDAO.addSingleProperty(smc, "loggingLevel", loggingLevel, hostPort);
			}
        	
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");  //2030-01-01T05:00:00.000Z 23:59:59
			Properties props = new Properties();
			props.setProperty("jdeSystemDatabaseName", jdeSystemDatabaseName);
			props.setProperty("jdeSystemConnectionURL", jdeSystemConnectionURL);
			props.setProperty("jdeUserDatabaseName", jdeUserDatabaseName);
			props.setProperty("jdeEnvironmentConnectionURL", jdeEnvironmentConnectionURL);
			props.setProperty("jdeEnvironment", jdeEnvironment);
			props.setProperty("dbmsUserId", dbmsUserId);
			props.setProperty("dbmsUserPassword", dbmsUserPassword);
			props.setProperty("jdeJDBCDriver", jdeJDBCDriver);
			props.setProperty("jdeUserName", jdeUserName);
			props.setProperty("emailFromAddress", emailFromAddress);
			props.setProperty("noSendOptionsPassthru", noSendOptionsPassthru);
			props.setProperty("authorizationType", authorizationType);
			props.setProperty("trigger1", trigger1);
			props.setProperty("totpOrganization", totpOrganization);
			props.setProperty("twilioFromNumber", twilioFromNumber);
			props.setProperty("loggingLevel", loggingLevel);
			File f = new File(System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE) + "ConfigurationExport.properties");
			OutputStream out = new FileOutputStream( f );
			props.store(out, sdf.format(new Date()));
			out.flush();
			out.close();
			
        	logger.info("***** Configuration Saved *****");
		    String hiddenMessage = "&hiddenMessage=" + GSIUtility.easyEncrypt("Configuration has been saved. Please check system status..");
			String redirectURL = GSIUtility.getURL(request, "/ConfigureApplication", "?userName=" + GSIUtility.easyEncrypt(userName) + hiddenMessage);
		    logger.info("13.2 - redirectURL: " + redirectURL);
		    response.sendRedirect(redirectURL);
			return;
        } 
        catch (Exception e) {
        	try {
	    		logger.info("***** ERROR *****");
				logger.error("",e);
		        String sendOptions = "";
	        	String hiddenMessage = "&hiddenMessage=" + GSIUtility.easyEncrypt("An error has occurred saving configuration:" + e.getMessage());
		        String redirectURL = GSIUtility.getURL(request, "/ConfigureApplication", "?userName=" + userName + sendOptions + hiddenMessage);
	        	logger.info("13.2 - redirectURL: " + redirectURL);
	        	response.sendRedirect(redirectURL);
				return;
        	}
        	catch (Exception e1) {
        		logger.error("", e);
        	}
        }  
    }  	
}
