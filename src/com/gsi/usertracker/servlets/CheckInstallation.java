package com.gsi.usertracker.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.JDEDAO;
import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.utility.GSIUtility;

public class CheckInstallation extends HttpServlet {
	private static final long serialVersionUID = 3754085364052460468L;
	private static final Logger logger = Logger.getLogger("2faLogger");

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	logger.info("1 - In TwoFactorAuthorization CheckInstallation");
		int hostPort = request.getServerPort();
    	String outputString = "";
    	StringBuffer xsb = new StringBuffer();
		PrintWriter out = response.getWriter();
        try {
        	String gsiHomeLocation = "";
        	try {
		    	xsb.append(" - GSI 2FA Env Variable:");
		    	logger.info("1.5 - before System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE)");
		    	gsiHomeLocation = System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE);
		    	logger.info("1.5 - after System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE)");
        		if (gsiHomeLocation != null && gsiHomeLocation.trim().length() > 0) {
        			xsb.append(" is defined. Success").append("<br><br>");
        		}
        		else {
        			xsb.append(" has not been defined. NOT successful").append("<br><br>");
        		}
        	}
        	catch (Exception e) {
        		xsb.append(" NOT successful determining if GSI 2FA Env Variable has been defined. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());
    		
        	try {
        		if (gsiHomeLocation != null && gsiHomeLocation.trim().length() > 0) {
        			xsb.append(" - GSI 2FA Home Location:");
                	File f = new File(gsiHomeLocation);
                	if (f.exists() && f.isDirectory()) {
                		xsb.append(" has been created. Success").append("<br><br>");
                	}
                	else {
            			xsb.append(" has not been created, is not a directory. NOT successful").append("<br><br>");
                	}
        		}
        	}
        	catch (Exception e) {
        		xsb.append(" NOT successful determining if GSI 2FA Env Variable file system location exists. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());
    		
        	try {
        		if (gsiHomeLocation != null && gsiHomeLocation.trim().length() > 0) {
        			xsb.append(" - GSI 2FA Home Location:");
                	File f = new File(gsiHomeLocation);
                	if (f.canRead() && f.canWrite()) {
                		xsb.append(" has correct permissions. Success").append("<br><br>");
                	}
                	else {
            			xsb.append(" does not have correct permissions (either READ or WRITE). NOT successful").append("<br><br>");
                	}
        		}
        	}
        	catch (Exception e) {
        		xsb.append(" NOT successful determining if GSI 2FA Env Variable file system location has correct permissions. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());
        	
        	try {
    			xsb.append(" - GSI 2FA License Key:");
    			String licenseStatus = GSIUtility.checkLicensedDate(hostPort);
    			if (licenseStatus.equals(GSIUtility.LICENSEVALID)) {
	    			xsb.append(" is valid.").append("<br><br>");
    			}
    			else if (licenseStatus.equals(GSIUtility.LICENSEWARNING)) {
	    			xsb.append(" is within 30 days of expiring.").append("<br><br>");
    			}
    			else if (licenseStatus.equals(GSIUtility.LICENSEEXPIRED)) {
	    			xsb.append(" has expired.").append("<br><br>");
    			} 
        	}
        	catch (Exception e) {
        		xsb.append(" validation was NOT successful. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());

    		String jdeUserName = "";
        	try {
		    	xsb.append(" - Connection to Derby database:");
		    	jdeUserName = TwoFactorAuthorizerDAO.selectPropertyByKey("jdeUserName", hostPort);
		    	xsb.append(" successful.").append("<br><br>");
           	}
        	catch (Exception e) {
        		xsb.append(" NOT successful. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());

    		Integer addressNumber = 0;
        	try {
	        	xsb.append(" - Connection to JDE system database:");
	        	addressNumber = JDEDAO.getAddressNumber(jdeUserName.toUpperCase(), hostPort);
	        	xsb.append(" successful.").append("<br><br>");
        	}
        	catch (Exception e) {
        		xsb.append(" NOT successful. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());

    		try {
	        	xsb.append(" - Connection to JDE user environment database:");
		    	List emailAddress = JDEDAO.getEmailAddress(addressNumber, hostPort);
		    	xsb.append(" successful.").append("<br><br>");
        	}
        	catch (Exception e) {
        		xsb.append(" NOT successful. An error occured: " + e.getMessage()).append("<br><br>");
        	}
    		logger.info(xsb.toString());
	    	
        	StringBuffer sb = new StringBuffer();
        	sb.append("<tr>").append("\n");
        	sb.append("<td colspan=\"2\" align=\"center\">").append("\n");
        	sb.append("<table cellpadding=\"15\" cellspacing=\"0\" class=\"overlay\" width=\"528\">").append("\n");
        	sb.append("<tbody>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\"><h2>2FA System Status</h2>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td style=\"padding: 0px; margin: 0px;\" class=\"listHeader\">").append("<img src=\"").append(request.getContextPath()).append("/imageGrabber?file=gShield_Logo_small_final.png&width=&height=\" height=\"63\" width=\"80\">").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr>").append("\n");
        	sb.append("<td class=\"listHeader\" style=\"font-size: 12px;\">").append("\n");
        	sb.append(xsb.toString()).append("\n");
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("").append("\n");
        	sb.append("").append("\n");
        	sb.append("</tbody>").append("\n");
        	sb.append("</table>").append("\n");
        	sb.append("</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("<tr align=\"center\" valign=\"middle\">").append("\n");
        	sb.append("<td align=\"center\" class=\"copyright\">&copy;2017 GSI. All Rights Reserved.&emsp;</td>").append("\n");
        	sb.append("</tr>").append("\n");
        	sb.append("</table>").append("\n");

        	String hiddenMessage = request.getParameter("hiddenMessage");
        	if (hiddenMessage == null) {
        		hiddenMessage = "";
        	}
        	else {
        		hiddenMessage = GSIUtility.easyDecrypt(hiddenMessage);
        	}

        	InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("skeletons/2FASkeleton.html");
        	BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String output;
			StringBuffer osb = new StringBuffer();
			while ((output = br.readLine()) != null) {
				osb.append(output).append("\n");
			}
			outputString = osb.toString();
			outputString = outputString.replaceAll("!!hiddenMessage!!", hiddenMessage);
			outputString = outputString.replaceAll("!!pageContents!!", sb.toString());
			outputString = outputString.replaceAll("!!requestContextPath!!", request.getContextPath());
			outputString = outputString.replaceAll("!!productLogo!!", "gShield_Logo_small_final.png");
    	}
        catch (Exception e) {
    		logger.info("***** ERROR *****");
			logger.error("",e);
			outputString = "An error occured: " + e.getMessage();
        }  
        finally {
    		response.setContentType("text/html");
    		response.setHeader("Cache-Control", "no-cache");
			out.print(outputString);
        	out.close();
        }
    }  	
}
