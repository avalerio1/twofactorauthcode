package com.gsi.usertracker.servlets;

import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.utility.GSIUtility;

public class TwoFactorAuthorizer_ORIG extends HttpServlet {
	private static final Logger logger = Logger.getLogger(TwoFactorAuthorizer.class);	

    public void doGet(HttpServletRequest request, HttpServletResponse response) {  
        try {
        	String authorizerCode = GSIUtility.getParam(request, "authorizerCode", "");
        	String twoFaUserCredentials = GSIUtility.getParam(request, "twoFaUserCredentials", "");
        	String userName = null;

        	if (twoFaUserCredentials != null) {
        		twoFaUserCredentials = GSIUtility.easyDecrypt(twoFaUserCredentials);
	        	StringTokenizer st = new StringTokenizer(twoFaUserCredentials, ";");
	        	while(st.hasMoreTokens()) {
	        		String hold = st.nextToken();
	        		if (hold.indexOf(GSIUtility.USERNAME) > -1) {
	            		StringTokenizer st2 = new StringTokenizer(hold, ":");
	            		st2.nextToken();
	            		userName = st2.nextToken();
	        		}
	        	}
        	}
        	TwoFactorDomain tfd = new TwoFactorDomain();
        	if (authorizerCode != null) {
        		tfd.setAuthCode(authorizerCode);
        	}
        	if (userName != null) {
        		tfd.setUserName(userName);
        	}
        	tfd.setIpAddress(request.getRemoteAddr());
        	
        	List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthCode(tfd);
	        if (userDomain != null & userDomain.size() > 0) {
	        	tfd.setValid(1);
	        	TwoFactorAuthorizerDAO.updateValidCode(tfd);
	        	request.getSession().setAttribute("2FA", "false");
	        	
	        	StringBuffer userAndPasswordParams = new StringBuffer("?");
	        	StringTokenizer st = new StringTokenizer(twoFaUserCredentials, ";");
	        	while(st.hasMoreTokens()) {
	        		String hold = st.nextToken();
	        		StringTokenizer st2 = new StringTokenizer(hold, ":");
	        		userAndPasswordParams.append(st2.nextToken()).append("=").append(st2.nextToken()).append("&");
	        	}
	        	String redirectURL = GSIUtility.getURL(request, "/LogonBad.do", userAndPasswordParams.toString());
	        	if (redirectURL != null) {
	        		logger.info(redirectURL.toString());
					HttpServletResponse httpResponse = (HttpServletResponse) response;
					httpResponse.sendRedirect(redirectURL.toString());
					return;
	        	}
	        	else {
	        		// we should probably do something here....
	        	}
	        }
        } 
        catch (Exception e) {
			logger.error("",e);
            e.printStackTrace();
        }  
    }  	
}
