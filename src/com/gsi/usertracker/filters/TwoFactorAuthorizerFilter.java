package com.gsi.usertracker.filters;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.gsi.usertracker.dao.JDEDAO;
import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.Authorization;
import com.gsi.usertracker.model.PhoneNumberParts;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.usertracker.model.UserLoginTracker;
import com.gsi.utility.GSIUtility;
import com.gsi.utility.NetworkServerControl;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

public class TwoFactorAuthorizerFilter implements Filter {
	private static final Logger logger = Logger.getLogger("2faLogger");	
	private NetworkServerControl server;
	private String derbyHost;
	private String derbyPort;
	private ResourceBundle properties;
	/**
	 * Filter configuration
	 */
	@SuppressWarnings("unused")
	private FilterConfig config;
	
	/**
	 * The list of URI extensions to exclude from tracking
	 */
	private Set<String> exclusionExtensions = new HashSet<String>();
	private ArrayList<String> urlList;

	public void init(FilterConfig config) throws ServletException {
		//String prefix =  config.getServletContext().getRealPath("/");
		//PropertyConfigurator.configure(prefix + "log4.properties");
		this.config = config;
		try {
		    Properties props = new Properties(); 
		    try { 
		        InputStream configStream = getClass().getResourceAsStream( "/log4j.properties"); 
		        props.load(configStream); 
		        configStream.close(); 
		    } catch (IOException e) { 
				logger.error("Error can't load configuration file ");
		        System.out.println("Error can't load configuration file "); 
		    } 
		    props.setProperty("log4j.appender.2faFileAppender.File", System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE) + "gsi2FA.log"); 
		    LogManager.resetConfiguration(); 
		    PropertyConfigurator.configure(props); 
			logger.debug("2100-02-15: " + GSIUtility.easyEncrypt("2100-02-15"));

			properties = PropertyResourceBundle.getBundle("resources/gsitracking");
			if (logger.isDebugEnabled()) {
				logger.debug("Behavior Tracking Properties:");
			}
			//HashMap<String, String> triggersMap = new HashMap<String, String>(); 
			HashMap<String, String> propertiesMap = new HashMap<String, String>(); 
			// Iterate over our properties
			for (Object key : properties.keySet()) {
				String keyString = (String) key;
				//String value = getPropertyValue(properties.getProperty(keyString));
				String value = properties.getString(keyString);
				propertiesMap.put(keyString, value);
				if (logger.isDebugEnabled()) {
					logger.debug("\t" + keyString + " = " + value);
				}
				// Handle exclusion extensions
				if (keyString.equalsIgnoreCase("gsiTracking.excludeExtensions")) {
					StringTokenizer st = new StringTokenizer(value, ",", false);
					while (st.hasMoreTokens()) {
						String extension = st.nextToken();
						exclusionExtensions.add(extension);
						if (logger.isDebugEnabled()) {
							logger.debug("Adding exclusion extension: " + extension);
						}
					}
				}
				if (keyString.equalsIgnoreCase("excludeURI")) {
					String urls = value;
					StringTokenizer token = new StringTokenizer(urls, ",");
					urlList = new ArrayList<String>();
					while (token.hasMoreTokens()) {
						urlList.add(token.nextToken());
					}
					
				}
				/*if (keyString.indexOf("trigger") > -1) {
					triggersMap.put(keyString, properties.getString(keyString));
				}*/
			}
			
			//Check if this installation is the primary derby database
			String isPrimaryDerbyServer = properties.getString("isPrimaryDerbyServer");
			if (isPrimaryDerbyServer != null && isPrimaryDerbyServer.trim().equalsIgnoreCase("true")) {
				// Start server if it hasn't been started
				try {
					derbyHost = properties.getString("derbyHostName");
					derbyPort = properties.getString("derbyPortNumber");
				}
				catch (Exception e) {
					derbyHost = null;
					derbyPort = null;				
				}
				boolean isServerStarted = false;
				if ((derbyHost != null && derbyHost.trim().length() > 0) && (derbyPort != null && derbyPort.trim().length() > 0)) {
					logger.info("derbyHost and derbyPort were not null");
					try {
						if (server == null) {
							server = new NetworkServerControl(InetAddress.getByName(derbyHost), new Integer(derbyPort), "sa", "7D0AfVhv");
							if (isServerStarted(server, 1)) {
								isServerStarted = true;
								logger.info("INIT: Server is already started");
								//return;
							}
						}
					}
					catch (Exception localException) {
						logger.error("", localException);
					}
					//LocalizedResource localLocalizedResource = new LocalizedResource("org.apache.derby.loc.drda.servlet");
					if (!isServerStarted) {
						logger.info("INIT: Before Server start");
						runServer(null, null);
						logger.info("INIT: After Server start");
					}
				}
				else {
					logger.info("derbyHost and derbyPort WERE null");
				}
				TwoFactorAuthorizerDAO.checkDatabase(propertiesMap, 0);
			}
		} catch (Exception eio) {
			logger.error(" " + eio.getMessage(), eio);
		} 
	}

	public void destroy() {
		String isPrimaryDerbyServer = properties.getString("isPrimaryDerbyServer");
		if (isPrimaryDerbyServer != null && isPrimaryDerbyServer.trim().equalsIgnoreCase("true")) {
			config = null;
			try {
				if (server != null) {
					try {
						//Windows: connection = DriverManager.getConnection("jdbc:derby://localhost:1527/C:/Users/Test/DB_Name", us
						Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
						String derbyShutDownURL = properties.getString("derby.shutdown.url"); //TwoFactorAuthorizerDAO.selectPropertyByKey("derby.shutdown.url");
						if (derbyShutDownURL != null && derbyShutDownURL.trim().length() > 0) {
							if (derbyShutDownURL.indexOf("!!DERBYROOTLOCATION!!") > -1) {
								derbyShutDownURL = derbyShutDownURL.replaceAll("!!DERBYROOTLOCATION!!", System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE));
							}
							DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
							//jdbc:derby://10.128.2.105:1528//u01/home/derbyNetwork/PasswordResetDataStore/working/GSITracker;user=sa;password=7D0AfVhv;shutdown=true
							DriverManager.getConnection(derbyShutDownURL);
						}
					}
					catch (Exception e) {
						logger.info("Known Exception explicitly shutting down derby database.");					
						logger.info("Known Exception explicitly shutting down derby database.", e);
					}
					
					logger.info("DESTROY: Before Server shutdown");
					server.shutdown();
					logger.info("DESTROY: After Server shutdown");
				}
			}
			catch (Exception localException) {
				logger.error("", localException);
			}
		}
		/*config = null;
		try {
			if (server != null) {
				logger.info("DESTROY: Before Server shutdown");
				server.shutdown();
				logger.info("DESTROY: After Server shutdown");
			}
		}
		catch (Exception localException) {
			logger.error("", localException);
		}*/
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			int hostPort = request.getServerPort();
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			String uri = httpServletRequest.getRequestURI();
			String chainId = RandomStringUtils.random(8, true, true);
			logger.info("******** Chain id: " + chainId);
			//logRequestHeaders(httpServletRequest, chainId);
			//logRequestParameters(httpServletRequest, chainId);
			//logCookies(httpServletRequest, chainId);
			boolean skipURL = false;
			// Filter out resources
			boolean trackThisUri = true;
			if (uri.indexOf(".") != -1) {
				String extension = uri.toLowerCase().substring(uri.lastIndexOf('.') + 1);
				if (exclusionExtensions.contains(extension)) {
					// Skip this URI
					//logger.info("---->Skipping this URI, extension: "+extension);
					trackThisUri = false;
				}
			}
			if (trackThisUri) {
				if (!skipURL) {
					//logger.info("1 - Not skipping URL - " + httpServletRequest.getSession().getId());
					trackThisUri = false; // Reset the track this URI flag
					String user = getUserRequestParams(request);
                	String holdName = "";
					if (user != null && user.trim().length() > 0) {
						logger.info("3 - USER != null - TRYING TO LOG IN : " + user);
						trackThisUri = true;		// the user is trying to log in, need to check for authorization
						// log the user, ip address and time of the login
						try {
		                	StringTokenizer st = new StringTokenizer(user, ";");
		                	while(st.hasMoreTokens()) {
		                		String hold = st.nextToken();
		                		if (hold.indexOf(GSIUtility.USERNAME) > -1) {
			                		StringTokenizer st2 = new StringTokenizer(hold, ":");
			                		st2.nextToken();
			                		holdName = st2.nextToken();
		                		}
		                	}
						}
						catch (Exception e) {
							logger.error("",e);
							e.printStackTrace();
						}
					}
					else {
						//logger.info("4 - User == null");
					}
					if (holdName.equalsIgnoreCase("JDEP3MON")) {
						trackThisUri = false;
					}
	            	if (trackThisUri) {
	            	    UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));
	            	    Browser browser = userAgent.getBrowser();
	            	    String browserName = browser.getName();
	            	    //or 
	            	    // String browserName = browser.getGroup().getName();
	            	    Version browserVersion = userAgent.getBrowserVersion();
	            	    logger.info("The user is using browser " + browserName + " - version " + browserVersion.toStringComplete() + " - OS" + userAgent.getOperatingSystem().getName());
	            		if (httpServletRequest.getSession().getAttribute("jdeLogoutInterceptorSessionKey") == null) {  // only insert into the login tracking table for the 'first' request thru the filter. I'm assuming that the presence of this attribute signifies not the first request...		            	    
		            		//logger.info("5 - Here's the user: " + holdName);
			            	UserLoginTracker ult = new UserLoginTracker();
			            	ult.setUserName(holdName);
			            	ult.setIpAddress(httpServletRequest.getRemoteAddr());
			            	ult.setBrowser(browserName);
			            	ult.setBrowserVersion(browserVersion.toStringComplete());
			            	TwoFactorAuthorizerDAO.insertUserLoginTracker(ult); // time stamp is generated when the UserLoginTracker object is instantiated
			            	logger.info("6 - Inserted login into UserLoginTracker: " + ult.getUserName() + " " + ult.getIpAddress() + " " + ult.getLoginTime().toString());
	            		}
		            	
				        String userIPAddress = httpServletRequest.getRemoteAddr();
				        String userName = httpServletRequest.getParameter(GSIUtility.USERNAME);
		        		String authorizationType = TwoFactorAuthorizerDAO.selectPropertyByKey("authorizationType", hostPort);
		        		if (authorizationType.equals("2FA")) {
					        // now check if the user needs the two factor authorization input
					        TwoFactorDomain tfd = new TwoFactorDomain();
					        tfd.setIpAddress(userIPAddress);
					        tfd.setUserName(userName);
					        tfd.setBrowserType(browserName);
					        String adminAlways2FA = TwoFactorAuthorizerDAO.selectPropertyByKey("adminAlways2FA", hostPort);
					        logger.info("9.9 - adminAlways2FA:" + adminAlways2FA);
		            		List triggerCriteriaList = TwoFactorAuthorizerDAO.selectTriggerProperties();		            		
					        Authorization authorization = GSIUtility.determineAuthorization(tfd, triggerCriteriaList, adminAlways2FA);
					        logger.info("10 - AuthorizationType:" + authorization.getAuthorizationType());
					        if (authorization.getAuthorizationType() != null) {
					        	if (!authorization.getAuthorizationOption().equals(GSIUtility.IPLIST_CONDITION_ABSOLUTE)) { // IPLIST_CONDITION_ABSOLUTE acts like a firewall (never letting anyone in black list or not on white list access to the system regardless or 2FA)
				        			logger.info("10.1 - ****************** 8Ij9CFd1: " + ((HttpServletRequest)request).getSession().getAttribute("8Ij9CFd1"));
				        			String justAuthorized = GSIUtility.getParam(((HttpServletRequest)request), "8Ij9CFd1", "");
				        			if (justAuthorized == null || justAuthorized.trim().length() == 0 || (!justAuthorized.trim().equals(userName))) { // added '!justAuthorized.trim().equals(userName)' to check that user entered credentials used on initial login and authorization (Tom Gabriele found bug)
							        	logger.info("11 - Need Authorization");
							        	Integer addressNumber = JDEDAO.getAddressNumber(userName, hostPort);
							        	logger.info("12 - *** addressNumber: " + addressNumber);
							        	if (addressNumber != null && addressNumber > 0) {
								        	boolean hasEmail = false;
								        	boolean hasPhone = false;
								        	List emailAddresses = JDEDAO.getEmailAddress(addressNumber, hostPort);
								        	String emailAddressesParamString = "";
								        	if (emailAddresses != null && emailAddresses.size() > 0) {
								        		hasEmail = true;
								        		for (Iterator i = emailAddresses.iterator(); i.hasNext(); ) {
								        			String emailAddress = (String)i.next();
								        			emailAddressesParamString = emailAddressesParamString + GSIUtility.easyEncrypt(emailAddress) + "|" + GSIUtility.getMaskedEmailAddress(emailAddress) + ",";
								        			logger.info("13 - *** emailAddress: " + emailAddress);
								        		}
								        	}
									        else {
									        	logger.info("18 - *** no emailAddresses");
									        }
								        	List mobilePhoneNumberParts = JDEDAO.getMobilePhoneNumber(addressNumber, hostPort);
								        	String phoneNumbersParamString = "";
								        	PhoneNumberParts pnp = null;
								        	if (mobilePhoneNumberParts != null && mobilePhoneNumberParts.size() > 0) {
								        		hasPhone = true;
								        		for (Iterator i = mobilePhoneNumberParts.iterator(); i.hasNext(); ) {
								        			pnp = (PhoneNumberParts)i.next();
								        			phoneNumbersParamString = phoneNumbersParamString + GSIUtility.easyEncrypt("+1" + pnp.getNumberConcatenated()) + "|" + GSIUtility.getMaskedPhoneNumber(pnp.getNumberConcatenated()) + ",";
								        			logger.info("14 - *** PhoneNumberParts: " + pnp.toString());
								        			logger.info("14.1 - *** PhoneNumberPartsConcat: " + "+1" + pnp.getNumberConcatenated());
								        		}
								        	}
								        	else {
									        	logger.info("18.1 - *** no phone Numbers");
								        	}
								        	if (hasEmail || hasPhone) {
										        String sendOptions = "";
										        if (hasEmail) {
										        	sendOptions = sendOptions + "&emailAddresses=" + emailAddressesParamString;
										        }
										        if (hasPhone) {
										        	sendOptions = sendOptions + "&phoneNumbers=" + phoneNumbersParamString;
										        }
										        String redirectURL = GSIUtility.getURL(httpServletRequest, "/AuthSendOptions", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(user) + sendOptions);
									        	logger.info("13.2 - redirectURL: " + redirectURL);
												httpServletResponse.sendRedirect(redirectURL);
									        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
												return;
								        	}
								        	else {
									        	logger.info("18.9 - *** user had no email or phone number: " + addressNumber);
								        		// check the 'noSendOptionsPassthru' property to determine whether user goes to send options page with no options (and error message) or continues to log into the system
									        	if (TwoFactorAuthorizerDAO.selectPropertyByKey("noSendOptionsPassthru", hostPort).trim().equalsIgnoreCase("true")) { // passthru to logging into the system
									        		logger.info("18.9.0 - Passing thru to let user log in.");
									        		chain.doFilter(request, response);
									        	}
									        	else {
									        		logger.info("18.9.1 - Stopping user from logging in.");
											        String sendOptions = "&hiddenMessage=" + GSIUtility.easyEncrypt("You have no email address or mobile phone number in the system to send authorization code. Please contact the system administrator.");
											        String redirectURL = GSIUtility.getURL(httpServletRequest, "/AuthSendOptions", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(user) + sendOptions);
										        	logger.info("13.2 - redirectURL: " + redirectURL);
													httpServletResponse.sendRedirect(redirectURL);
										        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
													return;
									        	}
								        	}
								        }
								        else {
								        	logger.info("19 - *** addressNumber IS NULL OR 0: " + addressNumber);
								        	chain.doFilter(request, response);
								        }
					        		}
				        			else { // 2FA Admin check just authorized
							        	logger.info("19 - *** 2FA Admin check just authorized");
							        	chain.doFilter(request, response);
				        			}
					        	}
					        	else {
							        String sendOptions = "&hiddenMessage=" + GSIUtility.easyEncrypt("You are not authorized to access this system.");
							        String redirectURL = GSIUtility.getURL(httpServletRequest, "/AuthSendOptions", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(user) + sendOptions);
						        	logger.info("13.2 - redirectURL: " + redirectURL);
									httpServletResponse.sendRedirect(redirectURL);
						        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
									return;
					        	}
							}
					        else {
					        	//logger.info("#### no need for authorization" + chainId);
					        	chain.doFilter(request, response);
					        }
		        		}
		        		else if (authorizationType.equals("GA")) {
		        			logger.info("18.0 - ****************** 8Ij9CFd1: " + ((HttpServletRequest)request).getSession().getAttribute("8Ij9CFd1"));
		        			String justAuthorized = GSIUtility.getParam(((HttpServletRequest)request), "8Ij9CFd1", "");
		        			if (justAuthorized == null || justAuthorized.trim().length() == 0 || (!justAuthorized.trim().equals(userName))) { // added '!justAuthorized.trim().equals(userName)' to check that user entered credentials used on initial login and authorization (Tom Gabriele found bug)
						        TwoFactorDomain tfd = new TwoFactorDomain();
						        tfd.setUserName(userName);
						        tfd.setValid(1);
						        List userDomainList = TwoFactorAuthorizerDAO.checkUserDomainForAuthorized(tfd);
						        String needQRCode = "true"; 
						        if (userDomainList != null && userDomainList.size() > 0) {
						        	needQRCode = "false";
						        }
	
			        			String sendOptions = "&xyz=" + needQRCode;
						        String redirectURL = GSIUtility.getURL(httpServletRequest, "/QRCapture", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(user) + sendOptions);
					        	logger.info("13.2 - redirectURL: " + redirectURL);
								httpServletResponse.sendRedirect(redirectURL);
					        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
								return;
		        			}
		        			else {
					        	chain.doFilter(request, response);
		        			}
		        		}
		        		else { // authorizationType in config is incorrect
					        String sendOptions = "&hiddenMessage=" + GSIUtility.easyEncrypt("Authorization Type in the configuration is incorrect: " + authorizationType + ". Please fix before continuing.");
					        String redirectURL =GSIUtility.getURL(httpServletRequest, "/AuthSendOptions", "?twoFaUserCredentials=" + GSIUtility.easyEncrypt(user) + sendOptions);
				        	logger.info("13.2 - redirectURL: " + redirectURL);
							httpServletResponse.sendRedirect(redirectURL);
				        	//logger.info("14 - After redirect  - " + httpServletRequest.getSession().getId());
							return;
		        		}
					}
					else {
						//logger.info("8 - NOT Tracking the url: " + uri);
						// Pass the request down the chain
						chain.doFilter(request, response);
					}
				}
				else {
					//logger.info("2 - Skipping URL");
					// Pass the request down the chain
					chain.doFilter(request, response);
				}
			}
			else {
				// Pass the request down the chain
				chain.doFilter(request, response);			
			}
		}
		catch (Exception e) {
			logger.error("",e);
			e.printStackTrace();
			chain.doFilter(request, response);
		}
	}

	private String getUserRequestParams(ServletRequest request) {
		Map<String, String[]> parmmap;
		try {
			parmmap = request.getParameterMap();
			Set<Entry<String, String[]>> params = parmmap.entrySet();
			int howManyGot = 0;
			StringBuffer sb = new StringBuffer();
			boolean firstTime = true;
			for (Entry<String, String[]> entry : params) {
				//emailAddressPage
				//if (entry.getKey().equals("User")) {
				if (entry.getKey().equals(GSIUtility.USERNAME)) {
					if (!firstTime) {
						sb.append(";");
					}
					else {
						firstTime = false;
					}
					sb.append(GSIUtility.USERNAME).append(":").append(entry.getValue()[0]);
					howManyGot++;
				}
				if (entry.getKey().equals(GSIUtility.PASSWORD)) {
					if (!firstTime) {
						sb.append(";");
					}
					else {
						firstTime = false;
					}
					sb.append(GSIUtility.PASSWORD).append(":").append(entry.getValue()[0]);
					howManyGot++;
				}
				if (entry.getKey().equals(GSIUtility.ENVIRONMENT)) {
					if (!firstTime) {
						sb.append(";");
					}
					else {
						firstTime = false;
					}
					sb.append(GSIUtility.ENVIRONMENT).append(":").append(entry.getValue()[0]);
					howManyGot++;
				}
				if (entry.getKey().equals(GSIUtility.ROLE)) {
					if (!firstTime) {
						sb.append(";");
					}
					else {
						firstTime = false;
					}
					sb.append(GSIUtility.ROLE).append(":").append(entry.getValue()[0]);
					howManyGot++;
				}
				if (howManyGot == 4) {
					break;
				}
			}
			return sb.toString();
		}
		catch (Exception e) {
			logger.error("", e);
			return "";
		}
	}
	
	private void runServer(String paramString, PrintWriter paramPrintWriter) throws Exception {
		final Runnable local1 = new Runnable() {
			public void run() {
				try {
					Properties p = System.getProperties();
					p.setProperty("derby.database.sqlAuthorization", "true");
					p.setProperty("derby.connection.requireAuthentication", "true");
					p.setProperty("derby.authentication.provider", "BUILTIN");
					p.setProperty("derby.user.sa", "7D0AfVhv");
					p.setProperty("derby.stream.error.logSeverityLevel", "10000"); // if issues, set to "0"
					p.setProperty("derby.stream.error.extendedDiagSeverityLevel", "10000");
					p.setProperty("derby.stream.error.logBootTrace", "true");

					String value = properties.getString("derby.error.logfile.location");
					value = value.replaceAll("!!DERBYROOTLOCATION!!", System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE));
					p.setProperty("derby.stream.error.file", value);

					NetworkServerControl localNetworkServerControl = new NetworkServerControl(InetAddress.getByName(derbyHost), new Integer(derbyPort));
					localNetworkServerControl.start(null);
				}
				catch (Exception localException) {
					logger.error("",localException);
					throw new RuntimeException(localException.getMessage());
				}
			}
		};
		Thread localThread = null;
		try {
			localThread = (Thread)AccessController.doPrivileged(new PrivilegedExceptionAction() {
				public Thread run() throws Exception {
					return new Thread(local1);
				}
			});
		}
		catch (Exception localException1) {
			logger.error("",localException1);
			throw new RuntimeException(localException1.getMessage());
		}
		localThread.start();
		try {
			int i = 0;
			int j = 0;
			do {
				j++;
				try {
					Thread.sleep(100L);
				}
				catch (InterruptedException localInterruptedException) {
					logger.error("", localInterruptedException);
					throw new Exception(localInterruptedException);
				}
				try {
					if (isServerStarted(server, 1)) {
						i = 1;
					}
				}
				catch (Exception localException3) {
					logger.error("", localException3);
				}
			}
			while ((i == 0) && (j < 20));
			if (j >= 20) {
				logger.error("Don't know what this is");
				throw new Exception("Don't know what this is");
			}
		}
		catch (Exception localException2) {
			logger.error("", localException2);
			throw new Exception(localException2.getMessage());
		}
	}
	
	private static boolean isServerStarted(NetworkServerControl paramNetworkServerControl, int paramInt) {
		int i = 1;
		while (i <= paramInt)
			try {
				Thread.sleep(500L);
				paramNetworkServerControl.ping();
				return true;
			}
		catch (Exception localException) {
			if (i == paramInt)
				return false;
			i++;
		}
		return false;
	}

	private void logRequestParameters(ServletRequest request, String chainId) {
		logger.info("REQUEST PARAMETERS START ******************");
		Map<String, String[]> sortedParams = sortRequestParameters(request);
		for (Map.Entry<String, String[]> entry : sortedParams.entrySet()) {
			StringBuilder builder = new StringBuilder();
			for (String s : entry.getValue()) {
				builder.append(s);
				builder.append(", ");
			}
			logger.info("^^^^ " + chainId + "  " + entry.getKey() + ": " + builder.toString());
		}
		logger.info("REQUEST PARAMETERS END ********************");
	}
	
	private Map<String, String[]> sortRequestParameters(ServletRequest request) {
		Map<String, String[]> sortedParams = new TreeMap<String, String[]>();
		Map<String, String[]> parmmap = request.getParameterMap();
		Set<Entry<String, String[]>> params = parmmap.entrySet();
		for (Entry<String, String[]> entry : params) {
			sortedParams.put(entry.getKey(), entry.getValue());
		}
		return sortedParams;
	}

	private Cookie getCookieValue(HttpServletRequest request, String cookieName) {
		Cookie returnedCookie = null;
		if (request.getCookies() != null) {
			// Go through cookies and retrieve GSITracker
			for (Cookie cookie : request.getCookies()) {
				String name = cookie.getName();
				if (name.equalsIgnoreCase(cookieName)) {
					// Found the cookie
					returnedCookie = cookie;
				}
			}
		}
		return returnedCookie;
	}
	
	private String getPropertyValue(String value) {
		if (value.startsWith("${")) {
			String systemProperty = value.substring(2, value.length() - 1);
			value = System.getProperty(systemProperty);
		}
		return value;
	}
	
	private void logRequestHeaders(HttpServletRequest request, String chainId) {
		logger.info("REQUEST HEADERS START ******************");
		Enumeration<String> headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			String key = headers.nextElement();
			String value = request.getHeader(key);
			logger.info("^^^^ " + chainId + "  " + "   Key:  |"+key+"|   Value:  |"+value+"|");
		}
		logger.info("REQUEST HEADERS END ********************");
	}
	
	private void logCookies(HttpServletRequest request, String chainId) {
		logger.info("COOKIES START ******************");
		if (null != request.getCookies()) {
			for (Cookie cookie : request.getCookies()) {
				String description = "";
				try {
					description = BeanUtils.describe(cookie).toString();
				} catch (Exception e) {
					logger.error("BeanUtils Exception describing cookie", e);
				}
				logger.info("^^^^ " + chainId + "  " + description);
			}
		}
		logger.info("COOKIES END ********************");
	}
	
	private void logSessionAttributes(HttpServletRequest request, String chainId) {
		logger.info("SESSION ATTRIBUTES START ******************");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(request.getSession().getCreationTime());
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		logger.info("SESSION " + request.getSession().getId() + " created at: " + dfm.format(cal.getTime()));
		logger.info("SESSION ATTRIBUTES: ");
		Map<String, Object> sortedAttrs = sortSessionAttributes(request);
		for (Map.Entry<String, Object> entry : sortedAttrs.entrySet()) {
			String description = "";
			try {
				description = BeanUtils.describe(entry.getValue()).toString();
			} catch (Exception e) {
				logger.error("BeanUtils Exception describing attribute " + entry.getKey(), e);
			}
			logger.info("^^^^ " + chainId + "  " + " " + entry.getKey() + ": " + description);
		}
		logger.info("SESSION ATTRIBUTES END ********************");
	}
	
	private Map<String, Object> sortSessionAttributes(HttpServletRequest request) {
		Map<String, Object> sortedAttrs = new TreeMap<String, Object>();
		Enumeration<String> attrEnum = request.getSession().getAttributeNames();
		while (attrEnum.hasMoreElements()) {
			String s = attrEnum.nextElement();
			sortedAttrs.put(s, request.getAttribute(s));
		}
		return sortedAttrs;
	}
}