package com.gsi.usertracker.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.gsi.usertracker.model.PhoneNumberParts;


public class JDEDAO {
	private static final Logger logger = Logger.getLogger("2faLogger");	

	public JDEDAO() {
	}

	public static Connection getSystemConnection(int hostPort) {
		try {
			String connectionUrl1 = TwoFactorAuthorizerDAO.selectPropertyByKey("jdeSystemConnectionURL", hostPort);
	    	logger.info("1.0 - before Class.forName, host port:" + hostPort);
			Class.forName(TwoFactorAuthorizerDAO.selectPropertyByKey("jdeJDBCDriver", hostPort));
	    	logger.info("1.1 - after Class.forName");
			String userId = TwoFactorAuthorizerDAO.selectPropertyByKey("dbmsUserId", hostPort);
			String password = TwoFactorAuthorizerDAO.selectPropertyByKey("dbmsUserPassword", hostPort);
	    	logger.info("1.2 - before DriverManager.getConnection");
			return DriverManager.getConnection(connectionUrl1, userId, password);
		}
		catch (Exception e) {
	    	logger.info("1.3 - exception DriverManager.getConnection: ", e);
			logger.error("", e);
			return null;
		}
	}

	public static Connection getEnvironmentConnection(int hostPort) {
		try {
			String connectionUrl1 = TwoFactorAuthorizerDAO.selectPropertyByKey("jdeEnvironmentConnectionURL", hostPort);
			Class.forName(TwoFactorAuthorizerDAO.selectPropertyByKey("jdeJDBCDriver", hostPort));
			String userId = TwoFactorAuthorizerDAO.selectPropertyByKey("dbmsUserId", hostPort);
			String password = TwoFactorAuthorizerDAO.selectPropertyByKey("dbmsUserPassword", hostPort);
			logger.info("1 - Connection info: ");
			logger.info("  1.1 - connectionUrl1: " + connectionUrl1);
			logger.info("  1.2 - userId: " + userId);
			logger.info("  1.3 - password: " + password);
			return DriverManager.getConnection(connectionUrl1, userId, password);
		}
		catch (Exception e) {
			logger.error("", e);
			return null;
		}
	}

	public static Integer getAddressNumber(String userName, int hostPort) throws Exception {
		Connection con1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String systemDatabaseName = TwoFactorAuthorizerDAO.selectPropertyByKey("jdeSystemDatabaseName", hostPort);
			con1 = getSystemConnection(hostPort);
			stmt = con1.createStatement();
			String sqlStatement = "select ULAN8 from " + systemDatabaseName + ".F0092 where ULUSER = '"+ userName + "'";
	        rs = stmt.executeQuery(sqlStatement);
	        Integer addressNumber = 0;
	        while (rs.next()) {
	        	addressNumber = rs.getInt("ULAN8");
	        }
	        return addressNumber;
		}
		catch (Exception e) {
			logger.error("", e);
			throw e;
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (Exception e) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} 
				catch (Exception e) {
				}
			}
			if (con1 != null) {
				try {
					con1.close();
				} 
				catch (Exception e) {
				}
			}
		}
	}
	
	public static List getEmailAddress(Integer addressNumber, int hostPort) throws Exception {
		Connection con1 = null;
		//Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<String> emailList = new ArrayList();
		try {
			logger.info("1.4 - address number: " + addressNumber);
			String userDatabaseName = TwoFactorAuthorizerDAO.selectPropertyByKey("jdeUserDatabaseName", hostPort);
			con1 = getEnvironmentConnection(hostPort);
			//stmt = con1.createStatement();
			String sqlStatement  = "";
			if (userDatabaseName.indexOf(";") > -1) {
				StringBuffer sb = new StringBuffer();
				StringTokenizer st = new StringTokenizer(userDatabaseName, ";");
				boolean firstTime = true;
				while(st.hasMoreTokens()) {
					if (!firstTime) {
						sb.append(" union ");
					}
					sb.append(" select EAEMAL from ").append(st.nextToken().trim()).append(".f01151 where eaan8 = ").append(addressNumber);
					firstTime = false;
				}
				sqlStatement = sb.toString();
			}
			else {
				sqlStatement = "select EAEMAL from " + userDatabaseName + ".f01151 where eaan8 = " + addressNumber;
			}
			logger.info("1.5 - email address SQL statement: " + sqlStatement);
			ps = con1.prepareStatement(sqlStatement);
			rs = ps.executeQuery();
	        while (rs.next()) {
	        	emailList.add(rs.getString("EAEMAL"));
	        }
	        return emailList;
		}
		catch (Exception e) {
			logger.error("", e);
			throw e;
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (Exception e) {
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} 
				catch (Exception e) {
				}
			}
			if (con1 != null) {
				try {
					con1.close();
				} 
				catch (Exception e) {
				}
			}
		}
	}
	
	public static List<PhoneNumberParts> getMobilePhoneNumber(Integer addressNumber, int hostPort) throws Exception {
		Connection con1 = null;
		//Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<PhoneNumberParts> phoneList = new ArrayList();
		try {
			String userDatabaseName = TwoFactorAuthorizerDAO.selectPropertyByKey("jdeUserDatabaseName", hostPort);
			con1 = getEnvironmentConnection(hostPort);
			//stmt = con1.createStatement();
			String sqlStatement = "";
			if (userDatabaseName.indexOf(";") > -1) {
				StringBuffer sb = new StringBuffer();
				StringTokenizer st = new StringTokenizer(userDatabaseName, ";");
				boolean firstTime = true;
				while(st.hasMoreTokens()) {
					if (!firstTime) {
						sb.append(" union ");
					}
					sb.append(" select WPAR1 as areaCode, WPPH1 as phoneNumber from ").append(st.nextToken().trim()).append(".f0115 where wpan8 = ").append(addressNumber).append("  and WPPHTP = 'CAR'");
					firstTime = false;
				}
				sqlStatement = sb.toString();
			}
			else {			
				sqlStatement = "select WPAR1 as areaCode, WPPH1 as phoneNumber from " + userDatabaseName + ".f0115 where wpan8 = " + addressNumber + " and WPPHTP = 'CAR'";
			}
			//logger.info("getting Phone SQL:" + sqlStatement);
			ps = con1.prepareStatement(sqlStatement);
	        rs = ps.executeQuery();
	        while (rs.next()) {
	        	PhoneNumberParts pnp = new PhoneNumberParts();
	        	pnp.setAreaCode(rs.getString("areaCode"));
	        	pnp.setPhoneNumber(rs.getString("phoneNumber"));
	        	phoneList.add(pnp);
	        }
	        return phoneList;
		}
		catch (Exception e) {
			logger.error("", e);
			throw e;
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (Exception e) {
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} 
				catch (Exception e) {
				}
			}
			if (con1 != null) {
				try {
					con1.close();
				} 
				catch (Exception e) {
				}
			}
		}
	}
}
