package com.gsi.usertracker.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.gsi.usertracker.model.PasswordResetTracker;
import com.gsi.usertracker.model.Property;
import com.gsi.usertracker.model.PropertyAudit;
import com.gsi.usertracker.model.SuperAdminUser;
import com.gsi.usertracker.model.SuperAdminUserAudit;
import com.gsi.usertracker.model.TriggerCriteria;
import com.gsi.usertracker.model.TwoFactorDomain;
import com.gsi.usertracker.model.UserLoginTracker;
import com.gsi.utility.GSIIbatisConnectionUtility;
import com.gsi.utility.GSIUtility;
import com.ibatis.sqlmap.client.SqlMapClient;

public class TwoFactorAuthorizerDAO {
	private static final Logger logger = Logger.getLogger("2faLogger");	
	private static boolean usingSecondaryDerbyInstance = false;
	public TwoFactorAuthorizerDAO() {
	}

	public static SqlMapClient getSqlMapClient() throws Exception {
		SqlMapClient smc = null;
		try {
			smc = GSIIbatisConnectionUtility.getPrimaryDerbyGSISqlMapClient();
			try {
				smc.queryForObject("checkDatabase");
				setUsingSecondaryDerbyInstance(false);
			}
			catch (SQLException sqle) {
				String theError = (sqle).getSQLState();
				if (theError.equals("08001")) { // Derby database is unreachable
					throw new Exception("Unable to reach primary Derby database.");
					/*if (TwoFactorAuthorizerDAO.selectPropertyByKey("useSecondaryDerbyInstance") != null && TwoFactorAuthorizerDAO.selectPropertyByKey("useSecondaryDerbyInstance").equalsIgnoreCase("true")) {
						 smc = GSIIbatisConnectionUtility.getSecondaryDerbyGSISqlMapClient();
						 try {
								smc.queryForObject("checkDatabase");
								setUsingSecondaryDerbyInstance(true);
						 }
						 catch (SQLException sqle2) {
							 String theError2 = (sqle2).getSQLState();
							 if (theError2.equals("08001")) { // Derby database is unreachable
								 throw new Exception("Primary and Secondary Derby databases are unreachable.");
							 }
							 else {
								 setUsingSecondaryDerbyInstance(true);
							 }
						 }
					}
					else {
						throw new Exception("Unable to reach primary Derby database and we're not using a secondary instance.");
					}*/
				}
			}
		}
		catch (Exception e) {
			logger.error("", e);
			throw new Exception("Error instantiating Derby SqlMapClient.");
		}
		return smc;
	}
	
	public static List checkUserDomainForAuthorized(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("checkUserDomainForAuthorized", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static List selectAllUserDomain() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectAllUserDomain");
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void createUserDomainTable(SqlMapClient smc) throws Exception {
		smc.update("createUserDomain");
	}
	
	public static void dropUserDomainTable(SqlMapClient smc) throws Exception {
		smc.update("dropUserDomain");
	}
	
	public static List checkUserDomainForAuthCode(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("checkUserDomainForAuthCode", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void insertUserDomain(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("insertUserDomain", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static int deleteUserDomain(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.delete("deleteUserDomain", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static int deleteUserDomainForTOTP(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.delete("deleteUserDomainForTOTP", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static void deleteUserDomainFor2FA(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("deleteUserDomainFor2FA", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static int updateValidCode(TwoFactorDomain tfd) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		int i = 0;
		if (smc != null) {
			i = smc.update("updateValidCode", tfd);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return i;
	}

	public static void createUserLoginTrackerTable(SqlMapClient smc) throws Exception {
		smc.update("createUserLoginTracker");
	}
	
	public static void dropUserLoginTrackerTable(SqlMapClient smc) throws Exception {
		smc.update("dropUserLoginTracker");
	}
	
	public static void insertUserLoginTracker(UserLoginTracker ult) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("insertUserLoginTracker", ult);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static List selectAllUserLoginTracker() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectAllUserLoginTracker");
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	/*public static void createTriggerCriteriaTable(SqlMapClient smc) throws Exception {
		smc.update("createTriggerCriteria");
	}
	
	public static void dropTriggerCriteriaTable(SqlMapClient smc) throws Exception {
		smc.update("dropTriggerCriteria");
	}
	
	public static List getTriggerCriteria() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectTriggerCriteria");
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}	*/

	public static void createAdmin2FATable(SqlMapClient smc) throws Exception {
		smc.update("createAdmin2FA");
	}
	
	public static void dropAdmin2FATable(SqlMapClient smc) throws Exception {
		smc.update("dropAdmin2FA");
	}
	
	public static void insertAdmin2FA(String userName) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("insertAdmin2FA", userName);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void deleteAdmin2FA(String userName) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("deleteAdmin2FA", userName);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static List selectAllAdmin2FA() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectAdmin2FA");
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static void createSuperAdminUserTable(SqlMapClient smc) throws Exception {
		smc.update("createSuperAdminUserTable");
	}
	
	public static void createSuperAdminUserAuditTable(SqlMapClient smc) throws Exception {
		smc.update("createSuperAdminUserAuditTable");
	}
	
	public static void dropSuperAdminUserTable(SqlMapClient smc) throws Exception {
		smc.update("dropSuperAdminUserTable");
	}
	
	public static void insertSuperAdminUser(SuperAdminUser sau, String modifyUser) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("insertSuperAdminUser", sau);
			SuperAdminUserAudit saua = new SuperAdminUserAudit();
			saua.setAdminUserName(sau.getAdminUserName());
			saua.setAdminPassword(sau.getAdminPassword());
			saua.setChangeUserName(modifyUser);
			saua.setAction("insert");
			smc.insert("insertSuperAdminUserAudit", saua);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void updateSuperAdminUser(SuperAdminUser sau, String modifyUser) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("updateSuperAdminUser", sau);
			SuperAdminUserAudit saua = new SuperAdminUserAudit();
			saua.setAdminUserName(sau.getAdminUserName());
			saua.setAdminPassword(sau.getAdminPassword());
			saua.setChangeUserName(modifyUser);
			saua.setAction("update");
			smc.insert("insertSuperAdminUserAudit", saua);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void deleteSuperAdminUser(SuperAdminUser sau, String modifyUser) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("deleteSuperAdminUser", sau);
			SuperAdminUserAudit saua = new SuperAdminUserAudit();
			saua.setAdminUserName(sau.getAdminUserName());
			saua.setAdminPassword(sau.getAdminPassword());
			saua.setChangeUserName(modifyUser);
			saua.setAction("delete");
			smc.insert("insertSuperAdminUserAudit", saua);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static List selectSuperAdminUser(SuperAdminUser sau) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectSuperAdminUser", sau);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static List selectAllSuperAdminUsers() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectAllSuperAdminUsers");
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static void createPropertiesTable(SqlMapClient smc) throws Exception {
		smc.update("createPropertiesTable");
	}
	
	public static void createPropertiesAuditTable(SqlMapClient smc) throws Exception {
		smc.update("createPropertiesAuditTable");
	}
	
	public static void dropPropertiesTable(SqlMapClient smc) throws Exception {
		smc.update("dropPropertiesTable");
	}
	
	public static void insertProperty(Property prop, String modifyUser, int hostPort) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("insertProperty", prop);
			PropertyAudit pa = new PropertyAudit();
			pa.setKeyValue(prop.getKeyValue());
			pa.setValueValue(prop.getValueValue());
			pa.setChangeUserName(modifyUser);
			pa.setAction("insert");
			pa.setHostPort(hostPort);
			smc.insert("insertPropertyAudit", pa);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void deleteProperty(Property prop, String modifyUser, int hostPort) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.delete("deleteProperty", prop);
			PropertyAudit pa = new PropertyAudit();
			pa.setKeyValue(prop.getKeyValue());
			pa.setValueValue(prop.getValueValue());
			pa.setChangeUserName(modifyUser);
			pa.setAction("delete");
			pa.setHostPort(hostPort);
			smc.insert("insertPropertyAudit", pa);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	public static void updateProperty(Property prop, String modifyUser, int hostPort) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.update("updateProperty", prop);
			PropertyAudit pa = new PropertyAudit();
			pa.setKeyValue(prop.getKeyValue());
			pa.setValueValue(prop.getValueValue());
			pa.setChangeUserName(modifyUser);
			pa.setAction("update");
			pa.setHostPort(hostPort);
			smc.insert("insertPropertyAudit", pa);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}
	
	/*public static String selectPropertyByKey(String key) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		String value = null;
		if (smc != null) {
			List list = smc.queryForList("selectPropertyByKey", key);
			if (list != null && list.size() > 0) {
				value = ((Property)list.get(0)).getValueValue();
			}
			else {
				throw new Exception("Property: " + key + " not found.");
			}
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return value;
	}*/
	
	public static List selectTriggerProperties() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		List<TriggerCriteria> triggerList = new ArrayList<TriggerCriteria>();
		if (smc != null) {
			List<Property> triggerProperties = smc.queryForList("selectTriggerProperties");
			if (triggerProperties != null && triggerProperties.size() > 0) {
				TriggerCriteria tc = new TriggerCriteria();
				for (Iterator<Property> i = triggerProperties.iterator(); i.hasNext();) {
					Property prop = (Property)i.next();
					String value = prop.getValueValue();
					if (value.indexOf("|") > -1) {
						StringTokenizer st = new StringTokenizer(value, "|");
						if (st.countTokens() != 2) {
							throw new Exception("Trigger definition in properties file is incorrect!");
						}
						tc.setTriggerType(st.nextToken());
						tc.setTriggerCriteria(st.nextToken());
					}
					else {
						tc.setTriggerType(value);
						tc.setTriggerCriteria("");
					}
					triggerList.add(tc);
				}
			}
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return triggerList;
	}
	
	public static List selectAllProperties() throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.queryForList("selectAllProperties");
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	/*public static void createPasswordResetTrackerTable(SqlMapClient smc) throws Exception {
		smc.update("createPasswordResetTrackerTable");
	}
	
	public static PasswordResetTracker selectPasswordResetCounterForIPAddress(String ipAddress) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		PasswordResetTracker passwordResetTracker = null;
		if (smc != null) {
			List list = smc.queryForList("selectPasswordResetCounterForIPAddress", ipAddress);
			if (list != null && list.size() > 0) {
				passwordResetTracker = ((PasswordResetTracker)list.get(0));
			}
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return passwordResetTracker;
	}
	
	public static void insertPasswordResetCounter(PasswordResetTracker passwordResetTracker) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			smc.insert("insertPasswordResetCounter", passwordResetTracker);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static int deletePasswordResetCounter(String  ipAddress) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		if (smc != null) {
			return smc.delete("deletePasswordResetCounter", ipAddress);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
	}

	public static int updatePasswordResetCounterForIPAddress(PasswordResetTracker passwordResetTracker) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		int i = 0;
		if (smc != null) {
			i = smc.update("updatePasswordResetCounterForIPAddress", passwordResetTracker);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return i;
	}
	
	public static int updatePasswordResetBlockTimeForIPAddress(PasswordResetTracker passwordResetTracker) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		int i = 0;
		if (smc != null) {
			i = smc.update("updatePasswordResetBlockTimeForIPAddress", passwordResetTracker);
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return i;
	}*/
	

	public static void checkDatabase(HashMap<String, String> properties, int hostPort) {
		try {
			SqlMapClient smc = getSqlMapClient();
			if (smc != null) {
				try {
					if (!wwdChk4Table(smc, "checkUserDomain")) {
						createUserDomainTable(smc);
					}
					if (!wwdChk4Table(smc, "checkUserLoginTracker")) {
						createUserLoginTrackerTable(smc);
					}
					/*if (!wwdChk4Table(smc, "checkTriggerCriteria")) {
						createTriggerCriteriaTable(smc);
						addTriggerData(smc, triggers);
					}*/
					if (!wwdChk4Table(smc, "checkAdmin2FA")) {
						createAdmin2FATable(smc);
					}
					if (!wwdChk4Table(smc, "checkSuperAdminUser")) {
						createSuperAdminUserTable(smc);
					}
					if (!wwdChk4Table(smc, "checkSuperAdminUserAudit")) {
						createSuperAdminUserAuditTable(smc);
					}
					if (!wwdChk4Table(smc, "checkProperties")) {
						createPropertiesTable(smc);
						addPropertyData(smc, properties, hostPort);
					}
					if (!wwdChk4Table(smc, "checkPropertiesAudit")) {
						createPropertiesAuditTable(smc);
					}
					/*if (!wwdChk4Table(smc, "checkPasswordResetCounter")) {
						createPasswordResetTrackerTable(smc);
					}*/
				}
				catch (Exception e1) {
					logger.error("",e1);
					e1.printStackTrace();
				}
			}
		} 
		catch (Exception e) {
			logger.error("",e);
			e.printStackTrace();
		}
	}
	
	public static boolean wwdChk4Table (SqlMapClient smc, String statement) throws SQLException {
		try {
			smc.queryForList(statement);
		}
		catch (SQLException sqle) {
			String theError = (sqle).getSQLState();
			//   System.out.println("  Utils GOT:  " + theError);
			/** If table exists will get -  WARNING 02000: No row was found **/
			if (theError.equals("42X05")) {   // Table does not exist
				return false;
			}  
			else if (theError.equals("42X14") || theError.equals("42821"))  {
				logger.info("WwdChk4Table: Incorrect table definition. Drop table WISH_LIST and rerun this program");
				throw sqle;   
			} 
			else {
				logger.info("WwdChk4Table: Unhandled SQLException" );
				throw sqle; 
			}
		}
		//  System.out.println("Just got the warning - table exists OK ");
		return true;
	}
	
	public static void addTriggerData(SqlMapClient smc, HashMap<String, String> triggers) throws Exception {
		try {
			for (Object key : triggers.keySet()) {
				TriggerCriteria tc = new TriggerCriteria();
				String keyString = (String) key;
				String value = (String)triggers.get(keyString);
				if (value.indexOf("|") > -1) {
					StringTokenizer st = new StringTokenizer(value, "|");
					if (st.countTokens() != 2) {
						throw new Exception("Trigger definition in properties file is incorrect!");
					}
					tc.setTriggerType(st.nextToken());
					tc.setTriggerCriteria(st.nextToken());
					smc.insert("insertTriggerCriteria", tc);
				}
				else {
					tc.setTriggerType(value);
					tc.setTriggerCriteria("");
					smc.insert("insertTriggerCriteria", tc);
				}
			}
		}
		catch (Exception e) {
			logger.error(" ", e);
			throw e;
		}
	}
	
	public static void addPropertyData(SqlMapClient smc, HashMap<String, String> properties, int hostPort) throws Exception {
		try {
			for (Object key : properties.keySet()) {
				Property prop = new Property();
				String keyString = (String) key;
				String value = (String)properties.get(keyString);
				prop.setKeyValue(keyString);
				if (value.indexOf("!!DERBYROOTLOCATION!!") > -1) {
					value = value.replaceAll("!!DERBYROOTLOCATION!!", System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE));
				}

				prop.setValueValue(value);
				prop.setHostPort(hostPort);
				smc.insert("insertProperty", prop);
			}
			/*Property prop = new Property();
			String keyString = "derby.shutdown.url";
			//String value = "derby.shutdown.url=jdbc:derby://!!DERBYHOSTIP!!:1528//u01/home/derbyNetwork/PasswordResetDataStore/working/GSITracker;user=sa;password=7D0AfVhv;dataEncryption=true;bootPassword=a1XcahqF;shutdown=true";
			//System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE)    -     File.separator  - C:/u01/home/
			String value = "";
			//if (File.separator.equals("/")) { //  UNIX/Linux system
				//value = "jdbc:derby://!!DERBYHOSTIP!!:1528//!!DERBYROOTLOCATION!!derbyNetwork/PasswordResetDataStore/working/GSITracker;user=sa;password=7D0AfVhv;shutdown=true";				
			//}
			//else {
				value = "jdbc:derby://!!DERBYHOSTIP!!:1527/!!DERBYROOTLOCATION!!derbyNetwork/PasswordResetDataStore/working/GSITracker;user=sa;password=7D0AfVhv;shutdown=true";
			//}
			value = value.replaceAll("!!DERBYHOSTIP!!", InetAddress.getLocalHost().getHostAddress());
			value = value.replaceAll("!!DERBYROOTLOCATION!!", System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE));
			prop.setKeyValue(keyString);
			prop.setValueValue(value);
			//prop.setHostPort(0);
			smc.insert("insertProperty", prop);*/
		}
		catch (Exception e) {
			logger.error(" ", e);
			throw e;
		}
	}

	public static boolean isUsingSecondaryDerbyInstance() {
		return usingSecondaryDerbyInstance;
	}

	public static void setUsingSecondaryDerbyInstance(boolean usingSecondaryDerbyInstance) {
		TwoFactorAuthorizerDAO.usingSecondaryDerbyInstance = usingSecondaryDerbyInstance;
	}

	public static String selectPropertyByKey(String key, int hostPort) throws Exception {
		SqlMapClient smc = getSqlMapClient();
		String value = null;
		if (smc != null) {
			Property property = new Property();
			property.setKeyValue(key);
			property.setHostPort(hostPort);
			List list = smc.queryForList("selectPropertyByKey", property);
			if (list != null && list.size() > 0) {
				value = ((Property)list.get(0)).getValueValue();
			}
			else {
				throw new Exception("Property: " + key + " not found.");
			}
		}
		else {
			throw new Exception("Error getting SqlMapClient in TwoFactorAuthorizerDAO");
		}
		return value;
	}

	public static boolean checkPropertyByKey(String key, int hostPort) throws Exception {
		boolean foundIt = false;
		SqlMapClient smc = getSqlMapClient();
		String value = null;
		if (smc != null) {
			Property property = new Property();
			property.setKeyValue(key);
			property.setHostPort(hostPort);
			List list = smc.queryForList("selectPropertyByKey", property);
			if (list != null && list.size() > 0) {
				foundIt = true;
			}
		}
		return foundIt;
		
	}

	public static void updatePropertyNoOldValue(SqlMapClient smc, String key, String value, int hostPort) throws Exception {
		try {
			Property prop = new Property(key, value, hostPort);
			smc.insert("updatePropertyNoOldValue", prop);
		}
		catch (Exception e) {
			logger.error(" ", e);
			throw e;
		}
	}

	public static void addSingleProperty(SqlMapClient smc, String key, String value, int hostPort) throws Exception {
		try {
			Property prop = new Property(key, value, hostPort);
			smc.insert("insertProperty", prop);
		}
		catch (Exception e) {
			logger.error(" ", e);
			throw e;
		}
	}
}
