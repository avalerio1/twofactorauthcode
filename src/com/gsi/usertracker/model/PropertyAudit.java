package com.gsi.usertracker.model;

import java.sql.Timestamp;
import java.util.Date;

public class PropertyAudit {

	private String keyValue = "";
	private String valueValue = "";
	private String changeUserName = "";
	private Timestamp modifyTime = new Timestamp(new Date().getTime());
	private String action = "";
	private int hostPort = 0;
	
	public PropertyAudit() {
	}

	public String getChangeUserName() {
		return changeUserName;
	}

	public void setChangeUserName(String changeUserName) {
		this.changeUserName = changeUserName;
	}

	public Timestamp getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Timestamp modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public String getValueValue() {
		return valueValue;
	}

	public void setValueValue(String valueValue) {
		this.valueValue = valueValue;
	}

	public String toString() {
		return "TriggerCriteria: " + "key: " + keyValue + "value: " + valueValue + "changeUserName: " + changeUserName + "modifyTime: " + modifyTime + "action" + action;
	}

	public int getHostPort() {
		return hostPort;
	}

	public void setHostPort(int hostPort) {
		this.hostPort = hostPort;
	}
}
