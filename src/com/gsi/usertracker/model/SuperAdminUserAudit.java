package com.gsi.usertracker.model;

import java.sql.Timestamp;
import java.util.Date;

public class SuperAdminUserAudit {

	private String adminUserName = "";
	private String adminPassword = "";
	private String changeUserName = "";
	private Timestamp modifyTime = new Timestamp(new Date().getTime());
	private String action = "";

	public SuperAdminUserAudit() {
	}

	public SuperAdminUserAudit(String adminUserName, String adminPassword) {
		setAdminUserName(adminUserName);
		setAdminPassword(adminPassword);
	}

	public String toString() {
		return "TriggerCriteria: " + "adminUserName: " + adminUserName + "adminPassword: " + adminPassword;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getChangeUserName() {
		return changeUserName;
	}

	public void setChangeUserName(String changeUserName) {
		this.changeUserName = changeUserName;
	}

	public Timestamp getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Timestamp modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
