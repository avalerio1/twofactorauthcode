package com.gsi.usertracker.model;

import java.sql.Timestamp;
import java.util.Date;

public class PasswordResetTracker {

	private String ipAddress = "";
	private Integer tryCounter = 0;
	private Timestamp blockTime = new Timestamp(new Date().getTime());

	public PasswordResetTracker() {
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getTryCounter() {
		return tryCounter;
	}

	public void setTryCounter(Integer tryCounter) {
		this.tryCounter = tryCounter;
	}

	public Timestamp getBlockTime() {
		return blockTime;
	}

	public void setBlockTime(Timestamp blockTime) {
		this.blockTime = blockTime;
	}
}
