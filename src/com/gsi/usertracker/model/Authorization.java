package com.gsi.usertracker.model;

public class Authorization {

	private String authorizationType = null;
	private String authorizationOption = "";

	public Authorization() {
	}

	public String getAuthorizationType() {
		return authorizationType;
	}

	public void setAuthorizationType(String authorizationType) {
		this.authorizationType = authorizationType;
	}

	public String getAuthorizationOption() {
		return authorizationOption;
	}

	public void setAuthorizationOption(String authorizationOption) {
		this.authorizationOption = authorizationOption;
	}

	public String toString() {
		return "TriggerCriteria: " + "authorizationType: " + authorizationType + "authorizationOption: " + authorizationOption;
	}
}
