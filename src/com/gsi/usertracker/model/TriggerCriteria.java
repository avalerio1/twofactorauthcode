package com.gsi.usertracker.model;

public class TriggerCriteria {

	private String triggerType = "";
	private String triggerCriteria = "";

	public TriggerCriteria() {
	}

	public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	public String getTriggerCriteria() {
		return triggerCriteria;
	}

	public void setTriggerCriteria(String triggerCriteria) {
		this.triggerCriteria = triggerCriteria;
	}

	public String toString() {
		return "TriggerCriteria: " + "triggerType: " + triggerType + "triggerCriteria: " + triggerCriteria;
	}
}
