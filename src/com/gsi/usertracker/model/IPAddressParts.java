package com.gsi.usertracker.model;

import java.util.StringTokenizer;

public class IPAddressParts {

	private String part1 = "";
	private String part2 = "";
	private String part3 = "";
	private String part4 = "";

	public IPAddressParts() {
	}

	public IPAddressParts(String ipAddress) throws Exception {
		StringTokenizer st = new StringTokenizer(ipAddress, ".");
		if (st.countTokens() != 4) {
			throw new Exception("IP address doesn't have 4 parts");
		}
		setPart1(st.nextToken());
		setPart2(st.nextToken());
		setPart3(st.nextToken());
		setPart4(st.nextToken());
	}

	public String getPart1() {
		return part1;
	}

	public void setPart1(String part1) {
		this.part1 = part1;
	}

	public String getPart2() {
		return part2;
	}

	public void setPart2(String part2) {
		this.part2 = part2;
	}

	public String getPart3() {
		return part3;
	}

	public void setPart3(String part3) {
		this.part3 = part3;
	}

	public String getPart4() {
		return part4;
	}

	public void setPart4(String part4) {
		this.part4 = part4;
	}

	public String toString() {
		return "part1: " + part1 + "part2: " + part2 + "part3: " + part3 + "part4: " + part4;
	}
}
