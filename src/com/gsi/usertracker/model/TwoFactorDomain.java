package com.gsi.usertracker.model;

import java.sql.Timestamp;
import java.util.Date;

public class TwoFactorDomain {

	private String userName = "";
	private String ipAddress = "";
	private Integer valid = 0;
	private String authCode = "";
	private Timestamp createTime = new Timestamp(new Date().getTime());
	private String browserType = "";

	public TwoFactorDomain() {
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getValid() {
		return valid;
	}
	public boolean getValidAsBoolean() {
		return valid == 0 ? false : true; 
	}
	public void setValid(Integer valid) {
		this.valid = valid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("userName:").append(userName);
		sb.append(" ipAddress:").append(ipAddress);
		sb.append(" valid:").append(valid);
		sb.append(" authCode:").append(authCode);
		sb.append(" createTime:").append(createTime);
		sb.append(" browserType:").append(browserType);
		return sb.toString();
	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
}
