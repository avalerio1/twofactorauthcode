package com.gsi.usertracker.model;

public class PhoneNumberParts {

	private String areaCode = "";
	private String phoneNumber = "";

	public PhoneNumberParts() {
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNumberConcatenated() {
		return (getAreaCode() == null ? "" : getAreaCode().trim()) + (getPhoneNumber() == null ? "" : getPhoneNumber().trim());
	}
	
	public String toString() {
		return "areaCode: " + areaCode + "number: " + phoneNumber;
	}
}
