package com.gsi.usertracker.model;

import java.util.Date;
import java.sql.Timestamp;

public class UserLoginTracker {

	private String userName = "";
	private String ipAddress = "";
	private Timestamp loginTime = new Timestamp(new Date().getTime());
	private String browser = "";
	private String browserVersion = "";

	public UserLoginTracker() {
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

}
