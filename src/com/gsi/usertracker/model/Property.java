package com.gsi.usertracker.model;

public class Property {

	private String keyValue = "";
	private String valueValue = "";
	private String oldValueValue = "";
	private int hostPort = 0;

	public Property() {
	}
	public Property(String key, String value, int hostPort) {
		setKeyValue(key);
		setValueValue(value);
		setHostPort(hostPort);
	}
	public Property(String key, String value, String oldValue, int hostPort) {
		setKeyValue(key);
		setValueValue(value);
		setOldValueValue(oldValue);
		setHostPort(hostPort);
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public String getValueValue() {
		return valueValue;
	}

	public void setValueValue(String valueValue) {
		this.valueValue = valueValue;
	}

	public String toString() {
		return "TriggerCriteria: " + "key: " + keyValue + "value: " + valueValue;
	}

	public String getOldValueValue() {
		return oldValueValue;
	}

	public void setOldValueValue(String oldValueValue) {
		this.oldValueValue = oldValueValue;
	}

	public int getHostPort() {
		return hostPort;
	}
	public void setHostPort(int hostPort) {
		this.hostPort = hostPort;
	}
}
