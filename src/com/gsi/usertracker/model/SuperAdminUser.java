package com.gsi.usertracker.model;

public class SuperAdminUser {

	private String adminUserName = "";
	private String adminPassword = "";
	private String oldAdminPassword = "";

	public SuperAdminUser() {
	}

	public SuperAdminUser(String adminUserName, String adminPassword) {
		setAdminUserName(adminUserName);
		setAdminPassword(adminPassword);
	}

	public String toString() {
		return "TriggerCriteria: " + "adminUserName: " + adminUserName + "adminPassword: " + adminPassword;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getOldAdminPassword() {
		return oldAdminPassword;
	}

	public void setOldAdminPassword(String oldAdminPassword) {
		this.oldAdminPassword = oldAdminPassword;
	}
}
