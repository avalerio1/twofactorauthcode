package com.gsi.usertracker.model;

public class ResponseTiming {


	private String url = "";
	private Double responseTiming = -1.0;

	public ResponseTiming() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Double getResponseTiming() {
		return responseTiming;
	}

	public void setResponseTiming(Double responseTiming) {
		this.responseTiming = responseTiming;
	}

}
