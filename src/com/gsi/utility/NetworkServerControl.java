package com.gsi.utility;

import java.io.PrintWriter;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.URL;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.PrivilegedExceptionAction;
import java.security.ProtectionDomain;
import java.util.Properties;
import org.apache.derby.iapi.services.info.JVMInfo;
import org.apache.derby.iapi.services.property.PropertyUtil;
import org.apache.derby.impl.drda.NetworkServerControlImpl;
import org.apache.log4j.Logger;

public class NetworkServerControl
{
  public static final int DEFAULT_PORTNUMBER = 1527;
  private static final String DERBYNET_JAR = "derbynet.jar";
  private static final String POLICY_FILENAME = "server.policy";
  private static final String POLICY_FILE_PROPERTY = "java.security.policy";
  private static final String DERBY_HOSTNAME_WILDCARD = "0.0.0.0";
  private static final String IPV6_HOSTNAME_WILDCARD = "::";
  private static final String SOCKET_PERMISSION_HOSTNAME_WILDCARD = "*";
  private NetworkServerControlImpl serverImpl;
  private static final Logger logger = Logger.getLogger("prLogger");	

  public NetworkServerControl(InetAddress paramInetAddress, int paramInt, String paramString1, String paramString2)
    throws Exception
  {
    serverImpl = new NetworkServerControlImpl(paramInetAddress, paramInt, paramString1, paramString2);
  }

  public NetworkServerControl(String paramString1, String paramString2)
    throws Exception
  {
    serverImpl = new NetworkServerControlImpl(paramString1, paramString2);
  }

  public NetworkServerControl(InetAddress paramInetAddress, int paramInt)
    throws Exception
  {
    serverImpl = new NetworkServerControlImpl(paramInetAddress, paramInt);
  }

  public NetworkServerControl()
    throws Exception
  {
    serverImpl = new NetworkServerControlImpl();
  }

  public static void main(String[] paramArrayOfString)
  {
    NetworkServerControlImpl localNetworkServerControlImpl = null;
    int i = 1;
    try
    {
      localNetworkServerControlImpl = new NetworkServerControlImpl();
      int j = localNetworkServerControlImpl.parseArgs(paramArrayOfString);
      if ((j == 0) && (JVMInfo.JDK_ID >= 8))
        try
        {
          AccessController.doPrivileged(new PrivilegedExceptionAction()
          {
            public Void run()
              throws Exception
            {
              System.setProperty("derby.__serverStartedFromCmdLine", "true");
              return null;
            }
          });
        }
        catch (Exception localException2)
        {
          logger.error("", localException2);
          localNetworkServerControlImpl.consoleExceptionPrintTrace(localException2);
          System.exit(1);
        }
      if (needsSecurityManager(localNetworkServerControlImpl, j))
      {
        verifySecurityState(localNetworkServerControlImpl);
        installSecurityManager(localNetworkServerControlImpl);
      }
      i = 0;
      localNetworkServerControlImpl.executeWork(j);
    }
    catch (Exception localException1)
    {
        logger.error("", localException1);    	
      if ((localException1.getMessage() == null) || (!localException1.getMessage().equals("Unexpected exception")) || (i != 0))
        if (localNetworkServerControlImpl != null)
          localNetworkServerControlImpl.consoleExceptionPrint(localException1);
        else
          localException1.printStackTrace();
      System.exit(1);
    }
    System.exit(0);
  }

  public void start(PrintWriter paramPrintWriter)
    throws Exception
  {
    serverImpl.start(paramPrintWriter);
  }

  public void shutdown()
    throws Exception
  {
    serverImpl.shutdown();
  }

  public void ping()
    throws Exception
  {
    serverImpl.ping();
  }

  public void trace(boolean paramBoolean)
    throws Exception
  {
    serverImpl.trace(paramBoolean);
  }

  public void trace(int paramInt, boolean paramBoolean)
    throws Exception
  {
    serverImpl.trace(paramInt, paramBoolean);
  }

  public void logConnections(boolean paramBoolean)
    throws Exception
  {
    serverImpl.logConnections(paramBoolean);
  }

  public void setTraceDirectory(String paramString)
    throws Exception
  {
    serverImpl.sendSetTraceDirectory(paramString);
  }

  public String getSysinfo()
    throws Exception
  {
    return serverImpl.sysinfo();
  }

  public String getRuntimeInfo()
    throws Exception
  {
    return serverImpl.runtimeInfo();
  }

  public void setMaxThreads(int paramInt)
    throws Exception
  {
    serverImpl.netSetMaxThreads(paramInt);
  }

  public int getMaxThreads()
    throws Exception
  {
    String str = serverImpl.getCurrentProperties().getProperty("derby.drda.maxThreads");
    return Integer.parseInt(str);
  }

  public void setTimeSlice(int paramInt)
    throws Exception
  {
    serverImpl.netSetTimeSlice(paramInt);
  }

  public int getTimeSlice()
    throws Exception
  {
    String str = serverImpl.getCurrentProperties().getProperty("derby.drda.timeSlice");
    return Integer.parseInt(str);
  }

  public Properties getCurrentProperties()
    throws Exception
  {
    return serverImpl.getCurrentProperties();
  }

  protected void setClientLocale(String paramString)
  {
    serverImpl.setClientLocale(paramString);
  }

  private static boolean needsSecurityManager(NetworkServerControlImpl paramNetworkServerControlImpl, int paramInt)
    throws Exception
  {
    return (System.getSecurityManager() == null) && (paramInt == 0) && (!paramNetworkServerControlImpl.runningUnsecure());
  }

  private static void verifySecurityState(NetworkServerControlImpl paramNetworkServerControlImpl)
    throws Exception
  {
  }

  private static void installSecurityManager(NetworkServerControlImpl paramNetworkServerControlImpl)
    throws Exception
  {
    if (PropertyUtil.getSystemProperty("derby.system.home") == null)
      System.setProperty("derby.system.home", PropertyUtil.getSystemProperty("user.dir"));
    if (PropertyUtil.getSystemProperty("derby.drda.traceDirectory") == null)
      System.setProperty("derby.drda.traceDirectory", PropertyUtil.getSystemProperty("derby.system.home"));
    System.setProperty("derby.security.host", getHostNameForSocketPermission(paramNetworkServerControlImpl));
    System.setProperty("derby.security.port", String.valueOf(paramNetworkServerControlImpl.getPort()));
    String str1 = getCodeSourcePrefix(paramNetworkServerControlImpl);
    System.setProperty("derby.install.url", str1);
    String str2 = getPolicyFileURL();
    System.setProperty("java.security.policy", str2);
    SecurityManager localSecurityManager = new SecurityManager();
    System.setSecurityManager(localSecurityManager);
    if (localSecurityManager.equals(System.getSecurityManager()))
    {
      String str3 = paramNetworkServerControlImpl.localizeMessage("DRDA_SecurityInstalled.I", null);
      paramNetworkServerControlImpl.consoleMessage(str3, true);
    }
  }

  private static String getHostNameForSocketPermission(NetworkServerControlImpl paramNetworkServerControlImpl)
    throws Exception
  {
    String str = paramNetworkServerControlImpl.getHost();
    if ((hostnamesEqual("0.0.0.0", str)) || ("::".equals(str)))
      str = "*";
    else if (isIPV6Address(str))
      str = '[' + str + "]:0-";
    return str;
  }

  private static boolean hostnamesEqual(String paramString1, String paramString2)
  {
    try
    {
      InetAddress localInetAddress1 = InetAddress.getByName(paramString1);
      InetAddress localInetAddress2 = InetAddress.getByName(paramString2);
      return localInetAddress1.equals(localInetAddress2);
    }
    catch (Exception localException)
    {
        logger.error("", localException);
    }
    return false;
  }

  private static boolean isIPV6Address(String paramString)
  {
    if (paramString == null)
      return false;
    int i = paramString.length();
    for (int j = 0; j < i; j++)
    {
      char c = paramString.charAt(j);
      if ((c != ':') && (Character.digit(c, 16) < 0))
        return false;
    }
    try
    {
      InetAddress localInetAddress = InetAddress.getByName(paramString);
      return localInetAddress instanceof Inet6Address;
    }
    catch (Exception localException)
    {
        logger.error("", localException);
    }
    return false;
  }

  private static String getCodeSourcePrefix(NetworkServerControlImpl paramNetworkServerControlImpl)
    throws Exception
  {
    ProtectionDomain localProtectionDomain = NetworkServerControl.class.getProtectionDomain();
    CodeSource localCodeSource = localProtectionDomain.getCodeSource();
    if (localCodeSource == null)
      return null;
    URL localURL = localCodeSource.getLocation();
    if (localURL == null)
      return null;
    String str1 = localURL.toExternalForm().replaceFirst("^file://([^/].*)", "file:////$1");
    int i = str1.indexOf("derbynet.jar");
    if (i < 0)
    {
      String str2 = paramNetworkServerControlImpl.localizeMessage("DRDA_MissingNetworkJar.S", null);
      paramNetworkServerControlImpl.consoleError(str2);
    }
    String str2 = str1.substring(0, i);
    return str2;
  }

  private static String getPolicyFileURL()
    throws Exception
  {
    String str1 = NetworkServerControl.class.getPackage().getName().replace('.', '/') + '/' + "server.policy";
    URL localURL = NetworkServerControl.class.getClassLoader().getResource(str1);
    String str2 = localURL.toExternalForm();
    return str2;
  }
}

/* Location:           C:\hold\JadGui\
 * Qualified Name:     org.apache.derby.drda.NetworkServerControl
 * JD-Core Version:    0.6.2
 */