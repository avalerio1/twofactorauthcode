package com.gsi.utility;

import java.io.IOException;
import java.io.Reader;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class GSIIbatisConnectionUtility {

	static private SqlMapClient primarySQLMapperDerby;
	static private Reader primaryReaderDerby;
	//static private SqlMapClient secondarySQLMapperDerby;
	//static private Reader secondaryReaderDerby;

	static public SqlMapClient getPrimaryDerbyGSISqlMapClient() throws Exception {
		if (primarySQLMapperDerby == null) {
			primarySQLMapperDerby = SqlMapClientBuilder.buildSqlMapClient(getPrimaryDerbyReader());
			primaryReaderDerby.close();
		}
		return primarySQLMapperDerby;
	}

	static private Reader getPrimaryDerbyReader() throws IOException {
		if (primaryReaderDerby == null) {
			primaryReaderDerby = Resources.getResourceAsReader("resources/PrimaryGSIDerbySqlMapConfig.xml");
		}
		return primaryReaderDerby;
	}

	/*static public SqlMapClient getSecondaryDerbyGSISqlMapClient() throws Exception {
		if (secondarySQLMapperDerby == null) {
			secondarySQLMapperDerby = SqlMapClientBuilder.buildSqlMapClient(getSecondaryDerbyReader());
			secondaryReaderDerby.close();
		}
		return secondarySQLMapperDerby;
	}

	static private Reader getSecondaryDerbyReader() throws IOException {
		if (secondaryReaderDerby == null) {
			secondaryReaderDerby = Resources.getResourceAsReader("resources/SecondaryGSIDerbySqlMapConfig.xml");
		}
		return secondaryReaderDerby;
	}*/
}
