package com.gsi.utility;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import coral.reader.FileReader;

public class BackgroundJobManager implements ServletContextListener {

	private static final Logger logger = Logger.getLogger("prLogger");	
    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
    	try {
		    	FileReader fileReader = new FileReader();
		    	String keyLocation = System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE).trim() + "key.key";
		    	Map<String, Object> licenseMap = fileReader.getValues(keyLocation);
		    	String licenseType = (String)licenseMap.get("licenseType");
		    	if (licenseType.equals(GSIUtility.LICENSE_TYPE_AUTOMATIC)) {		
			    	scheduler = Executors.newSingleThreadScheduledExecutor();
			        scheduler.scheduleAtFixedRate(new ValcCheck(), 0, 1, TimeUnit.DAYS);
		    	}
    	}
    	catch (Exception e) {
			logger.error("Error setting ValcCheck scheduler: ",e);
    	}
        //scheduler.scheduleAtFixedRate(new SomeHourlyJob(), 0, 1, TimeUnit.HOURS);
        //scheduler.scheduleAtFixedRate(new SomeQuarterlyJob(), 0, 15, TimeUnit.MINUTES);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (scheduler != null) {
        	scheduler.shutdownNow();
        }
    }

}
