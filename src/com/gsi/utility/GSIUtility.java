package com.gsi.utility;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.model.Authorization;
import com.gsi.usertracker.model.IPAddressParts;
import com.gsi.usertracker.model.TriggerCriteria;
import com.gsi.usertracker.model.TwoFactorDomain;

import coral.reader.FileReader;

public class GSIUtility {

	private static final Logger logger = Logger.getLogger("2faLogger");	
	public static final String USERNAME = "User";
	public static final String PASSWORD = "Password";
	public static final String ENVIRONMENT = "Environment";
	public static final String ROLE = "Role";
	public static final String LOGIN_FAILED_TEXT = "Sign In Error:";
	public static final String LOGIN_URL = "/E1Menu.maf";
	public static final String LOGOUT_URL = "/MafletClose.mafService?e1.namespace=&e1.service=MafletClose&RENDER_MAFLET=E1Menu&e1.state=maximized&e1.mode=view";
	public static final String IPWHITELIST = "IPWhiteList";
	public static final String IPBLACKLIST = "IPBlackList";
	public static final String IPUNIQUE = "IPUnique";
	public static final String BROWSERUNIQUE = "BrowserUnique";
	public static final String IPLIST_CONDITION_ABSOLUTE = "absolute";
	public static final String IPLIST_CONDITION_CONDITIONALONETIME = "conditionalOneTime";
	public static final String IPLIST_CONDITION_CONDITIONALALLTIME = "conditionalAllTime";
	public static final String ADMIN2FA = "Admin2FA";
    private static final String dCase = "abcdefghijklmnopqrstuvwxyz";
    private static final String uCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String sChar = "!@#$%^&*";
    private static final String intChar = "0123456789";
    public static final String LICENSEEXPIRED = "licenseExpired";
    public static final String LICENSEVALID = "licenseValid";
    public static final String LICENSEWARNING = "licenseWarning";
    public static final String PRODUCT_IDENTIFIER = "JDE2FA";
    public static final String LICENSE_TYPE_AUTOMATIC = "Automatic";
    public static final String LICENSE_TYPE_MANUAL = "Manual";
    public static final String HOME_LOCATION_ENV_VARIABLE = "GSI_2FA_HOME";
    private static Random r = new Random();
	
	//public static final String USERNAME = "emailAddressPage";  // <- XSE Tonerworld
	//public static final String PASSWORD = "userPasswordPage";  // <- XSE Tonerworld
	//public static final String LOGIN_FAILED_TEXT = "Forgot password?"; // <- XSE Tonerworld login failure text.
	//public static final String LOGIN_URL = "/LogonBad.do";
	//public static final String LOGOUT_URL = "/Logout.do";  // <- XSE Tonerworld
	
	public static String getURL(HttpServletRequest request, String URI, String requestParams) {
		try {
			StringBuffer redirectURL = new StringBuffer();
			redirectURL.append(request.getScheme()).append("://"); //     http://;
			redirectURL.append(request.getServerName()); //     http://avdev
			int port = request.getServerPort();
			if (port != 80) {
				redirectURL.append(":").append(port);
			}
			/*else { 
				redirectURL.append("/"); //
			}*/
			redirectURL.append(request.getContextPath());   //   /TWJustWeb
			redirectURL.append(URI).append(requestParams);
			logger.info(redirectURL.toString());
			
			return redirectURL.toString();
		}
		catch (Exception e) {
			logger.error("",e);
			e.printStackTrace();
			return null;
		}
	}
	
    // Check the param if it's not present return the default  
    public static String getParam(HttpServletRequest request, String param, String def) {  
        String parameter = request.getParameter(param);  
        if (parameter == null || "".equals(parameter)) {  
            return def;  
        } else {  
            return parameter;  
        }  
    }  

	public static String easyEncrypt(String unencryptedString) throws UnsupportedEncodingException {
		if (unencryptedString != null) {
			byte[] encryptArray = Base64.encodeBase64(unencryptedString.getBytes());        
			String encstr = new String(encryptArray,"UTF-8");   
			return encstr;
		}
		else {
			return null;
		}
	}
	
	public static String easyDecrypt(String encryptedString) throws UnsupportedEncodingException {
		if (encryptedString != null) {
			byte[] dectryptArray = encryptedString.getBytes();
			byte[] decarray = Base64.decodeBase64(dectryptArray);
			String decstr = new String(decarray,"UTF-8"); 
			return decstr;
		}
		else {
			return null;
		}
	}
	
	public static String getMaskedEmailAddress(String emailAddress) {
		String maskedEmail = "";
		int i = emailAddress.indexOf("@");
		if (i > -1) {
			maskedEmail = emailAddress.substring(0,1) + "****" + emailAddress.substring(i);
		}
		return maskedEmail;
	}
	
	public static String getMaskedPhoneNumber(String concatNumber) {
		String maskedPhoneNumber = "";
		if (concatNumber.trim().length() > 4) {
			maskedPhoneNumber = "****" + concatNumber.substring(concatNumber.trim().length() - 4);
		}
		else {
			maskedPhoneNumber = "***" + concatNumber;
		}
		return maskedPhoneNumber;
	}

	public static Authorization determineAuthorization(TwoFactorDomain tfd, List<TriggerCriteria> triggerCriteriaList, String adminAlways2FA) throws Exception {
		/* trigger type examples
		<!-- 2FA triggers. Triggers available: IPBlackList, IPWhiteList, IPUnique, BrowserUnique  -->
		<!-- trigger type examples -->
		<!-- 	IPWhiteList|10.0.0.1-conditional -->
		<!-- 	IPWhiteList|192.168.1.*-192.168.2.100-absolute -->
		<!-- 	IPBlackList|172.16.0.0-172.31.255.255-conditional -->
		<!-- 	IPBlackList|10.114.243.0-10.114.243.200-absolute -->
		<!-- 	IPUnique -->
		<!-- 	BrowserUnique -->
		 */
		Authorization authorization = new Authorization();
		if (adminAlways2FA != null && adminAlways2FA.trim().equalsIgnoreCase("true")) {
			List<String> admin2FAList = TwoFactorAuthorizerDAO.selectAllAdmin2FA();
			boolean adminNeedsAuthorization = false;
			for (Iterator<String> i = admin2FAList.iterator(); i.hasNext();) {
				String adminUserName = (String)i.next();
				if (adminUserName.trim().equalsIgnoreCase(tfd.getUserName().trim())) {
					adminNeedsAuthorization = true;
				}
			}
			if (adminNeedsAuthorization) {
				authorization.setAuthorizationType(ADMIN2FA);
			}
		}
		for (Iterator<TriggerCriteria> i = triggerCriteriaList.iterator(); i.hasNext();) {
			if (authorization.getAuthorizationType() != null) { // no need to check further if authorization is already needed
				break;
			}
			TriggerCriteria tc = (TriggerCriteria)i.next();
			String triggerType = tc.getTriggerType();
			String triggerCriteria = tc.getTriggerCriteria();
			if (triggerType.trim().equalsIgnoreCase(IPWHITELIST)) {
				StringTokenizer st = new StringTokenizer(triggerCriteria, ":");
				if (st.countTokens() != 2) {
					throw new Exception("IP Blacklist configured incorrectly (not enough parameters).");
				}
				String ipRange = st.nextToken();
				String option = st.nextToken();
				authorization.setAuthorizationOption(option);
				boolean isInList = isIPContainedInList(tfd.getIpAddress(), ipRange);
				if (!isInList) {
					// below will never let any black listed ip address in
					if (option.equals(IPLIST_CONDITION_ABSOLUTE)) {
						authorization.setAuthorizationType(IPWHITELIST);
					}
					else if (option.equals(IPLIST_CONDITION_CONDITIONALONETIME)) {
					// below will check if black listed ip address has been authorized
						TwoFactorDomain tfd2 = new TwoFactorDomain();
						tfd2.setIpAddress(tfd.getIpAddress());
						tfd2.setUserName(tfd.getUserName());
				        tfd2.setValid(1);
				        List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthorized(tfd2);
				        if (userDomain == null || userDomain.size() < 1) {
							authorization.setAuthorizationType(IPWHITELIST);
				        }
						else if (option.equals(IPLIST_CONDITION_CONDITIONALALLTIME)) {
							authorization.setAuthorizationType(IPWHITELIST);
						}
					}
					else {
						throw new Exception("IP Blacklist configured incorrectly (ip list option incorrect).");
					}
				}
			}
			else if (triggerType.trim().equalsIgnoreCase(IPBLACKLIST)) {
				logger.info("1 - triggerType == " + IPBLACKLIST);
				StringTokenizer st = new StringTokenizer(triggerCriteria, ":");
				if (st.countTokens() != 2) {
					throw new Exception("IP Blacklist configured incorrectly (not enough parameters).");
				}
				String ipRange = st.nextToken();
				String option = st.nextToken();
				authorization.setAuthorizationOption(option);
				logger.info("2 - ipRange - option == " + ipRange + " - " + option);
				boolean isInList = isIPContainedInList(tfd.getIpAddress(), ipRange);
				if (isInList) {
					// below will never let any black listed ip address in
					if (option.equals(IPLIST_CONDITION_ABSOLUTE)) {
						authorization.setAuthorizationType(IPBLACKLIST);
					}
					else if (option.equals(IPLIST_CONDITION_CONDITIONALONETIME)) {
					// below will check if black listed ip address has been authorized
						TwoFactorDomain tfd2 = new TwoFactorDomain();
						tfd2.setIpAddress(tfd.getIpAddress());
						tfd2.setUserName(tfd.getUserName());
				        tfd2.setValid(1);
				        logger.info("IPBLACKLIST checkUserDomain: " + tfd2.toString());
				        List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthorized(tfd2);
				        if (userDomain == null || userDomain.size() < 1) {
							authorization.setAuthorizationType(IPBLACKLIST);
				        }
					}
					else if (option.equals(IPLIST_CONDITION_CONDITIONALALLTIME)) {
						authorization.setAuthorizationType(IPBLACKLIST);
					}
					else {
						throw new Exception("IP Blacklist configured incorrectly (ip list option incorrect).");
					}
				}
			}
			else if (triggerType.trim().equalsIgnoreCase(IPUNIQUE)) {
				TwoFactorDomain tfd2 = new TwoFactorDomain();
				tfd2.setIpAddress(tfd.getIpAddress());
				tfd2.setUserName(tfd.getUserName());
		        tfd2.setValid(1);
		        List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthorized(tfd2);
		        if (userDomain == null || userDomain.size() < 1) {
					authorization.setAuthorizationType(IPUNIQUE);
		        }
			}
			else if (triggerType.trim().equalsIgnoreCase(BROWSERUNIQUE)) {
				TwoFactorDomain tfd2 = new TwoFactorDomain();
				tfd2.setBrowserType(tfd.getBrowserType());
				tfd2.setUserName(tfd.getUserName());
		        tfd2.setValid(1);
		        List userDomain = TwoFactorAuthorizerDAO.checkUserDomainForAuthorized(tfd2);
		        if (userDomain == null || userDomain.size() < 1) {
					authorization.setAuthorizationType(BROWSERUNIQUE);
		        }
			}
		}
		return authorization;
	}
	
	private static boolean isIPContainedInList(String ipAddress, String triggerCriteria) throws Exception {
		boolean isInList = true;
		if (triggerCriteria.indexOf("-") > -1) { // signifies criteria has range of ip addresses
			StringTokenizer st = new StringTokenizer(triggerCriteria, "-");
			if (st.countTokens() != 2) {
				throw new Exception("IP address range dosen't have two parts.");
			}
			IPAddressParts ipAddressParts = new IPAddressParts(ipAddress);
			String beginIPRange = st.nextToken();
			String endIPRange = st.nextToken();
			IPAddressParts beginRangeParts = new IPAddressParts(beginIPRange);
			IPAddressParts endRangeParts = new IPAddressParts(endIPRange);
			//check if ip address is greater/equal to begin range
			if (!beginRangeParts.getPart1().equals("*")) {
				if (Integer.parseInt(ipAddressParts.getPart1()) >= Integer.parseInt(beginRangeParts.getPart1())) {
					if (!beginRangeParts.getPart2().equals("*")) {
						if (Integer.parseInt(ipAddressParts.getPart2()) >= Integer.parseInt(beginRangeParts.getPart2())) {
							if (!beginRangeParts.getPart3().equals("*")) {
								if (Integer.parseInt(ipAddressParts.getPart3()) >= Integer.parseInt(beginRangeParts.getPart3())) {
									if (!beginRangeParts.getPart4().equals("*")) {
										if (Integer.parseInt(ipAddressParts.getPart4()) >= Integer.parseInt(beginRangeParts.getPart4())) {
											// ip address is greater or equal to begin range ip address
										}									
										else {
											isInList = false;
										}
									}
								}
								else {
									isInList = false;
								}
							}							
						}
						else {
							isInList = false;
						}
					}
				}
				else {
					isInList = false;
				}
			}
			if (isInList) { // ip address is at least greater or equal to begin range ip address
				if (!endRangeParts.getPart1().equals("*")) {
					if (Integer.parseInt(ipAddressParts.getPart1()) <= Integer.parseInt(endRangeParts.getPart1())) {
						if (!endRangeParts.getPart2().equals("*")) {
							if (Integer.parseInt(ipAddressParts.getPart2()) <= Integer.parseInt(endRangeParts.getPart2())) {
								if (!endRangeParts.getPart3().equals("*")) {
									if (Integer.parseInt(ipAddressParts.getPart3()) <= Integer.parseInt(endRangeParts.getPart3())) {
										if (!endRangeParts.getPart4().equals("*")) {
											if (Integer.parseInt(ipAddressParts.getPart4()) <= Integer.parseInt(endRangeParts.getPart4())) {
												// ip address is less or equal to begin range ip address
											}									
											else {
												isInList = false;
											}
										}
									}
									else {
										isInList = false;										
									}
								}
							}
							else {
								isInList = false;
							}
						}
					}
					else {
						isInList = false;
					}
				}
			}
		}
		else { // single ip address
			if (ipAddress.trim().equalsIgnoreCase(triggerCriteria)) {
				isInList = true;
			}
		}
		
		return isInList;
	}
		
    public static String checkLicensedDate(int hostPort) throws Exception {
    	String licenseStatus = null;
    	//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");  //2030-01-01T05:00:00.000Z 23:59:59
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  //2030-01-01T05:00:00.000Z 23:59:59
    	try {
    		logger.info("1 - Before new FileReader()");
        	FileReader fileReader = new FileReader();
    		logger.info("2 - After new FileReader()");
        	String keyLocation = System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE).trim() + "key.key";
    		logger.info("3 - key file location: " + keyLocation);
        	Map licenseMap = fileReader.getValues(keyLocation);
    		logger.info("4 - key file location: " + keyLocation);
        	if (!((String)licenseMap.get("productName")).equals(PRODUCT_IDENTIFIER)) {
        		throw new Exception("License key file is not valid for 2FA.");
        	}
        	
        	String licensedDateString = (String)licenseMap.get("expirationDate");
        	if (licensedDateString != null && licensedDateString.trim().length() > 0) {
	        	Date licensedDate = sdf.parse(licensedDateString.trim() + " 23:59:59");
	        	if (licensedDate.compareTo(new Date()) < 0) { // license has expired
	        		licenseStatus = LICENSEEXPIRED;
	        	}
	        	else { // not expired, but check for 30 day warning
	    			Calendar warningCal = Calendar.getInstance();
	    			warningCal.setTime(new Date());
	    			warningCal.add(Calendar.DATE, 30); // add 30 days to todays date as warning date
	    			Date warningDate = warningCal.getTime();
	            	if (warningDate.compareTo(licensedDate) > 0) { // license will expire within 30 days
	            		licenseStatus = LICENSEWARNING;
	            	}
	            	else {
	            		licenseStatus = LICENSEVALID;
	            	}
	        	}
        	}
        	else {
        		licenseStatus = LICENSEEXPIRED;
        	}
    	}
    	catch (Exception e) {
    		logger.error("", e);
    		licenseStatus = LICENSEEXPIRED;
    	}
    	return licenseStatus;
    }

    public static void setLoggingLevel(Logger logger, int hostPort) {
		try {
			String loggingLevel = TwoFactorAuthorizerDAO.selectPropertyByKey("loggingLevel", hostPort);
			if (loggingLevel != null && !loggingLevel.isEmpty()) {
				if (loggingLevel.trim().equalsIgnoreCase("ALL")) {
					logger.setLevel(Level.ALL);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("TRACE")) {
					logger.setLevel(Level.TRACE);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("DEBUG")) {
					logger.setLevel(Level.DEBUG);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("INFO")) {
					logger.setLevel(Level.INFO);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("WARN")) {
					logger.setLevel(Level.WARN);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("ERROR")) {
					logger.setLevel(Level.ERROR);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("FATAL")) {
					logger.setLevel(Level.FATAL);
				}
				else if (loggingLevel.trim().equalsIgnoreCase("OFF")) {
					logger.setLevel(Level.OFF);
				}
				else {
				}
			}
		}
		catch (Exception e) {
			logger.error("Exception getting/setting logging level.");
		}
    	
    }
}
