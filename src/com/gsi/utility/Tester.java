package com.gsi.utility;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.Random;
import java.util.ResourceBundle;

import org.apache.commons.lang.RandomStringUtils;

import com.gsi.usertracker.dao.JDEDAO;
import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;
import com.gsi.usertracker.filters.TwoFactorAuthorizerFilter;
import com.ibatis.sqlmap.client.SqlMapClient;

public class Tester {
	private static NetworkServerControl server;
	private static String derbyHost;
	private static String derbyPort;

    private static final String UPPER = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
    private static final String LOWER = "abcdefghijklmnpqrstuvwxyz";
    private static final String NUMBER = "123456789";
    //private static final String SPECIAL = "!@#$%&*+?";
    private static final String SPECIAL = "@#";
    private Random randGen = new Random();
    private static final String dCase = "abcdefghijklmnopqrstuvwxyz";
    private static final String uCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String sChar = "!@#$%^&*";
    private static final String intChar = "0123456789";
    private static Random r = new Random();
	
	public Tester() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		try {
			/*ResourceBundle properties = PropertyResourceBundle.getBundle("resources/gsitracking");
			HashMap<String, String> triggers = new HashMap<String, String>();
			HashMap<String, String> propertiesMap = new HashMap<String, String>();
			System.out.println("INIT: Before Server start");
			derbyHost = properties.getString("derbyHostName");
			derbyPort = properties.getString("derbyPortNumber");
			server = new NetworkServerControl(InetAddress.getByName(derbyHost), new Integer(derbyPort), "sa", "7D0AfVhv");
			runServer(null, null);
			System.out.println("INIT: After Server start");
			TwoFactorAuthorizerDAO.checkDatabase(triggers, propertiesMap);
			int i = 0;
			if (server != null) {
				System.out.println("DESTROY: Before Server shutdown");
				server.shutdown();
				System.out.println("DESTROY: After Server shutdown");
			}*/

			/*Tester me = new Tester();
	        for (int i = 0; i <= 10; i++) {
	            me.printPassword();
	        }*/

			/*char[] possibleCharacters = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#!@#!@#")).toCharArray();
			for (int i = 0; i <= 10; i++) {
				String randomStr = RandomStringUtils.random(10, 0, possibleCharacters.length-1, false, false, possibleCharacters, new SecureRandom() );
				System.out.println( randomStr );			
			}*/
			
	        System.out.println ("Generating pass...");
			for (int i = 0; i <= 10; i++) {
				String pass = "";
		        while (pass.length () != 11){
		            int rPick = r.nextInt(4);
		            if (rPick == 0){
		                int spot = r.nextInt(25);
		                pass += dCase.charAt(spot);
		            } else if (rPick == 1) {
		                int spot = r.nextInt (25);
		                pass += uCase.charAt(spot);
		            } else if (rPick == 2) {
		                int spot = r.nextInt (7);
		                pass += sChar.charAt(spot);
		            } else if (rPick == 3){
		                int spot = r.nextInt (9);
		                pass += intChar.charAt(spot);
		            }
		        }
		        System.out.println ("Generated Pass: " + pass);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

    private void printPassword() {
        StringBuffer buf = new StringBuffer();
        buf.append(UPPER.charAt(Math.abs(randGen.nextInt()) % UPPER.length()));
        buf.append(LOWER.charAt(Math.abs(randGen.nextInt()) % LOWER.length()));
        buf.append(NUMBER.charAt(Math.abs(randGen.nextInt()) % NUMBER.length()));
        buf.append(SPECIAL.charAt(Math.abs(randGen.nextInt()) % SPECIAL.length()));
        for (int i = 0; i <= 4; i++) {
            buf.append(LOWER.charAt(Math.abs(randGen.nextInt()) % LOWER.length()));
        }
        buf.append(UPPER.charAt(Math.abs(randGen.nextInt()) % UPPER.length()));
        buf.append(LOWER.charAt(Math.abs(randGen.nextInt()) % LOWER.length()));
        System.out.println(buf.toString());
    }
	
	private static void runServer(String paramString, PrintWriter paramPrintWriter) throws Exception {
		final Runnable local1 = new Runnable() {
			public void run() {
				try {
					Properties p = System.getProperties();
					p.setProperty("derby.database.sqlAuthorization", "true");
					p.setProperty("derby.connection.requireAuthentication", "true");
					p.setProperty("derby.authentication.provider", "BUILTIN");
					p.setProperty("derby.user.sa", "7D0AfVhv");
					NetworkServerControl localNetworkServerControl = new NetworkServerControl(InetAddress.getByName(derbyHost), new Integer(derbyPort));
					localNetworkServerControl.start(null);
				}
				catch (Exception localException) {
					throw new RuntimeException(localException.getMessage());
				}
			}
		};
		Thread localThread = null;
		try {
			localThread = (Thread)AccessController.doPrivileged(new PrivilegedExceptionAction() {
				public Thread run() throws Exception {
					return new Thread(local1);
				}
			});
		}
		catch (Exception localException1) {
			throw new RuntimeException(localException1.getMessage());
		}
		localThread.start();
		try {
			int i = 0;
			int j = 0;
			do {
				j++;
				try {
					Thread.sleep(100L);
				}
				catch (InterruptedException localInterruptedException) {
					System.out.println(localInterruptedException);
					throw new Exception(localInterruptedException);
				}
				try {
					if (isServerStarted(server, 1)) {
						i = 1;
					}
				}
				catch (Exception localException3) {
				}
			}
			while ((i == 0) && (j < 20));
			if (j >= 20) {
				System.out.println("Don't know what this is");
				throw new Exception("Don't know what this is");
			}
		}
		catch (Exception localException2) {
			System.out.println(localException2);
			throw new Exception(localException2.getMessage());
		}
	}
	private static boolean isServerStarted(NetworkServerControl paramNetworkServerControl, int paramInt) {
		int i = 1;
		while (i <= paramInt)
			try {
				Thread.sleep(500L);
				paramNetworkServerControl.ping();
				return true;
			}
		catch (Exception localException) {
			if (i == paramInt)
				return false;
			i++;
		}
		return false;
	}
	
}
