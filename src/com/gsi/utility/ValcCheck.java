package com.gsi.utility;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.log4j.Logger;

import coral.reader.FileReader;

public class ValcCheck implements Runnable {
	private static final Logger logger = Logger.getLogger("prLogger");
	private static final int BUFFER_SIZE = 4096;

	@Override
	public void run() {
		try {
	    	FileReader fileReader = new FileReader();
	    	String keyLocation = System.getenv(GSIUtility.HOME_LOCATION_ENV_VARIABLE).trim() + "key.key";
	    	Map<String, Object> licenseMap = fileReader.getValues(keyLocation);
	    	//String clientId = (String)licenseMap.get("clientId");
	    	//String productId = (String)licenseMap.get("productId");
	    	String keyId = (String)licenseMap.get("keyId");
	    	String expirationDateStringFromKeyFile = (String)licenseMap.get("expirationDate");
	    	
	    	StringBuilder fileURL = new StringBuilder("http://staging-valcspa.cloudapp.net/resource/client/download-key/"); //AVClientId1/AVProdId1";
	    	//fileURL.append(clientId.trim()).append("/").append(productId.trim());
	    	fileURL.append(keyId.trim());
			URL url = new URL(fileURL.toString());
			HttpURLConnection  httpConn = (HttpURLConnection )url.openConnection();
	        int responseCode = httpConn.getResponseCode();
	        // always check HTTP response code first
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	            // opens input stream from the HTTP connection
	            InputStream inputStream = httpConn.getInputStream();
	            FileReader newFileReader = new FileReader();
	            Map<String, Object> newMap = newFileReader.getValues(inputStream);
	            String expirationDateStringFromValc = (String)newMap.get("expirationDate");
	            //Date newExpirationDate = sdf.parse(newExpirationDateString);
	            inputStream.close();
		        httpConn.disconnect();
	            if (!expirationDateStringFromKeyFile.equals(expirationDateStringFromValc)) {
    	            logger.info("New Key File will be downloaded with expiration date: " + expirationDateStringFromValc);
	    			httpConn = (HttpURLConnection )url.openConnection();
	    	        responseCode = httpConn.getResponseCode();
	    	        // always check HTTP response code first
	    	        if (responseCode == HttpURLConnection.HTTP_OK) {
	    	            String saveFilePath = keyLocation;
	    	            // opens an output stream to save into file
	    	            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
	    	            inputStream = httpConn.getInputStream();
	    	            int bytesRead = -1;
	    	            byte[] buffer = new byte[BUFFER_SIZE];
	    	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	    	                outputStream.write(buffer, 0, bytesRead);
	    	            }
	    	            inputStream.close();	    	 
	    	            outputStream.close();
	    		        httpConn.disconnect();
	    	            logger.info("New Key File downloaded successfully.");
	    	        }
	            }
	        }
	        else {
	            throw new Exception("No file to download in ValcCheck. Key Id: " + keyId + ". Server replied HTTP code: " + responseCode);
	        }
		}
		catch (Exception e) {
        	logger.error("", e);
		}
	}
}
