package com.gsi.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

/*import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;*/
import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import com.gsi.usertracker.dao.TwoFactorAuthorizerDAO;

public class SendMessageUtility {
	private static final Logger logger = Logger.getLogger("2faLogger");	
	private static final String MESSAGETYPE_EMAIL = "email";
	private static final String MESSAGETYPE_SMS = "sms";
	
	public SendMessageUtility() {
	}

	public static void sendEmailViaRest(String emailBody, String emailSubject, List<String> emailRecipients, int hostPort) throws Exception {
		try {
			StringBuffer sb = new StringBuffer("{");
			sb.append("\"recipients\":[");
			boolean firstTime = true;
			for (Iterator<String> i = emailRecipients.iterator(); i.hasNext(); ) {
				if (!firstTime) {
					sb.append(",");
				}
				else {
					firstTime = false;
				}
				sb.append("\"").append(i.next()).append("\"");
			}
			sb.append("],");
			sb.append("\"message\":\"").append(emailBody).append("\",");
			sb.append("\"sms\":false,");
			sb.append("\"email\":true,");
			sb.append("\"subject\":").append("\"").append(emailSubject).append("\",");
			sb.append("\"fromEmail\":").append("\"").append(TwoFactorAuthorizerDAO.selectPropertyByKey("emailFromAddress", hostPort)).append("\"");
			sb.append("}");
			sendMessageViaRest(sb.toString(), MESSAGETYPE_EMAIL);
		}
		catch (Exception e) {
			logger.error("", e);
		}
	}

	public static void sendSMSViaRest(String messageBody, List<String> emailRecipients, int hostPort) throws Exception {
		try {
			logger.info("Message Body:" + messageBody);
			logger.info("emailRecipients:" + emailRecipients.get(0));
			logger.info("fromNumber:" + TwoFactorAuthorizerDAO.selectPropertyByKey("twilioFromNumber", hostPort));
			StringBuffer sb = new StringBuffer("{");
			sb.append("\"recipients\":[");
			boolean firstTime = true;
			for (Iterator<String> i = emailRecipients.iterator(); i.hasNext(); ) {
				if (!firstTime) {
					sb.append(",");
				}
				else {
					firstTime = false;
				}
				sb.append("\"").append(i.next()).append("\"");
			}
			sb.append("],");
			sb.append("\"message\":\"").append(messageBody).append("\",");
			sb.append("\"fromNumber\": ").append("\"").append(TwoFactorAuthorizerDAO.selectPropertyByKey("twilioFromNumber", hostPort)).append("\"");
			sb.append("}");
			logger.info(" SMS JSON: " + sb.toString());
			sendMessageViaRest(sb.toString(), MESSAGETYPE_SMS);
		}
		catch (Exception e) {
			logger.error("", e);
		}
	}
	
	public static void sendMessageViaRest(String jsonString, String messageType) throws Exception {
		HttpsURLConnection conn = null;
		try {
			//logger.info("Before accepting all connections");
        	javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
        			new javax.net.ssl.HostnameVerifier() {
        			    public boolean verify(String hostname,
        			            javax.net.ssl.SSLSession sslSession) {
        			        return true; //ALL THE TIME!!!
        			    }
        			});				        	
			//logger.info("After accepting all connections");
			URL url = null;
			logger.info("Message Type:" + messageType);
			if (messageType.equalsIgnoreCase(MESSAGETYPE_EMAIL)) {
				//url = new URL("https://genius.getgsi.com/GENIUSAlertProcessor/alrtsvc/messageService/");
				url = new URL(null, "https://genius.getgsi.com/GENIUSAlertProcessor/alrtsvc/messageService/",new sun.net.www.protocol.https.Handler());
			}
			else if (messageType.equalsIgnoreCase(MESSAGETYPE_SMS)) {
				//url = new URL("https://genius.getgsi.com/GENIUSAlertProcessor/alrtsvc/sendSMS/");
				url = new URL(null, "https://genius.getgsi.com/GENIUSAlertProcessor/alrtsvc/sendSMS/",new sun.net.www.protocol.https.Handler());
			}
			if (url != null) {
				conn = (HttpsURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
			
				OutputStream os = conn.getOutputStream();
				os.write(jsonString.getBytes());
				os.flush();
	
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK && conn.getResponseCode() != HttpURLConnection.HTTP_CREATED && conn.getResponseCode() != HttpURLConnection.HTTP_ACCEPTED) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}
	
				BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
				}
			}
			else {
				throw new Exception("Incorrect send meaage type" + messageType);
			}
		} 
		catch (MalformedURLException e) {
			logger.error("", e);
			throw e;
		} 
		catch (IOException e) {
			logger.error("", e);
			throw e;
		}
		finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	/*private static final String SMTP_HOST_NAME = "smtp.sendgrid.net";
	private static final String SMTP_AUTH_USER = "azure_515e6297a25432a9a6e6fa9661c08d98@azure.com";
	private static final String SMTP_AUTH_PWD = "vH3QOHPrpHJDU90";
	private static final String SMTP_PORT = "587";
	public static void sendEmailViaMail(String emailBody, String emailSubject, List<String> emailRecipients) throws Exception {
		Transport transport = null;
		try {
			Properties properties = new Properties();
			properties.put("mail.transport.protocol", "smtp");
			properties.put("mail.smtp.host", SMTP_HOST_NAME);
			properties.put("mail.smtp.port", SMTP_PORT);
			properties.put("mail.smtp.auth", "true");
			String fromEmail = "noreply@getgsi.com";
	
			Authenticator auth = new SMTPAuthenticator();
			Session mailSession = Session.getInstance(properties, auth);
	
			transport = mailSession.getTransport();
			// Connect the transport object.
			transport.connect();
			//transport.connect(SMTP_HOST_NAME, SMTP_PORT, SMTP_AUTH_USER, SMTP_AUTH_PWD);
			Message email = getEmailMessage(mailSession, emailBody, emailSubject, fromEmail, emailRecipients);
			Address[] addresses = email.getAllRecipients();		
			transport.sendMessage(email, addresses);
		}
		catch (Exception e) {
			logger.error("", e);
			throw e;
		}
		finally {
			if (transport != null) {
				// Close the connection.
				transport.close();
			}
		}
	}

	private static Message getEmailMessage(Session mailSession, String emailBody, String emailSubject, String fromEmail, List<String> recipients) {
		MimeMessage message = new MimeMessage(mailSession);
		Multipart multipart = new MimeMultipart("alternative");
		try {
			BodyPart part1 = new MimeBodyPart();
			part1.setText("JDE Authentication Code: ");
			BodyPart part2 = new MimeBodyPart();			
			part2.setContent("<p>Hello,</p>"
							+ "<p>This is a JDE Authentication Alert message:</p>"
							+ "<p>"+ emailBody + " </br></p>", "text/html");
			multipart.addBodyPart(part1);
			multipart.addBodyPart(part2);
			Iterator<String> recipsi = recipients.iterator();
			while (recipsi.hasNext()) {
				String recip = recipsi.next();
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(recip));
			}
			message.setFrom(new InternetAddress(fromEmail));
			message.setSubject(emailSubject);
			message.setContent(multipart);
		} catch (AddressException e) {
			logger.error("",e);
			e.printStackTrace();
		} catch (MessagingException e) {
			logger.error("",e);
			e.printStackTrace();
		}
		return message;
	}

	private static class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			String username = SMTP_AUTH_USER;
			String password = SMTP_AUTH_PWD;
			return new PasswordAuthentication(username, password);
		}
	}*/
}
