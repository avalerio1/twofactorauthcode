USE [JDE910]
GO
/****** Object:  Trigger [SY910].[F0092_AFTERINSERT]    Script Date: 4/11/2017 1:23:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [SY910].[F0092_AFTERINSERT]
ON [SY910].[F0092] AFTER INSERT,UPDATE,DELETE 
as
BEGIN
	SET NOCOUNT ON;
    	DECLARE 
	   @v_username VARCHAR(10),
	   @v_action VARCHAR(10);

	SET @v_action = ''
	SET @v_action = (CASE 
	   WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		  THEN 'Update'  -- Set Action to Updated.
	   WHEN EXISTS(SELECT * FROM INSERTED)
		  THEN 'Insert'  -- Set Action to Insert.
	   WHEN EXISTS(SELECT * FROM DELETED)
    		THEN 'Delete'  -- Set Action to Deleted.
    	ELSE '' -- Skip. It may have been a "failed delete".   
    	END);
                       
   if @v_action = 'Insert'
   	BEGIN
		insert into SY910.F0092_GSIAUDIT
			(OLDULUSER, NEWULUSER, ULUGRP, UPDATE_DATETIME, UPDATE_USER, UPDATE_ACTION)
		select ' ', i.ULUSER, i.ULUGRP, CURRENT_TIMESTAMP, User_Name(), @v_action
			from SY910.F0092 f
			inner join inserted i on f.ULUSER = i.ULUSER
   	END
   else if @v_action = 'Update'
   	BEGIN
		insert into SY910.F0092_GSIAUDIT
			(OLDULUSER, NEWULUSER, ULUGRP, UPDATE_DATETIME, UPDATE_USER, UPDATE_ACTION)
		select d.ULUSER, i.ULUSER, i.ULUGRP, CURRENT_TIMESTAMP, User_Name(), @v_action
			from SY910.F0092 f
			inner join inserted i on f.ULUSER = i.ULUSER
			inner join deleted d on f.ULUSER = d.ULUSER
   	END
   else if @v_action = 'Delete'
   	BEGIN
		insert into SY910.F0092_GSIAUDIT
			(OLDULUSER, NEWULUSER, ULUGRP, UPDATE_DATETIME, UPDATE_USER, UPDATE_ACTION)
		select d.ULUSER, ' ', d.ULUGRP, CURRENT_TIMESTAMP, User_Name(), @v_action
			from SY910.F0092 f
			inner join deleted d on f.ULUSER = d.ULUSER
   	END
END;
