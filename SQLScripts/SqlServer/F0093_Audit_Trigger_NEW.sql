USE [JDE910]
GO
/****** Object:  Trigger [SY910].[F0093_AFTERINSERT]    Script Date: 4/11/2017 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [SY910].[F0093_AFTERINSERT]
ON [SY910].[F0093] AFTER INSERT,UPDATE,DELETE 
as
BEGIN
	SET NOCOUNT ON;
	DECLARE 
	   @v_action VARCHAR(10);
	   
	SET @v_action = ''
	SET @v_action = (CASE 
	   WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		  THEN 'Update'  -- Set Action to Updated.
	   WHEN EXISTS(SELECT * FROM INSERTED)
		  THEN 'Insert'  -- Set Action to Insert.
	   WHEN EXISTS(SELECT * FROM DELETED)
    		THEN 'Delete'  -- Set Action to Deleted.
    	ELSE '' -- Skip. It may have been a "failed delete".   
    	END);
                       
   if @v_action = 'Insert'
   	BEGIN
	   	insert into SY910.F0093_GSIAUDIT
		   (OLDLLUSER,OLDLLLL,OLDLLSEQ,OLDLLMNI,NEWLLUSER,NEWLLLL,NEWLLSEQ,NEWLLMNI,UPDATE_DATETIME,UPDATE_USER,UPDATE_ACTION)
		select ' ',' ',' ',' ', i.LLUSER,i.LLLL,i.LLSEQ,i.LLMNI, CURRENT_TIMESTAMP, User_Name(), @v_action
		from SY910.F0093 f
			inner join inserted i on f.LLUSER = i.LLUSER and f.LLLL = i.LLLL
		
   	END
   else if @v_action = 'Update'
   	BEGIN
	   	insert into SY910.F0093_GSIAUDIT
		   (OLDLLUSER,OLDLLLL,OLDLLSEQ,OLDLLMNI,NEWLLUSER,NEWLLLL,NEWLLSEQ,NEWLLMNI,UPDATE_DATETIME,UPDATE_USER,UPDATE_ACTION)
		select d.LLUSER,d.LLLL,d.LLSEQ,d.LLMNI, i.LLUSER,i.LLLL,i.LLSEQ,i.LLMNI, CURRENT_TIMESTAMP, User_Name(), @v_action
		from SY910.F0093 f
			inner join inserted i on f.LLUSER = i.LLUSER and f.LLLL = i.LLLL
			inner join deleted d on f.LLUSER = d.LLUSER and f.LLLL = d.LLLL
   	END
   else if @v_action = 'Delete'
   	BEGIN
	   	insert into SY910.F0093_GSIAUDIT
		   (OLDLLUSER,OLDLLLL,OLDLLSEQ,OLDLLMNI,NEWLLUSER,NEWLLLL,NEWLLSEQ,NEWLLMNI,UPDATE_DATETIME,UPDATE_USER,UPDATE_ACTION)
		select d.LLUSER,d.LLLL,d.LLSEQ,d.LLMNI, ' ',' ',' ',' ', CURRENT_TIMESTAMP, User_Name(), @v_action
		from SY910.F0093 f
			inner join deleted d on f.LLUSER = d.LLUSER and f.LLLL = d.LLLL
   	END
END;
