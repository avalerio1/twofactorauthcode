CREATE OR REPLACE TRIGGER "SY910"."F98OWSEC_AFTERINSERT"
AFTER UPDATE OR INSERT OR DELETE ON "SY910"."F98OWSEC"
FOR EACH ROW
DECLARE 
	v_username varchar2(10); 
	v_action varchar2(10);
	oldSCUSER NCHAR(10)  := ' ';
	oldSCOWPWD NCHAR(10) := ' ';
	oldSCSECFRQ NUMBER := 0;
	oldSCSECLST NUMBER(6) := 0;
	oldSCSECUSR NCHAR(10) := ' ';
	oldSCSECPWD NCHAR(10) := ' ';
	oldSCSECTPE NCHAR(3) := ' ';
	oldSCUGRP NCHAR(10) := ' ';
	oldSCSECF1 NCHAR(30) := ' ';
	oldSCSECF2 NCHAR(30) := ' ';
	oldSCSECF3 NCHAR(30) := ' ';
	oldSCSECV1 NCHAR(10) := ' ';
	oldSCSECV2 NCHAR(10) := ' ';
	oldSCSECV3 NCHAR(10) := ' ';
	oldSCSECV4 NCHAR(10) := ' ';
	oldSCSRVBLOBA BLOB;
	oldSCATTEMPTS NUMBER := 0;
	oldSCEUSER NCHAR(2) := ' ';
	oldSCRETRY NUMBER := 0;
	oldSCMUSE NCHAR(10) := ' ';
	oldSCPID NCHAR(10) := ' ';
	oldSCJOBN NCHAR(10) := ' ';
	oldSCUPMJ NUMBER(6) := 0;
	oldSCUPMT NUMBER := 0;
	newSCUSER NCHAR(10)  := ' ';
	newSCOWPWD NCHAR(10) := ' ';
	newSCSECFRQ NUMBER := 0;
	newSCSECLST NUMBER(6) := 0;
	newSCSECUSR NCHAR(10) := ' ';
	newSCSECPWD NCHAR(10) := ' ';
	newSCSECTPE NCHAR(3) := ' ';
	newSCUGRP NCHAR(10) := ' ';
	newSCSECF1 NCHAR(30)  := ' ';
	newSCSECF2 NCHAR(30) := ' ';
	newSCSECF3 NCHAR(30) := ' ';
	newSCSECV1 NCHAR(10) := ' ';
	newSCSECV2 NCHAR(10) := ' ';
	newSCSECV3 NCHAR(10) := ' ';
	newSCSECV4 NCHAR(10) := ' ';
	newSCSRVBLOBA BLOB;
	newSCATTEMPTS NUMBER := 0;
	newSCEUSER NCHAR(2) := ' ';
	newSCRETRY NUMBER := 0;
	newSCMUSE NCHAR(10) := ' ';
	newSCPID NCHAR(10) := ' ';
	newSCJOBN NCHAR(10) := ' ';
	newSCUPMJ NUMBER(6) := 0;
	newSCUPMT NUMBER := 0;
BEGIN
    -- First initialize the BLOB variables
    DBMS_LOB.CREATETEMPORARY(oldSCSRVBLOBA, FALSE, DBMS_LOB.SESSION);
    DBMS_LOB.CREATETEMPORARY(newSCSRVBLOBA, FALSE, DBMS_LOB.SESSION);
    -- Find username of person performing UPDATE on the table
   SELECT user INTO v_username
   FROM dual;
   
    IF INSERTING THEN 
        v_action := 'Insert';
	newSCUSER := :new.SCUSER;
	newSCOWPWD := :new.SCOWPWD;
	newSCSECFRQ := :new.SCSECFRQ;
	newSCSECLST := :new.SCSECLST;
	newSCSECUSR := :new.SCSECUSR;
	newSCSECPWD := :new.SCSECPWD;
	newSCSECTPE := :new.SCSECTPE;
	newSCUGRP := :new.SCUGRP;
	newSCSECF1 := :new.SCSECF1;
	newSCSECF2 := :new.SCSECF2;
	newSCSECF3 := :new.SCSECF3;
	newSCSECV1 := :new.SCSECV1;
	newSCSECV2 := :new.SCSECV2;
	newSCSECV3 := :new.SCSECV3;
	newSCSECV4 := :new.SCSECV4;
	newSCSRVBLOBA := :new.SCSRVBLOBA;
	newSCATTEMPTS := :new.SCATTEMPTS;
	newSCEUSER := :new.SCEUSER;
	newSCRETRY := :new.SCRETRY;
	newSCMUSE := :new.SCMUSE;
	newSCPID := :new.SCPID;
	newSCJOBN := :new.SCJOBN;
	newSCUPMJ := :new.SCUPMJ;
	newSCUPMT := :new.SCUPMT;
    END IF;
    IF DELETING THEN 
        v_action := 'Delete'; 
	oldSCUSER := :old.SCUSER;
	oldSCOWPWD := :old.SCOWPWD;
	oldSCSECFRQ := :old.SCSECFRQ;
	oldSCSECLST := :old.SCSECLST;
	oldSCSECUSR := :old.SCSECUSR;
	oldSCSECPWD := :old.SCSECPWD;
	oldSCSECTPE := :old.SCSECTPE;
	oldSCUGRP := :old.SCUGRP;
	oldSCSECF1 := :old.SCSECF1;
	oldSCSECF2 := :old.SCSECF2;
	oldSCSECF3 := :old.SCSECF3;
	oldSCSECV1 := :old.SCSECV1;
	oldSCSECV2 := :old.SCSECV2;
	oldSCSECV3 := :old.SCSECV3;
	oldSCSECV4 := :old.SCSECV4;
	oldSCSRVBLOBA := :old.SCSRVBLOBA;
	oldSCATTEMPTS := :old.SCATTEMPTS;
	oldSCEUSER := :old.SCEUSER;
	oldSCRETRY := :old.SCRETRY;
	oldSCMUSE := :old.SCMUSE;
	oldSCPID := :old.SCPID;
	oldSCJOBN := :old.SCJOBN;
	oldSCUPMJ := :old.SCUPMJ;
	oldSCUPMT := :old.SCUPMT;
     END IF;
    IF UPDATING THEN 
        v_action := 'Update'; 
	oldSCUSER := :old.SCUSER;
	oldSCOWPWD := :old.SCOWPWD;
	oldSCSECFRQ := :old.SCSECFRQ;
	oldSCSECLST := :old.SCSECLST;
	oldSCSECUSR := :old.SCSECUSR;
	oldSCSECPWD := :old.SCSECPWD;
	oldSCSECTPE := :old.SCSECTPE;
	oldSCUGRP := :old.SCUGRP;
	oldSCSECF1 := :old.SCSECF1;
	oldSCSECF2 := :old.SCSECF2;
	oldSCSECF3 := :old.SCSECF3;
	oldSCSECV1 := :old.SCSECV1;
	oldSCSECV2 := :old.SCSECV2;
	oldSCSECV3 := :old.SCSECV3;
	oldSCSECV4 := :old.SCSECV4;
	oldSCSRVBLOBA := :old.SCSRVBLOBA;
	oldSCATTEMPTS := :old.SCATTEMPTS;
	oldSCEUSER := :old.SCEUSER;
	oldSCRETRY := :old.SCRETRY;
	oldSCMUSE := :old.SCMUSE;
	oldSCPID := :old.SCPID;
	oldSCJOBN := :old.SCJOBN;
	oldSCUPMJ := :old.SCUPMJ;
	oldSCUPMT := :old.SCUPMT;
	newSCUSER := :new.SCUSER;
	newSCOWPWD := :new.SCOWPWD;
	newSCSECFRQ := :new.SCSECFRQ;
	newSCSECLST := :new.SCSECLST;
	newSCSECUSR := :new.SCSECUSR;
	newSCSECPWD := :new.SCSECPWD;
	newSCSECTPE := :new.SCSECTPE;
	newSCUGRP := :new.SCUGRP;
	newSCSECF1 := :new.SCSECF1;
	newSCSECF2 := :new.SCSECF2;
	newSCSECF3 := :new.SCSECF3;
	newSCSECV1 := :new.SCSECV1;
	newSCSECV2 := :new.SCSECV2;
	newSCSECV3 := :new.SCSECV3;
	newSCSECV4 := :new.SCSECV4;
	newSCSRVBLOBA := :new.SCSRVBLOBA;
	newSCATTEMPTS := :new.SCATTEMPTS;
	newSCEUSER := :new.SCEUSER;
	newSCRETRY := :new.SCRETRY;
	newSCMUSE := :new.SCMUSE;
	newSCPID := :new.SCPID;
	newSCJOBN := :new.SCJOBN;
	newSCUPMJ := :new.SCUPMJ;
	newSCUPMT := :new.SCUPMT;
    END IF;
   
   --DBMS_OUTPUT.PUT_LINE('****' || oldNameValue || ' * ' || oldPositionValue || ' * ' || CURRENT_TIMESTAMP || ' * ' || :new.NAME || ' * ' || :new.POSITION || ' * ' || v_username || ' * ' || v_action);
   
   insert into F98OWSEC_GSIAUDIT
        values (oldSCUSER,oldSCOWPWD,oldSCSECFRQ,oldSCSECLST,oldSCSECUSR,oldSCSECPWD,oldSCSECTPE,oldSCUGRP,oldSCSECF1,oldSCSECF2,oldSCSECF3,oldSCSECV1,oldSCSECV2,oldSCSECV3,oldSCSECV4,oldSCSRVBLOBA,oldSCATTEMPTS,oldSCEUSER,oldSCRETRY,oldSCMUSE,oldSCPID,oldSCJOBN,oldSCUPMJ,oldSCUPMT,
        	newSCUSER,newSCOWPWD,newSCSECFRQ,newSCSECLST,newSCSECUSR,newSCSECPWD,newSCSECTPE,newSCUGRP,newSCSECF1,newSCSECF2,newSCSECF3,newSCSECV1,newSCSECV2,newSCSECV3,newSCSECV4,newSCSRVBLOBA,newSCATTEMPTS,newSCEUSER,newSCRETRY,newSCMUSE,newSCPID,newSCJOBN,newSCUPMJ,newSCUPMT,
        CURRENT_TIMESTAMP, v_username, v_action);
        
END;
