CREATE OR REPLACE TRIGGER "AV"."TABLE1_BEFORE_UPDATE"
BEFORE UPDATE OR INSERT OR DELETE ON AV.TABLE1
FOR EACH ROW
DECLARE 
    v_username varchar2(10); 
    v_action varchar2(10);
    oldNameValue varchar2(25);
    oldPositionValue varchar2(25);
BEGIN
    -- Find username of person performing UPDATE on the table
   SELECT user INTO v_username
   FROM dual;
   
    IF INSERTING THEN 
        v_action := 'Insert';
        oldNameValue := ' ';
        oldPositionValue := ' ';
    END IF;
    IF DELETING THEN 
        v_action := 'Delete'; 
        oldNameValue := ' ';
        oldPositionValue := ' ';
    END IF;
    IF UPDATING THEN 
        v_action := 'Update'; 
        oldNameValue := :old.NAME;
        oldPositionValue := :old.POSITION;
    END IF;
   
   DBMS_OUTPUT.PUT_LINE('****' || oldNameValue || ' * ' || oldPositionValue || ' * ' || CURRENT_TIMESTAMP || ' * ' || :new.NAME || ' * ' || :new.POSITION || ' * ' || v_username || ' * ' || v_action);
   
   insert into AV.TABLE1HISTORY
        values (oldNameValue, oldPositionValue, CURRENT_TIMESTAMP, :new.NAME, :new.POSITION, v_username, v_action);
        
END;
