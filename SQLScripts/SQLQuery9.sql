USE [AVDB1]
GO
/****** Object:  Trigger [dbo].[Name_Table_Trigger]    Script Date: 4/10/2017 8:29:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[Name_Table_Trigger] 
   ON  [dbo].[Name_Table] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
    DECLARE 
	   @v_username VARCHAR(10),
	   @v_action VARCHAR(10)

	SET @v_action = ''
	SET @v_action = (CASE 
	   WHEN EXISTS(SELECT * FROM INSERTED) AND EXISTS(SELECT * FROM DELETED)
		  THEN 'Update'  -- Set Action to Updated.
	   WHEN EXISTS(SELECT * FROM INSERTED)
		  THEN 'Insert'  -- Set Action to Insert.
	   WHEN EXISTS(SELECT * FROM DELETED)
    		THEN 'Delete'  -- Set Action to Deleted.
    	ELSE '' -- Skip. It may have been a "failed delete".   
    	END);

   	if @v_action != ''
   	BEGIN
		if @v_action = 'Insert'
		BEGIN
			insert into dbo.Name_Table_Audit
			(OLD_Name, OLD_Middle_Name, OLD_Name_id, NEW_Name, NEW_Middle_Name, NEW_Name_id, Update_Time, Update_Type)
			select '' as oldName, '' as oldMiddleName, '' as oldNameId, nt1.Name as newName, nt1.Middle_Name as newMiddleName, nt1.Name_Id as newNameId, CURRENT_TIMESTAMP as currentTime, @v_action as updateType from dbo.Name_Table nt1
				inner join inserted i on nt1.Name_Id = i.Name_Id
		END
		if @v_action = 'Update'
		BEGIN
			insert into dbo.Name_Table_Audit
			(OLD_Name, OLD_Middle_Name, OLD_Name_id, NEW_Name, NEW_Middle_Name, NEW_Name_id, Update_Time, Update_Type)
			select d.Name as oldName, d.Middle_Name as oldMiddleName, d.Name_Id as oldNameId, i.Name as newName, i.Middle_Name as newMiddleName, i.Name_Id as newNameId, CURRENT_TIMESTAMP as currentTime, @v_action as updateType from dbo.Name_Table nt1
				inner join deleted d on nt1.Name_Id = d.Name_Id
				inner join inserted i on nt1.Name_Id = i.Name_Id
		END
		if @v_action = 'Delete'
		BEGIN
			insert into dbo.Name_Table_Audit
			(OLD_Name, OLD_Middle_Name, OLD_Name_id, NEW_Name, NEW_Middle_Name, NEW_Name_id, Update_Time, Update_Type)
			select d.Name as oldName, d.Middle_Name as oldMiddleName, d.Name_Id as oldNameId, '' as newName, '' as newMiddleName, '' as newNameId, CURRENT_TIMESTAMP as currentTime, @v_action as updateType from dbo.Name_Table nt1
				inner join deleted d on nt1.Name_Id = d.Name_Id
		END
    END
END;
